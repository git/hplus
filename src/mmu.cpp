/* mmu.cpp - memory management unit - implementation */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <bits/addr_space_layout.h>
#include <bits/interrupt_entry.h>
#include <stdlib/stdlib.h>
#include <stdlib/assert.h>
#include <mmu.hpp>
#include <malloc.hpp>
#include <scheduler.hpp>
#include <global.hpp>
#include <panic.hpp>
#include <processor.hpp>
#include <kernel-sync.hpp>
#include <algorithm>

#define PHYSICAL_PAGE_ARRAY physical_addr_t(page_size)

static const size_t malloc_cache = 1 * 1024 * 1024;

#if 0
static inline size_t
page_to_view(size_t page)
  {
    return page / (sizeof(PhysicalPageView::view_t) * 8 / 2);
  }
#endif

static AddressSpace* kernel_space;
static uintptr_t kmalloc_begin;
mutex_t mmu_lock;

AddressSpace::AddressSpace() :
  pt(true)
{
  return;
}

AddressSpace::AddressSpace(const GlobalStorage& gs) :
  datasegment(NULL), pt(false)
{
  kernel_space = this;
}

void
AddressSpace::load()
{
  this->pt.load();
}

AddressSpace*
AddressSpace::current()
{
  Task& t(Processor::getTask());
  GlobalStorage& gs(Processor::getGlobalStorage());
  handle<Scheduler> s(gs.getScheduler());
  Process* curproc(s->getProcess(t));
  assert(curproc != NULL);
  return &(curproc->getAddressSpace());
}

MMU::MMU(BootInfo::MemoryMap& m, bool quiet) :
  memory(m, quiet), swapspace(0), max_kernel_datasegment(
      (char*) (KERNELSPACE_BEGIN - INIT_MAPPED_MIN + INIT_MAPPED_MAX)),
      min_kernel_datasegment((char*) (KERNELSPACE_BEGIN - INIT_MAPPED_MIN
          + INIT_MAPPED_MAX))
{
  kernel_datasegment.begin = (char*) KTEXT_BEGIN;
  kernel_datasegment.end = (char*) KTEXT_END;
  return;
}
MMU::~MMU()
{
  panic("Destroying the Memory Management Unit");
}

void
MMU::start()
{
  Processor::self().setInterrupt(Processor::page_fault, do_page_fault);
  kernel_space->pt.moveToKernel();
  MemoryMap map;
  map.flags = MMU::reserved | MMU::read | MMU::write;
  kernel_space->datasegment = &(kernel_space->mmaps.insert(std::make_pair(
      kernel_datasegment, map)).first->first);
}

bool
MMU::addMapping(AddressSpace* sp, const MMU::AddressRange& range,
    unsigned int flags)
{
  MemoryMap map;
  map.flags = flags & MMU::public_flag_mask;
  MMU::AddressRange r(range);
  if (r.begin == r.end)
    return false;
  if (sp == NULL || sp == kernel_space)
    {
      sp = kernel_space;
      r.begin = max((char*) KMMAP_BEGIN, r.begin);
      r.end = min((char*) KERNELSPACE_END, r.end);
      if (r.end <= r.begin)
        return false;
    }
  else
    {
      r.end = min(r.end, (char*) USER_END);
      r.begin = max(r.begin, (char*) USER_DYN_BEGIN);
      if (r.end <= r.begin)
        return false;
    }
  scoped_lock l(sp->lock);
  maptree::iterator lb(sp->mmaps.lower_bound(r));
  if (lb == sp->mmaps.end())
    {
      sp->mmaps.insert(std::make_pair(r, map));
      return true;
    }
  if (!(r < lb->first)) // overlap
    return false;
  sp->mmaps.insert(lb, std::make_pair(r, map));
  return true;
}
bool
MMU::dropMapping(AddressSpace* sp, const MMU::AddressRange& in)
{
  MMU::AddressRange r(in);
  if (sp == NULL || sp == kernel_space)
    {
      sp = kernel_space;
      r.begin = max((char*) KMMAP_BEGIN, r.begin);
      r.end = min((char*) KERNELSPACE_END, r.end);
      if (r.end <= r.begin)
        return true;
    }
  else
    {
      r.end = min(r.end, (char*) USER_END);
      r.begin = max(r.begin, (char*) USER_DYN_BEGIN);
      if (r.end <= r.begin)
        return true;
    }
  scoped_lock l(sp->lock);
  maptree::iterator lb(sp->mmaps.lower_bound(r));
  if (lb == sp->mmaps.end())
    return true;
  maptree::iterator ub(sp->mmaps.upper_bound(r));
  while (lb != ub)
    {
      if (lb->second.flags & MMU::merged_dest)
        // this is the source of a merging, don't drop the mapping
        return false;
    }
  bool haspre(false), haspost(false);
  AddressRange pre, post;
  MemoryMap mpre, mpost;
  AddressRange unpre, unpost;
  maptree::iterator next(lb);
  next++;
  if (lb->first.begin < r.begin)
    {
      mpre = lb->second;
      pre.begin = lb->first.begin;
      pre.end = r.begin;
      haspre = true;
      if (lb->first.end <= r.end)
        { // one map for the whole range
          unpre.begin = r.begin;
          unpre.end = lb->first.end;
          this->unfault(sp, unpre);
          sp->mmaps.erase(lb);
          sp->mmaps.insert(std::make_pair(pre, mpre));
          return true;
        }
      else
        {
          this->unfault(sp, r);
        }
    }
  else if (lb->first.end <= r.end)
    {
      sp->mmaps.erase(lb);
      this->unfault(sp, lb->first);
      return true;
    }
  sp->mmaps.erase(lb);
  lb = next;
  while (lb != sp->mmaps.end())
    {
      if (r < lb->first)
        break;
      if (lb->first.end > r.end)
        {
          mpost = lb->second;
          post.begin = r.end;
          post.end = lb->first.end;
          haspost = true;
          unpost.begin = lb->first.begin;
          unpost.end = r.end;
          this->unfault(sp, unpost);
        }
      else
        this->unfault(sp, lb->first);
      sp->mmaps.erase(lb);
      lb = next;
      next++;
    }
  if (haspre)
    sp->mmaps.insert(std::make_pair(pre, mpre));
  if (haspost)
    sp->mmaps.insert(std::make_pair(post, mpost));
  return true;
}
bool
MMU::merge(AddressSpace* sp_src, const AddressRange& src, AddressSpace* sp_dst,
    const AddressRange& dst, unsigned int dst_flags)
{
  // consistency checks
  AddressRange rsrc(src);
  if (sp_src == NULL || sp_src == kernel_space)
    {
      sp_src = kernel_space;
      rsrc.begin = max((char*) KMMAP_BEGIN, rsrc.begin);
      rsrc.end = min((char*) KERNELSPACE_END, rsrc.end);
      if (rsrc.end <= rsrc.begin)
        return false;
    }
  else
    {
      rsrc.end = min(rsrc.end, (char*) USER_END);
      rsrc.begin = max(rsrc.begin, (char*) USER_DYN_BEGIN);
      if (rsrc.end <= rsrc.begin)
        return false;
    }
  AddressRange rdst(dst);
  if (sp_dst == NULL || sp_dst == kernel_space)
    {
      sp_dst = kernel_space;
      rdst.begin = max((char*) KMMAP_BEGIN, rdst.begin);
      rdst.end = min((char*) KERNELSPACE_END, rdst.end);
      if (rdst.end <= rdst.begin)
        return false;
    }
  else
    {
      rdst.end = min(rdst.end, (char*) USER_END);
      rdst.begin = max(rdst.begin, (char*) USER_DYN_BEGIN);
      if (rdst.end <= rdst.begin)
        return false;
    }
  maptree::iterator psrc;
  MemoryMap src_mem;
  maptree::iterator lb_src(sp_src->mmaps.lower_bound(rsrc));
  maptree::iterator lb_dst(sp_dst->mmaps.lower_bound(rdst));
  if (lb_dst != sp_dst->mmaps.end() and !(rdst < lb_dst->first))
    // destination map already in use
    return false;
  if (lb_src == sp_src->mmaps.end() or rsrc < lb_src->first)
    // source memory map is missing
    return false;
  else
    {
      AddressRange original_src(lb_src->first);
      MemoryMap mem(lb_src->second);
      if (mem.flags & MMU::merged_dest)
        return false;
      if (!(mem.flags & MMU::write) && (dst_flags & MMU::write))
        return false;
      if (original_src.begin <= rsrc.begin)
        {
          // split the map
          if (original_src.end < rsrc.end)
            {
              // crosses a map boundary
              return false;
            }
          sp_src->mmaps.erase(lb_src);
          AddressRange ar;
          if (original_src.begin < rsrc.begin)
            {
              ar.begin = original_src.begin;
              ar.end = rsrc.begin;
              sp_src->mmaps.insert(std::make_pair(ar, mem));
            }
          src_mem = mem;
          src_mem.flags |= MMU::merged_dest;
          psrc = (sp_src->mmaps.insert(std::make_pair(rsrc, src_mem))).first;
          if (original_src.end > rsrc.end)
            {
              ar.begin = rsrc.end;
              ar.end = original_src.end;
              sp_src->mmaps.insert(std::make_pair(ar, mem));
            }
        }
      else
        return false;
    }
  MemoryMap dst_mem;
  dst_mem.flags = (dst_flags & MMU::public_flag_mask) | MMU::merged_source;
  maptree::iterator pdst =
      (sp_dst->mmaps.insert(std::make_pair(rdst, dst_mem))).first;
  psrc->second.range_dst = &(pdst->first);
  psrc->second.map_dst = &(pdst->second);
  psrc->second.sp_dst = sp_dst;
  pdst->second.range_src = &(psrc->first);
  pdst->second.map_dst = &(psrc->second);
  pdst->second.sp_src = sp_src;
  return true;
}
bool
MMU::checkPermission(AddressSpace* sp, const AddressRange& rng,
    unsigned int acl)
{
  acl = acl & MMU::public_flag_mask;
  maptree::iterator lb(sp->mmaps.lower_bound(rng));
  if (lb == sp->mmaps.end() or rng < lb->first)
    return false;
  maptree::iterator ub(sp->mmaps.upper_bound(rng));
  while (lb != ub)
    {
      if (!(lb->second.flags & acl))
        return false;
      ++lb;
    }
  return true;
}
bool
MMU::fault(AddressSpace* sp, const MMU::AddressRange& in)
{
  scoped_lock l(mmu_lock);
  AddressRange r(in);
  assert(r.end >= r.begin);
  if (sp == NULL)
    sp = kernel_space;
  physical_addr_t page_no(((char*) (r.end) - (char*) (r.begin)) / page_size);
  char* cur((char*) (r.begin));
  MemoryMap* m(&(sp->mmaps.find(r)->second));
  unsigned int acl = m->flags & PageTable::acl_mask;
  while (m->flags & MMU::merged_source)
    {
      // find the real source of data
      m = (m->map_src);
    }
  while (page_no > 0)
    {
      PhysicalPageView::AddressRange pr(this->memory.getFree());
      if (pr.from == physical_addr_t(-1) and pr.to == 0x0) // null range
        return false;
      PhysicalPageView::AddressRange mapped(pr);
      mapped.to = mapped.from;
      for (physical_addr_t p(pr.from); p < min(pr.to, pr.from + (page_no
          * page_size)); p += page_size)
        {
          sp->pt.setPage(cur + p - pr.from, p, true, acl);
          mapped.to += page_size;
        }
      this->memory.markRange(mapped.from, mapped.to, PhysicalPageView::mapped);
      if (m->flags & MMU::file_source)
        {
          m->file_src->fill(cur, pr.to - pr.from);
        }
      else
        memset(cur, 0, pr.to - pr.from);
      cur += pr.to - pr.from;
      page_no -= (pr.to - pr.from) / page_size;
    }
  return true;
}
bool
MMU::fault(AddressSpace* sp, const MMU::AddressRange& r, physical_addr_t at)
{
  scoped_lock l(mmu_lock);
  assert(r.end >= r.begin);
  if (sp == NULL)
    sp = kernel_space;
  MemoryMap& m(sp->mmaps.find(r)->second);
  if (m.flags & MMU::merged_source or m.flags & MMU::file_source)
    return false;
  size_t sz(r.end - r.begin);
  char* cur(r.begin);
  for (physical_addr_t p(at); p < (at + sz); p += page_size)
    {
      sp->pt.setPage(cur + p - at, p, true, m.flags & PageTable::acl_mask);
    }
  this->memory.markRange(at, at + sz,
      (m.flags & MMU::reserved) ? PhysicalPageView::reserved
          : PhysicalPageView::mapped);
  return true;
}
bool
MMU::unfault(AddressSpace* sp, const MMU::AddressRange& r)
{
  scoped_lock l(mmu_lock);
  if (sp == NULL)
    sp = kernel_space;
  char* begin(r.begin);
  physical_addr_t pbegin(sp->pt.getPage(begin));
  while (pbegin == physical_addr_t(-1))
    {
      begin += page_size;
      if (begin >= r.end)
        return true; // nothing in main memory
      pbegin = sp->pt.getPage(begin);
    }
  sp->pt.setPage(begin, 0, false, 0);
  physical_addr_t pend = pbegin;
  for (char* cur(begin + page_size); cur < (char*) r.end; cur += page_size)
    {
      physical_addr_t pcur = sp->pt.getPage(cur);
      if (pcur != (pend + page_size))
        { // not contiguous range
          if (pend != pbegin)
            {
              this->memory.markRange(pbegin, pend, PhysicalPageView::available);
            }
          while (pcur == physical_addr_t(-1))
            {
              cur += page_size;
              if (cur >= r.end)
                return true; // complete!
              pcur = sp->pt.getPage(cur);
            }
          pbegin = pcur;
          pend = pcur;
        }
      else
        {
          pend = pcur;
        }
      sp->pt.setPage(cur, 0, false, 0);
    }
  return true;
}
MMU::AddressRange
MMU::createMMap(AddressSpace* sp, size_t sz)
{
  char* spbegin;
  char* spend;
  if (sp == NULL || sp == kernel_space)
    {
      sp = kernel_space;
      spbegin = (char*) KMMAP_BEGIN;
      spend = (char*) KMMAP_END;
    }
  else
    {
      spbegin = (char*) USER_DYN_BEGIN;
      spend = (char*) USER_END;
    }
  scoped_lock l(sp->lock);
  char* cur_end(spbegin);
  AddressRange ret;
  if (sp->mmaps.empty())
    {
      ret.begin = spbegin;
      ret.end = spbegin + sz;
      return ret;
    }
  for (maptree::iterator lb(sp->mmaps.begin()); lb != sp->mmaps.end(); lb++)
    {
      if (size_t(lb->first.begin - cur_end) > sz) // fill a gap
        {
          ret.begin = cur_end;
          ret.end = cur_end + sz;
          return ret;
        }
      cur_end = lb->first.end;
    }
  if (size_t(spend - cur_end) < sz)
    {
      // reached end of space
      ret.begin = ret.end = NULL;
      return ret; // not valid
    }
  ret.begin = cur_end;
  ret.end = cur_end + sz;
  return ret;
}

void*
MMU::extend(AddressSpace* sp, ptrdiff_t offset)
{
  bool prefault(false);
  if (sp == NULL)
    sp = kernel_space;
  char* spend;
  char* spbegin;
  if (sp == kernel_space)
    {
      prefault = true;
      spend = (char*) KMALLOC_END;
      spbegin = (char*) KERNELSPACE_BEGIN;
    }
  else
    {
      spend = (char*) USER_DYN_BEGIN;
      spbegin = (char*) USER_BEGIN;
    }
  const AddressRange* curdataseg = (sp->datasegment);
  if (curdataseg == NULL)
    {
      if (sp == kernel_space)
        curdataseg = &kernel_datasegment;
      else
        return (void*) -1;
    }
  scoped_lock l(sp->lock);
  if (offset > 0)
    {
      if (sp->datasegment != NULL)
        {
          maptree::iterator curdatapos = sp->mmaps.find(*curdataseg);
          maptree::iterator nextmappos(curdatapos);
          nextmappos++;
          if (nextmappos == sp->mmaps.end())
            { // nothing past the data segment
              // ensure we don't go over the maximums
              if ((spend - curdataseg->end) < offset)
                return (void*) -1;
            }
          else
            {
              // ensure we don't overlap
              if ((nextmappos->first.begin - curdataseg->end) < offset)
                return (void*) -1;
              // still ensure we don't go over the maximums
              if ((spend - curdataseg->end) < offset)
                return (void*) -1;
            }
        }
      if (prefault)
        {
          if ((char*) sp->datasegment->end + offset > (max_kernel_datasegment))
            {// map more memory
              AddressRange r;
              r.begin = sp->datasegment->end;
              r.end = max_kernel_datasegment + malloc_cache;
              this->fault(sp, r);
              max_kernel_datasegment += malloc_cache;
            }
        }
      if (sp->datasegment != NULL)
        {
          AddressRange newdataseg(*curdataseg);
          newdataseg.end += offset;
          sp->mmaps.erase(*curdataseg);
          MemoryMap m;
          m.flags = MMU::read | MMU::write;
          sp->mmaps.insert(std::make_pair(newdataseg, m));
        }
      else
        this->kernel_datasegment.end += offset;
    }
  else if (offset < 0)
    {
      if (curdataseg->end - offset < curdataseg->begin) // never drop all of the data seg
        return (void*) -1;
      if (prefault && size_t(-offset) > malloc_cache)
        {
          size_t drop_size(0);
          if (prefault) // drop in malloc_cache units
            drop_size = (malloc_cache * ((-offset) / malloc_cache));
          else
            // drop in page_size units
            drop_size = (page_size * ((-offset) / page_size));
          if ((max_kernel_datasegment - drop_size) > min_kernel_datasegment)
            {
              AddressRange r;
              r.begin = max_kernel_datasegment - drop_size;
              r.end = max_kernel_datasegment;
              this->unfault(sp, r);
              max_kernel_datasegment -= drop_size;
            }
        }
      if (sp->datasegment != NULL)
        {
          AddressRange newdataseg(*curdataseg);
          newdataseg.end += offset;
          sp->mmaps.erase(*curdataseg);
          MemoryMap m;
          m.flags = MMU::read | MMU::write;
          sp->mmaps.insert(std::make_pair(newdataseg, m));
        }
      else
        this->kernel_datasegment.end += offset;
    }
  return (curdataseg->end);
}

physical_addr_t
MMU::getFreePage()
{
  PhysicalPageView::AddressRange r(this->memory.getFree());
  if (r.from == physical_addr_t(-1) and r.to == 0)
    panic("Here we should swap, I suppose");
  this->memory.markPage(r.from, PhysicalPageView::reserved);
  return r.from;
}
void
MMU::releaseFreePage(physical_addr_t at)
{
  this->memory.markPage(at, PhysicalPageView::available);
}

void
MMU::do_fault(uintptr_t address, unsigned long flags)
{
  Task& t(Processor::getTask());
  GlobalStorage& gs(Processor::getGlobalStorage());
  handle<Scheduler> s(gs.getScheduler());
  MMU& mem(gs.getMMU());
  Process* proc(s->getProcess(t));
  assert(proc != NULL);
  AddressSpace& sp(proc->getAddressSpace());
  scoped_lock l(sp.lock);
  AddressRange key;
  key.begin = (char*) (address & ~(page_size - 1));
  key.end = (char*) ((address + page_size) & ~(page_size - 1));
  maptree::iterator lb(sp.mmaps.lower_bound(key));
  if (lb == sp.mmaps.end() or ((char*) address < lb->first.begin)
      or ((char*) address > lb->first.end))
    {
      if (proc->getPid() == 0)
        {
          printf("Page fault in kernel code, accessed 0x%lx\n", address);
          panic();
        }
      else
        {
          siginfo_t sig =
            { siginfo_t::SIGSEGV,
                (flags & PAGEFAULT_PROTECTION ? siginfo_t::SEGV_ACCERR
                    : siginfo_t::SEGV_MAPERR), 0, 0, t.getProgramCounter(), 0,
                  { 0 } };
          Thread* thr(proc->getThread(t));
          thr->queueSignal(&sig);
          thr->mark(Thread::error);
        }
    }
  mem.fault(&sp, key);
}
PhysicalPageView::PhysicalPageView(BootInfo::MemoryMap& m, bool quiet) :
  mem_size(0), free_size(0), map_size(0), view(NULL), first_free_view(NULL),
      view_end(NULL)
{
  for (BootInfo::MemoryMap::iterator i(m.begin()); i != m.end(); ++i)
    {
      if (i.available())
        mem_size = max(mem_size, i.end() / page_size);
    }
  if (!quiet)
    printf("Initializing physical address space...\n%ld kilobytes available\n",
        mem_size * page_size / (1024));
#if 0
  size_t view_cnt = (mem_size + 63) / 64; // excess integer rounding
#else
  size_t view_cnt = (0x100000) / 64; // all 4GBs are seen
#endif
  this->view = this->first_free_view = new ((void*) KTEXT_END) view_t[view_cnt];
  this->view_end = this->view + view_cnt;
  this->markRange(0, 0x100000000, missing);
  kmalloc_begin = uintptr_t(this->view_end);
  for (BootInfo::MemoryMap::iterator i(m.begin()); i != m.end(); ++i)
    {
      physical_addr_t pstart(i.begin()), pend(i.end());
      assert(pend > pstart);
#if 0
      if (pstart >= mem_size * page_size)
      break;
#endif
      this->markRange(pstart, pend, i.available() ? available : reserved);
      if (!quiet)
        printf("Memory pool from %lx to %lx, marked %s\n", pstart, pend,
            (i.available() ? "available" : "reserved"));
    }
  this->markRange(TEXT_SEGMENT_MIN, TEXT_SEGMENT_END, mapped);
#if HAVE_RUNTIME_SELFTEST
  for (uintptr_t i(0); i < (this->plast); i += 3789 * 1023)
    {
      MMU::PhysicalPage::memory_type type(this->getPage(i));
      printf("Page at %lx is %s\n", i,
          type == MMU::PhysicalPage::available ? "available" : (type
              == MMU::PhysicalPage::reserved ? "reserved" : (type
                  == MMU::PhysicalPage::mapped ? "mapped" : "missing")));
    }
  MMU::PhysicalPage::memory_type last_type(this->getPage(this->plast));
  printf("Maximum address is %lx, which is %s\n", (this->plast), last_type
      == MMU::PhysicalPage::available ? "available" : (last_type
          == MMU::PhysicalPage::reserved ? "reserved" : (last_type
              == MMU::PhysicalPage::mapped ? "mapped" : "missing")));
#endif
}
PhysicalPageView::PhysicalPageView(size_t sz) :
  mem_size(sz), free_size(sz), map_size(0), view(NULL), first_free_view(NULL),
      view_end(NULL)
{
  if (sz != 0)
    {
      this->view = this->first_free_view = new view_t[sz / 64];
      this->view_end = this->view + (sz / 64);
    }
}
bool
PhysicalPageView::AddressRange::size_compare(
    const PhysicalPageView::AddressRange& a,
    const PhysicalPageView::AddressRange& b)
{
  return (a.to - a.from) < (b.to - b.from);
}

PhysicalPageView::memory_type
PhysicalPageView::getPage(physical_addr_t addr)
{
  if (addr >= mem_size * page_size)
    return missing;
  physical_addr_t page(addr / page_size);
  physical_addr_t view(page / 64);
  return page_from_view(this->view[view], page);
}
void
PhysicalPageView::markRange(physical_addr_t from, physical_addr_t to,
    PhysicalPageView::memory_type as)
{
  physical_addr_t end(min(to / page_size, mem_size));
  physical_addr_t start(min(from / page_size, mem_size - 1));
  bool free(false), exists(false);
  switch (as)
    {
  case available:
    free_size += end - start;
    this->first_free_view = min(this->first_free_view, this->view
        + (start / 64));
    exists = true;
    free = true;
    break;
  case missing:
    free = true;
    exists = false;
    break;
  case mapped:
    map_size += end - start;
    free = false;
    exists = true;
    break;
  case reserved:
    free = false;
    exists = false;
    }
  view_t view_mark =
    { free ? 0xFFFFFFFFFFFFFFFF : 0, exists ? 0xFFFFFFFFFFFFFFFF : 0 };
  if ((end / 64) == (start / 64))
    {
      view_t& view(this->view[(start / 64)]);
      for (physical_addr_t j(start % 64); j < (end % 64); ++j)
        {
          if (free)
            view.free |= (1 << j);
          else
            view.free &= ~(1 << j);
          if (exists)
            view.exists |= (1 << j);
          else
            view.exists &= ~(1 << j);
        }
    }
  else
    {
      if ((start % 64) != 0)
        {
          view_t& first_view(this->view[(start / 64)]);
          for (physical_addr_t j(start % 64); j < 64; ++j)
            {
              if (free)
                first_view.free |= (1 << j);
              else
                first_view.free &= ~(1 << j);
              if (exists)
                first_view.exists |= (1 << j);
              else
                first_view.exists &= ~(1 << j);
            }
        }
      for (physical_addr_t view((start / 64) + (((start % 64) != 0 ? 1 : 0))); view
          < (end / 64); ++view)
        this->view[view] = view_mark;
      view_t& second_view(this->view[(end / 64)]);
      for (physical_addr_t j(0); j < (end % 64); ++j)
        {
          if (free)
            second_view.free |= (1 << j);
          else
            second_view.free &= ~(1 << j);
          if (exists)
            second_view.exists |= (1 << j);
          else
            second_view.exists &= ~(1 << j);
        }
    }
}
void
PhysicalPageView::markPage(physical_addr_t at, PhysicalPageView::memory_type as)
{
  this->markRange(at, at + page_size, as);
}
inline PhysicalPageView::memory_type
PhysicalPageView::page_from_view(view_t view, uint8_t offset)
{
  return memory_type((((view.free >> offset) & 1) << 1) | ((view.exists
      >> offset) & 1));
}

PhysicalPageView::AddressRange
PhysicalPageView::getFree()
{
  while (true)
    {
      view_t* v(this->first_free_view);
      while (v->free == 0 or v->exists == 0)
        { // fast skip
          v++;
          if (v == this->view_end)
            return ((AddressRange)
              { -1, 0x0}); // no more memory
          this->first_free_view = v;
        }
      unsigned int exists(v->exists);
      unsigned int free(v->free);
      AddressRange tmp =
        { 0x0, 0x0 };
      bool found(false);
      for (int j(min(__builtin_ffs(free) - 1, __builtin_ffs(exists) - 1)); j
          < 64; ++j)
        {
          if (found)
            {
              if (!((exists & (1 << j)) && (free & (1 << j))))
                break;
              tmp.to += page_size;
            }
          else if ((exists & (1 << j)) && (free & (1 << j)))
            {
              found = true;
              tmp.from = ((v - this->view) * 64 + j) * page_size;
              tmp.to = tmp.from + page_size;
            }
        }
      if (found)
        return tmp;
      this->first_free_view++;
    }
}

size_t
PhysicalPageView::getSize() const
{
  return this->mem_size;
}
