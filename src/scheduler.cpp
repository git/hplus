/* scheduler.cpp - process and thread management (implementation) */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <bits/addr_space_layout.h>
#include <stdlib/assert.h>
#include <kernel-sync.hpp>
#include <scheduler.hpp>
#include <global.hpp>
#include <panic.hpp>

Scheduler::Scheduler()
{
  Task& t(Processor::getTask());
  Process* kern = new Process(0, 0);
  this->by_pid.insert(std::make_pair(0, kern));
  this->by_pages.insert(std::make_pair(t.getPagingCR(), kern));
}

Process*
Scheduler::getProcess(Task& t) const
{
  if (t.getProgramCounter() > (void*) USER_END)
    return (this->by_pid.find(0)->second);
  paging_cr_t cr(t.getPagingCR());
  std::map<paging_cr_t, Process*>::const_iterator i(this->by_pages.find(cr));
  if (i == this->by_pages.end())
    return NULL;
  return i->second;
}

Process*
Scheduler::getProcess(pid_t p) const
{
  std::map<pid_t, Process*>::const_iterator proc(this->by_pid.find(p));
  if (proc == this->by_pid.end())
    return NULL;
  return proc->second;
}

Process::Process(Process* p, pid_t s) :
  space((p == NULL) ? &((Processor::getGlobalStorage()).getKernelSpace())
      : new AddressSpace()), self(s), parent((p == NULL) ? 0 : p->getPid())
{
  return;
}

Thread*
Process::getThread(Task& t) const
{
  void* stack = t.getStack();
  std::map<void*, Thread*>::const_iterator
      lb(this->by_stack.lower_bound(stack));
  if (lb == this->by_stack.end())
    return NULL;
  if ((char*) stack < ((char*) lb->first - lb->second->stack_size))
    return NULL;
  return lb->second;
}

void
Thread::mark(Thread::status_t as)
{
  assert(as != running);
  this->status = as;
  if (this->status == running and *(this->on) != Processor::self())
    {
      this->on->sendIRQ(Processor::rescheduling_irq);
    }
}

void
Thread::queueSignal(siginfo_t* sig)
{
  if (sig->si_signo < siginfo_t::SIGRTMIN)
    { // normal signal
      if (this->pending_signals[sig->si_signo] != NULL)
        delete this->pending_signals[sig->si_signo];
      this->pending_signals[sig->si_signo] = sig;
    }
  rtsiginfo_t* s = static_cast<rtsiginfo_t*> (sig);
  s->si_id = this->pending_rtsignals.rbegin()->first + 1; // one past the last
  this->pending_rtsignals.insert(std::make_pair(s->si_id, s));
  this->sigpending |= (1 << sig->si_signo) & ~(this->sigmask);
}

#define TIME_SLICE 420000 // 150 microseconds at 2,80 GHz
