/* errno.h - error numbers */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef ERRNO_H_
#define ERRNO_H_

enum err_t
{
  /* Basic error codes */
  ENOERR = 0, // Success
  EPERM, // Operation not permitted
  ENOENT, // No such file or directory
  ESRCH, // No such process
  EINTR, // Interrupted system call
  EIO, // Input/output error
  ENXIO, // No such device or address
  E2BIG, // Argument list too long
  ENOEXEC, // Exec format error
  EBADF, // Bad file descriptor
  ECHILD, // No child processes
  EDEADLK, // Deadlock avoided
  ENOMEM, // No memory available
  EACCES, // Permission denied
  EFAULT, // Bad address
  ENOTBLK, // Block device required
  EBUSY, // Resource busy
  EEXIST, // File exists
  EXDEV, // Invalid cross-device link
  ENODEV, // No such device
  ENOTDIR, // Not a directory
  EISDIR, // Is a directory
  EINVAL, // Invalid argument
  EMFILE, // Too many open files
  ENFILE, // Too many open files in system
  ENOTTY, // Inappropriate ioctl for the device
  ETXTBSY, // Text file busy
  EFBIG, // File too big
  ENOSPC, // No space left on device
  ESPIPE, // Illegal seek
  EROFS, // Read-only file system
  EMLINK, // Too many links
  EPIPE, // Broken pipe
  EDOM, // Domain error
  ERANGE, // Range error
  EAGAIN, // Resource temporarily unavailable
  /* Socket related errors */
  EINPROGRESS, // Operation in progress
  EALREADY, // Operation already in progress
  ENOTSOCK, // Not a socket
  EMSGSIZE, // Message too large
  EPROTOTYPE, // Protocol not supported by the socket type
  ENOPROTOOPT, // Option not supported for the protocol
  EPROTONOSUPPORT, // Protocol not supported
  ESOCKTNOSUPPORT, // Socket type not supported
  EOPNOTSUPP, // Operation not supported for the socket
  EPFNOSUPPORT, // Protocol family not supported
  EAFNOSUPPORT, // Address family not supported
  EADDRINUSE, // Address in use
  EADDRNOTAVAIL, // Address not available
  ENETDOWN, // Network is down
  ENETUNREACH, // Network is unreachable
  ENETRESET, // Network reset
  ECONNABORTED, // Connection aborted
  ECONNRESET, // Connection reset by peer
  ENOBUFS, // Not enough memory for server
  EISCONN, // Already connected
  ENOTCONN, // Not connected
  EDESTADDRREQ, // Destination address required
  ESHUTDOWN, // Socket has been shut down
  ETOOMANYREFS, // ???
  ETIMEDOUT, // Connection timed out
  ECONNREFUSED, // Connection refused
  EHOSTDOWN, // The remote host is down
  EHOSTUNREACH, // No route to host
  /* Filesystem */
  ELOOP, // Too many levels of symbolic links
  ENAMETOOLONG, // Filename too long
  ENOTEMPTY, // Directory not empty
  EPROCLIM, // Process limit reached
  EUSERS, // Too many users
  EDQUOT, // Disk quota exceeded
  ESTALE, // Stale NFS file handle
  EREMOTE, // Object is remote
  EBADRPC, // ???
  ERPCMISMATCH, // ???
  EPROGUNAVAIL, // ???
  EPROGMISMATCH, // ???
  EPROCUNAVAIL, // ???
  ENOLCK, // No locks available
  EFTYPE, // Inappropriate file type or format
  EAUTH, // ???
  ENEEDAUTH, // ???
  /* Various */
  ENOSYS, // Function not implemented
  ENOTSUP, // Not supported
  EILSEQ, // Illegal multibyte sequence
  EBACKGROUND, // Process is in background
  EDIED, // ???
  /* GNU specific (EIEIO in particular means: no translator for the file) */
  ED, // The experienced user will know what is wrong
  EGREGIOUS, // You did what?
  EIEIO, // Computer bought the farm
  EGRATUITOUS, // This error code has no purpose.
  /* STREAMS errors */
  EBADMSG, // Not a data message
  EIDRM, // Identifier removed
  EMULTIHOP, // Multihop attempted
  ENODATA, // No device available
  ENOLINK, // Link has been severed
  ENOMSG, // No message of desired type
  ENOSR, // Out of stream resources
  ENOSTR, // Device not a stream
  EOVERFLOW, // Value too large for defined data type
  EPROTO, // Protocol error
  ETIME, // Timer expired
  ECANCELED, // Operation canceled
};

#define EWOULDBLOCK EAGAIN

#endif /* ERRNO_H_ */
