/* stdout.cpp - standard output stub */
/* Copyright (C) 1999 Free Software Foundation
                 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib/stdlib.h>
#include <stdlib/stdarg.h>

/* Some screen stuff. */
/* The number of columns. */
#define COLUMNS                 80
/* The number of lines. */
#define LINES                   24
/* The attribute of an character. */
#define ATTRIBUTE               7
/* The video memory address. */
#define VIDEO                   0xFFFFFFFF800B8000

/* Variables. */
/* Save the X position. */
static int xpos;
/* Save the Y position. */
static int ypos;
/* Point to the video memory. */
static volatile unsigned char *video;

/* Format a string and print it on the screen, just like the libc
 function printf. */
void
printf(const char *format, ...)
{
  va_list vl;
  va_start(vl, format);
  int c;
  unsigned long val;
  char buf[60];

  while ((c = *format++) != 0)
    {
      if (c != '%')
        putchar(c);
      else
        {
          const char *p;

          c = *format++;
          switch (c)
            {
          case 'l':
            val = va_arg(vl,unsigned long);
            c = *format++;
            goto integer;
          case 'u':
          case 'x':
            val = va_arg(vl,unsigned int);
          case 'd':
            val = va_arg(vl,int);
            integer: itoa(val, buf, (c == 'x' ? 16 : 10));
            p = buf;
            goto string;
            break;

          case 's':
            p = va_arg(vl,char*);
            if (!p)
              p = "(null)";

            string: while (*p)
              putchar(*p++);
            break;

          case 'c':
          default:
            putchar(va_arg(vl, unsigned int));
            break;
            }
        }
    }
  va_end(vl);
}

/* Put the character C on the screen. */
void
putchar(int c)
{
  if (c == '\n' || c == '\r')
    {
      newline: xpos = 0;
      ypos++;
      if (ypos >= LINES)
        ypos = 0;
      return;
    }

  *(video + (xpos + ypos * COLUMNS) * 2) = c & 0xFF;
  *(video + (xpos + ypos * COLUMNS) * 2 + 1) = ATTRIBUTE;

  xpos++;
  if (xpos >= COLUMNS)
    goto newline;
}

/* Clear the screen and initialize VIDEO, XPOS and YPOS. */
void
cls(void)
{
  volatile int i;

  video = (unsigned char *) VIDEO;

  for (i = 0; i < COLUMNS * LINES * 2; i++)
    *(video + i) = 0;

  xpos = 0;
  ypos = 0;
}

