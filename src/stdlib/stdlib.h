/* stdlib.h - C-like standard library */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef STDLIB_H_
#define STDLIB_H_

#define _STDDEF_H
#define _SYS_TYPES_H
#define _TIME_H
#define _UNISTD_H

#include <architecture.hpp>
#include <errno.h>
#include <limits.h>
#include <stddef.h>

/* Types */

typedef uint16_t pid_t;
typedef uint16_t thread_id_t;
struct translator_id_t
{
  pid_t pid;
  uint16_t trans_id;
};
typedef uint32_t dev_t;
typedef uint32_t ino_t;
typedef uint16_t mode_t;
typedef uint32_t nlink_t;
typedef int64_t off_t;
struct scheduler_id_t
{
  pid_t pid;
  thread_id_t tid;
};
typedef uint16_t uid_t;
typedef uint16_t gid_t;
struct siginfo_t
{
  enum
  {
    SIGHUP = 1,
    SIGINT,
    SIGQUIT,
    SIGILL,
    SIGTRAP,
    SIGABRT,
    SIGBUS,
    SIGFPE,
    SIGKILL,
    SIGUSR1,
    SIGSEGV,
    SIGUSR2,
    SIGPIPE,
    SIGALRM,
    SIGTERM,
    SIGSTKFLT,
    SIGCHLD,
    SIGCONT,
    SIGSTOP,
    SIGTSTP,
    SIGTTIN,
    SIGTTOU,
    SIGURG,
    SIGSYS,
    SIGXCPU,
    SIGXFSZ,
    SIGVTALARM,
    SIGIO,
    SIGRTMIN = 32, // first real time signal
    SIGSYSCALL = 32,
    SIGINTERRUPT,
    SIGRTUSRMIN, // first user space real time signal
    SIGRTMAX = 63
  } si_signo;
  enum
  {
    ILL_ILLOPC,
    ILL_ILLOPN,
    ILL_ILLADR,
    ILL_ILLTRP,
    ILL_PRVOPC,
    ILL_PRVREG,
    ILL_COPROC,
    ILL_BADSTK,
    FPE_INTDIV,
    FPE_INTOVF,
    FPE_FLTDIV,
    FPE_FLTOVF,
    FPE_FLTUND,
    FPE_FLTRES,
    FPE_FLTINV,
    FPE_FLTSUB,
    SEGV_MAPERR,
    SEGV_ACCERR,
    BUS_ADRALN,
    BUS_ADRERR,
    BUS_OBJERR,
    TRAP_BRKPT,
    TRAP_TRACE,
    CLD_EXITED,
    CLD_KILLED,
    CLD_DUMPED,
    CLD_TRAPPED,
    CLD_STOPPED,
    CLD_CONTINUED,
    POLL_IN,
    POLL_OUT,
    POLL_MSG,
    POLL_ERR,
    POLL_PRI,
    POLL_HUP,
    SI_USER,
    SI_QUEUE,
    SI_TIMER,
    SI_ASYNCIO,
    SI_MESGQ
  } si_code;
  pid_t si_pid;
  uid_t si_uid;
  void* si_addr;
  int si_status;
  union sigval {
    int val_int;
    void* val_ptr;
  } si_value;
  size_t si_valuesz; // size of value (immediately following, if not realtime)
} __attribute__((aligned(8)));
struct rtsiginfo_t : siginfo_t {
  typedef uint32_t sigid_t;
  sigid_t si_id;
};
typedef uint64_t time_t;
typedef uint64_t sigset_t;
typedef size_t blksize_t;
typedef size_t blkcnt_t;
struct stat {
    dev_t     st_dev;     /* ID of device containing file */
    ino_t     st_ino;     /* inode number */
    mode_t    st_mode;    /* protection */
    nlink_t   st_nlink;   /* number of hard links */
    uid_t     st_uid;     /* user ID of owner */
    gid_t     st_gid;     /* group ID of owner */
    dev_t     st_rdev;    /* device ID (if special file) */
    off_t     st_size;    /* total size, in bytes */
    blksize_t st_blksize; /* blocksize for file system I/O */
    blkcnt_t  st_blocks;  /* number of 512B blocks allocated */
    time_t    st_atime;   /* time of last access */
    time_t    st_mtime;   /* time of last modification */
    time_t    st_ctime;   /* time of last status change */
};

/* Constants ('x' marks those that have/require kernel support) */
#define O_READ  0x1 // x
#define O_WRITE 0x2 // x
#define O_EXEC  0x4 // x
#define O_CREAT 0x8 // x
#define O_EXCL  0x10 // x
#define O_NONBLOCK 0x20
#define O_NOTRANS  0x40 // x
#define O_TRUNC    0x80
#define O_SHLOCK   0x100 // x
#define O_EXLOCK   0x200 // x
#define O_APPEND   0x400
#define O_ASYNC    0x800
#define O_FSYNC    0x1000
#define O_NOATIME  0x2000

void
cls(void);
char*
itoa(long d, char* value, int base);
char*
itoa(unsigned long d, char* value, int base);
void
putchar(int c);
void
printf(const char *format, ...) __attribute__((format(gnu_printf,1,2)));

extern "C"
  {
    void* memcpy(void* dest,const void* source,size_t sz);
    void* memset(void* dest,int c,size_t sz);
    int strcmp(const char* c1,const char* c2);
    size_t strlen(const char* c);
    char* strchr(const char* str,int c);
    int strncmp(const char* s1,const char* s2,size_t size);
  }

template<class T>
inline T min(T a,T b)
  {
    return a < b?a:b;
  }
template<class T>
inline T max(T a,T b)
  {
    return a < b?b:a;
  }
#endif /* STDLIB_H_ */
