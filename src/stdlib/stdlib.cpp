/* stdlib.cpp - utility functions */
/* Copyright (C) 2010 Giovanni Campagna

   This file is part of the H+ kernel, a microkernel for the GNU system.      

   H+ is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */


/* Convert the integer D to a string and save the string in BUF. */
char*
itoa(long d,char *buf, int base)
{
  char *p = buf;
  char *p1, *p2;
  unsigned long ud = d;

  /* If %d is specified and D is minus, put `-' in the head. */
  if (base == 10 && d < 0)
    {
      *p++ = '-';
      buf++;
      ud = -d;
    }

  /* Divide UD by DIVISOR until UD == 0. */
  do
    {
      long remainder = ud % base;

      *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
    }
  while (ud /= base);

  /* Terminate BUF. */
  *p = 0;

  /* Reverse BUF. */
  p1 = buf;
  p2 = p - 1;
  while (p1 < p2)
    {
      char tmp = *p1;
      *p1 = *p2;
      *p2 = tmp;
      p1++;
      p2--;
    }
  return buf;
}

/* Convert the integer D to a string and save the string in BUF. */
char*
itoa(unsigned long d,char *buf, int base)
{
  char *p = buf;
  char *p1, *p2;
  unsigned long ud = d;

  /* Divide UD by DIVISOR until UD == 0. */
  do
    {
      long remainder = ud % base;

      *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
    }
  while (ud /= base);

  /* Terminate BUF. */
  *p = 0;

  /* Reverse BUF. */
  p1 = buf;
  p2 = p - 1;
  while (p1 < p2)
    {
      char tmp = *p1;
      *p1 = *p2;
      *p2 = tmp;
      p1++;
      p2--;
    }
  return buf;
}
