/* assert.h - kernel version of standard assert.h header */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef ASSERT_H_
#define ASSERT_H_

#include <stdlib.h>
#include <panic.hpp>

#define assert(expr) ((!(expr)) ? assertion_failed (#expr, __FILE__, __LINE__, __func__) : true)

inline bool assertion_failed(const char *assertion, const char *file, unsigned int line,
    const char *function)
{
  (void) printf("%s:%d: %s: Assertion `%s' failed.\n", file, line, function,
      assertion);
  panic();
  return false;
}

#endif /* ASSERT_H_ */
