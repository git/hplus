/* main.cpp - the kernel boot procedure */
/* Copyright (C) 1999  Free Software Foundation, Inc.
 2009  Giovanni Campagna

 This file is part of H+, a microkernel for the GNU system.

 H+ is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

#include <config.h>
#include <stdlib/stdlib.h>
#include <stdlib/assert.h>
#include <processor.hpp>
#include <mmu.hpp>
#include <malloc.hpp>
#include <scheduler.hpp>
#include <rtconfig.hpp>
#include <bootinfo.hpp>
#include <panic.hpp>
#include <global.hpp>

/* Initial kernel operation. */
extern "C" void
cmain(unsigned long magic, opaque_boot_info mb)
{
  cls();
  BootInfo b(magic, mb);
  GlobalStorage gs(b);
  RTConfig& conf(gs.getConfig());
  MMU& mem(gs.getMMU());
  ExecCommand init(conf.getInitCmd());
  mem.start();
  BootInfo::InitRamFS initrd(b.getInitRd());
    {
      char* cur((char*) initrd.begin());
      printf("Loaded initrd at address %lx\nIt says: ", uintptr_t(cur));
      while (cur != (char*) initrd.end())
        {
          putchar(*cur);
          ++cur;
        }
      putchar('\n');
    }
  if (!conf.quiet())
    printf("Executing %s\n",init.getFilename());
  printf("%s: no such file or directory\n", init.getFilename());
  panic();
}

GlobalStorage::GlobalStorage(BootInfo& b) :
  conf(b.getCmdLine()), mem(b.getMemoryMap(), conf.quiet()),
      kernel_space(*this), mem_arena(mem, true), multiproc(*this)
{
  return;
}
