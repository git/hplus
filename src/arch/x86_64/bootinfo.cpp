/* bootinfo.cpp - version independent interface to multiboot (implementation) */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <bits/addr_space_layout.h>
#include <stdlib.h>
#include <architecture.hpp>
#include <bootinfo.hpp>
#include <mmu.hpp>
#include <global.hpp>
#include <panic.hpp>

/* Check if the bit BIT in FLAGS is set. */
#define CHECK_FLAG(flags,bit)   ((flags) & (1 << (bit)))

BootInfo::BootInfo(unsigned long magic, multiboot_info* i) :
  memory((multiboot_mmap_entry*) ((char*) i->mmap_addr),
      (multiboot_mmap_entry*) ((char*) i->mmap_addr + i->mmap_length)),
      initrd_begin(0), initrd_end(0), rom(i->config_table),
      cmdline((char*) i->cmdline)
{
  uintptr_t addr((uintptr_t) i);
  if (addr >= LOW_MAPPED_MAX or addr < LOW_MAPPED_MIN)
    panic("Bootloader information is at an unreachable location");
  if (magic != MULTIBOOT_BOOTLOADER_MAGIC)
    panic("Unsupported boot loader (configured for: multiboot-v1)");
  if (!(i->flags & MULTIBOOT_INFO_MEM_MAP))
    panic("Boot loader did not provide a memory map");
  if (!(i->flags & MULTIBOOT_INFO_CMDLINE))
    this->cmdline = NULL;
  if (i->mods_count > 1)
    {
      panic("More than one initial ram FS loaded");
    }
  else if (i->mods_count == 0)
    {
      panic("Initial ram FS not loaded");
    }
  multiboot_mod_list* mod((multiboot_mod_list*) (i->mods_addr));
  this->initrd_begin = mod->mod_start;
  this->initrd_end = mod->mod_end;
}

BootInfo::InitRamFS::InitRamFS(physical_addr_t begin,
    physical_addr_t end)
{
  size_t sz(end - begin);
  sz = (sz + page_size) & ~(page_size - 1);
  GlobalStorage& gs(Processor::getGlobalStorage());
  MMU& mem(gs.getMMU());
  MMU::AddressRange range(mem.createMMap(NULL, sz));
  if (mem.addMapping(NULL, range, MMU::read
      | MMU::write | MMU::reserved))
    {
      mem.fault(NULL, range, begin);
    }
  this->_begin = range.begin;
  this->_end = (range.begin + (end - begin));
}
