/* processor.cpp - processor management */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <bits/flags.h>
#include <bits/interrupt_entry.h>
#include <assert.h>
#include <architecture.hpp>
#include <processor.hpp>
#include <kernel-sync.hpp>
#include <global.hpp>

extern char boot_stack[];

//char boot_stack[BOOT_STACK_SIZE];
char stack[STACK_SIZE];

struct TSSDescriptor
{
  unsigned short limit_1;
  unsigned short base_1;
  unsigned char base_2;
  unsigned int type :4;
  bool res_1 :1;
  unsigned int dpl :2;
  bool present :1;
  unsigned int limit_2 :4;
  int res_2 :4;
  unsigned char base_3;
  unsigned long base_4;
  long reserved;
}__attribute__((packed));

extern TSSDescriptor tss_descr;
extern uint32_t gdt;

struct TaskStateSegment
{
  int res_1;
  void* lev0_rsp;
  void* lev1_rsp;
  void* lev2_rsp;
  long res_2;
  void* ist[7];
  long res_3;
  short res_4;
  void* iomap_base;
}__attribute__((packed));

struct InterruptDescriptor
{
  unsigned short offset_1;
  unsigned short code_segment;
  int ist_entry :3;
  int res_1 :5;
  int type :4;
  int res_2 :1;
  int dpl :2;
  bool present :1;
  unsigned short offset_2;
  unsigned int offset_3;
  unsigned int res_4;
}__attribute__((packed));

struct IGDTR
{
  unsigned short limit;
  uintptr_t igdt;
}__attribute__((packed));

InterruptDescriptor idt[256];
mutex_t idt_lock;

// Interrupt handlers


Processor::Processor(GlobalStorage& gs, unsigned int id)
{
  /* Load a GDT in high address space */
  IGDTR gdtr =
    { (gdt), uintptr_t(&gdt) };
  asm("lgdt %0" : : "m" (gdtr));

  /* Set up local storage */
  ls.id = id;
  ls.t = new Task;
  ls.tss = new TaskStateSegment;
  memset(ls.tss,0,sizeof(TaskStateSegment));
  ls.glob = &gs;

  /* Set up TSS */
  ls.tss->lev0_rsp = (void*) ((char*) stack + STACK_SIZE - 1);
  ls.tss->ist[0] = ls.tss->lev0_rsp;
  uintptr_t addr = uintptr_t(&ls.tss);
  tss_descr.present = true;
  tss_descr.type = 0b1001;
  tss_descr.limit_1 = sizeof(TaskStateSegment);
  tss_descr.limit_2 = sizeof(TaskStateSegment) >> 15;
  tss_descr.base_1 = addr;
  tss_descr.base_2 = addr >> 16;
  tss_descr.base_3 = addr >> 24;
  tss_descr.base_4 = addr >> 32;
  register short tsssel asm("ax") = KERNEL_TSSSEL;
  asm("ltr %0" : : "r" (tsssel));
  this->write_msr(MSR_GS_BASE,uint64_t(&ls));
  this->write_msr(MSR_KERNEL_GS_BASE,uint64_t(&ls));

  this->setInterrupt(Processor::general_protection,do_gp);
  this->setInterrupt(Processor::invalid_instruction,do_ud);
}

void
Processor::setInterrupt(Processor::interrupt_code code,
    Processor::exception_handler_t handler)
{
  bool int_enabled(this->interrupts_enabled());
  if (int_enabled)
    Processor::disable_interrupts();
  mutex_lock(&idt_lock);
  InterruptDescriptor& d(idt[code]);
  d.present = true;
  d.type = 0b1110;
  d.ist_entry = 0;
  uintptr_t h((uintptr_t) handler);
  d.offset_1 = h;
  d.offset_2 = h >> 16;
  d.offset_3 = h >> 32;
  d.code_segment = KERNEL_CS;
  IGDTR reg =
    { sizeof(idt), uintptr_t(idt) };
  asm("lidt %0" : : "m" (reg));
  mutex_unlock(&idt_lock);
  if (int_enabled)
    Processor::enable_interrupts();
}


SMP::SMP(GlobalStorage& gs) :
  proc_cnt(1), procs(new Processor(gs, 1))
{
  return;
}
