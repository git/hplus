/* link.x - linker script */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */
 
 /* This file was derived on the standard ld linker script.
 ld is part of GNU Binutils
 Copyright 2008 Free Software Foundation, Inc. */
 
#define LD_SCRIPT
#include <bits/addr_space_layout.h>
 
/* Script for -z combreloc: combine and sort reloc sections */
OUTPUT_FORMAT("elf64-x86-64", "elf64-x86-64",
	      "elf64-x86-64")
OUTPUT_ARCH(i386:x86-64)
ENTRY(start)
/* PHDRS {
	text PT_LOAD FILEHDR PHDRS;
	build-id PT_NOTE;
	eh_frame_hdr PT_GNU_EH_FRAME;
	data PT_LOAD;
} */
SECTIONS
{
  PROVIDE(__executable_start = SEGMENT_START("text-segment", TEXT_SEGMENT_MIN));
  . = SEGMENT_START("text-segment", TEXT_SEGMENT_MIN) + SIZEOF_HEADERS;
  .boot : AT(TEXT_SEGMENT_MIN+0x1000)
  {
  	*(.boot)
  } =0x90909090
  _boot_end = .;
  . = KTEXT_BEGIN;
  .text :  AT(_boot_end)
  {
    *(.text .stub .text.* .gnu.linkonce.t.*)
    /* .gnu.warning sections are handled specially by elf32.em.  */
    *(.gnu.warning)
  }  =0x90909090
  .note.gnu.build-id : { *(.note.gnu.build-id) }
  _text_end = .;
  .rodata       : { *(.rodata .rodata.* .gnu.linkonce.r.*) } 
  .eh_frame_hdr : { *(.eh_frame_hdr) } 
  .eh_frame     : { *(.eh_frame) } 
  .data         :
  {
    *(.data .data.* .gnu.linkonce.d.*)
    SORT(CONSTRUCTORS)
  } 
  _data_end = .;
  .bss            :
  {
   *(.dynbss)
   *(.bss .bss.* .gnu.linkonce.b.*)
   *(COMMON)
   _bss_end = .;
   /* ensure that .bss ends physically at INIT_MAPPED_MAX */
   . = ALIGN(BSS_SIZE);
  }
  _end = .;
  _image_end = .;
  .comment       0 : { *(.comment) }
  /* DWARF debug sections.
     Symbols in the DWARF debugging sections are relative to the beginning
     of the section so we begin them at 0.  */
  /* DWARF 1 */
  .debug          0 : { *(.debug) }
  .line           0 : { *(.line) }
  /* GNU DWARF 1 extensions */
  .debug_srcinfo  0 : { *(.debug_srcinfo) }
  .debug_sfnames  0 : { *(.debug_sfnames) }
  /* DWARF 1.1 and DWARF 2 */
  .debug_aranges  0 : { *(.debug_aranges) }
  .debug_pubnames 0 : { *(.debug_pubnames) }
  /* DWARF 2 */
  .debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
  .debug_abbrev   0 : { *(.debug_abbrev) }
  .debug_line     0 : { *(.debug_line) }
  .debug_frame    0 : { *(.debug_frame) }
  .debug_str      0 : { *(.debug_str) }
  .debug_loc      0 : { *(.debug_loc) }
  .debug_macinfo  0 : { *(.debug_macinfo) }
  /* SGI/MIPS DWARF 2 extensions */
  .debug_weaknames 0 : { *(.debug_weaknames) }
  .debug_funcnames 0 : { *(.debug_funcnames) }
  .debug_typenames 0 : { *(.debug_typenames) }
  .debug_varnames  0 : { *(.debug_varnames) }
  /* DWARF 3 */
  .debug_pubtypes 0 : { *(.debug_pubtypes) }
  .debug_ranges   0 : { *(.debug_ranges) }
  .gnu.attributes 0 : { KEEP (*(.gnu.attributes)) }
  /DISCARD/ : { *(.note.GNU-stack) *(.gnu_debuglink) }
}
