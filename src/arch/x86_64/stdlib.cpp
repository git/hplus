/* stdlib.cpp - assembly implementation of standard library */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <processor.hpp>

// C++ only version, until we get better ones in assembly

extern "C"
{
  void*
  memcpy(void* dest, const void* source, size_t sz)
  {
    char* dst((char*) dest);
    char* src((char*) source);
    while (sz--)
      {
        *dst++ = *src++;
      }
    return dst;
  }
  void*
  memset(void* dest, int val, size_t sz)
  {
    unsigned char* dst((unsigned char*) dest);
    unsigned char v(val);
    while (sz--)
      {
        *dst++ = v;
      }
    return dst;
  }

  int
  strcmp(const char* s1, const char* s2)
  {
    while (*s1 == *s2 and *s1 and *s2)
      {
        ++s1;
        ++s2;
      }
    if (*s1 > *s2)
      return 1;
    if (*s2 > *s1)
      return -1;
    return 0;
  }
  size_t
  strlen(const char* c)
  {
    size_t cnt(0);
    while (*c)
      {
        ++cnt;
        ++c;
      }
    return cnt;
  }
  char*
  strchr(const char* str, int c) {
    while(*str) {
      if(*str == c)
        return (char*)str;
    }
    return NULL;
  }
  int
  strncmp(const char* s1, const char* s2, size_t size)
  {
    while (*s1 == *s2 and *s1 and *s2 and size)
      {
        ++s1;
        ++s2;
        --size;
      }
    if (*s1 > *s2)
      return 1;
    if (*s2 > *s1)
      return -1;
    return 0;

  }
}
