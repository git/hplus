/* paging.cpp - processor support for memory paging (implementation) */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <bits/addr_space_layout.h>
#include <bits/flags.h>
#include <stdlib.h>
#include <assert.h>
#include <malloc.hpp>
#include <mmu.hpp>
#include <global.hpp>
#include <panic.hpp>
#include <paging.hpp>

struct PageEntry
{
  bool present :1;
  bool read_write :1;
  bool user_space :1;
  bool write_through :1;
  bool cache_disable :1;
  bool accessed :1;
  bool dirty :1;
  bool huge :1;
  bool global :1;
  unsigned int padding :3;
  physical_addr_t address :40;
  unsigned int extra :11;
  bool execute_disable :1;
}__attribute__((packed));

struct PageDirectory
{
  PageEntry entries_phys[512];
  void*
  operator new(size_t sz);
  void
  operator delete(void*);
  void
  operator delete(void* at, size_t sz);
};

struct PDT : PageDirectory
{
  PageDirectory* entries_log[512];
  void
  operator delete(void*);
};

struct PDPT : PageDirectory
{
  PDT* entries_log[512];
  void
  operator delete(void*);
};

struct PML4 : PageDirectory
{
  PDPT* entries_log[512];
  void
  operator delete(void*);
};

/* Inside .boot, so it has a physical address */
extern PML4 initial_pml4;

/* Kernel address space */
static PDPT* kernel_pdpt;

static PDT mmu_pdt __attribute__((aligned(4096)));
// first pdt inside the pdpt
static PageDirectory mmu_first_pd __attribute__((aligned(4096)));

// 1 if used, 0 if not
static uint64_t
    allocator_bitmap[((((KMMU_END - KMMU_BEGIN) / (page_size)) + 63) / 64)];
static uint64_t* free_bitmap;

inline void
invalidateTLB(void* addr)
{
  asm volatile ("invlpg [%0]" : : "r" (addr));
}

void
PageDirectory::operator delete(void* at, size_t sz)
{
  uintptr_t addr((uintptr_t) at);
  if (!(addr >= KMMU_BEGIN and addr < KMMU_END)) // not an address we own, do nothing
    return;
  unsigned long page_pos((addr - KMMU_BEGIN) / page_size);
  unsigned long view_index(page_pos / 64);
  unsigned int pd_index(view_index / 8);
  unsigned int page_index(page_pos % 512);
  if (page_index == 511) // don't free page directories used for other pages
    return;
  if (sz == page_size)
    allocator_bitmap[view_index] &= ~(1 << (page_pos % 64));
  else
    allocator_bitmap[view_index] &= ~(0b11 << (page_pos % 64));
  if (size_t(free_bitmap - allocator_bitmap) > view_index)
    free_bitmap = allocator_bitmap + view_index;
  PageDirectory* pd(mmu_pdt.entries_log[pd_index]);
  assert(pd != NULL);
  PageEntry& entry(pd->entries_phys[page_index]);
  physical_addr_t curaddr(entry.address << 12);
  GlobalStorage& gs(Processor::getGlobalStorage());
  MMU& mem(gs.getMMU());
  mem.releaseFreePage(curaddr);
  entry.present = false;
  entry.address = 0;
  invalidateTLB(at);
}
void*
PageDirectory::operator new(size_t sz)
{
  bool looping(false);
  size_t* view(free_bitmap);
  while (true)
    {
      while (*view == 0xFFFFFFFFFFFFFFFF)
        {
          ++view;
          if (view == allocator_bitmap + (sizeof(allocator_bitmap)
              / sizeof(size_t)))
            panic("End of MMU reserved space reached");
        }
      if (!looping)
        free_bitmap = view;
      for (int pos(__builtin_ffsl(~(*view)) - 1); pos < 64; pos++)
        {
          unsigned long view_index((view - allocator_bitmap));
          uintptr_t curaddr(KMMU_BEGIN + (view_index * 64 + pos) * page_size);
          unsigned int pd_index(view_index / 8);
          unsigned int page_index((view_index % 8) * 64 + pos);
          if (page_index == 511)
            {
              /* don't fill the last page in the directory
               instead use it for a new directory */
              GlobalStorage& gs(Processor::getGlobalStorage());
              MMU& mem(gs.getMMU());
              physical_addr_t pl(mem.getFreePage());
              mmu_pdt.entries_log[pd_index] = (PageDirectory*) curaddr;
              PageEntry& cur_entry(mmu_pdt.entries_phys[pd_index]);
              cur_entry.address = pl >> 12;
              cur_entry.execute_disable = false;
              cur_entry.global = true;
              cur_entry.read_write = true;
              cur_entry.present = true;
              *view |= (1 << pos);
              return PageDirectory::operator new(sz);
            }
          if (page_index == 510 and sz == 2 * page_size)
            {
              /* this would overwrite the reserved page, move on;
               * next iteration, if page exists, we can move
               * to the following view, else we add a page directory
               */
              continue;
            }
          if (sz == 2 * page_size && (*view & (1 << (pos + 1))) == 0)
            {
              // we have space for exactly one page here
              continue;
            }
          // finally, we've found what we need
          if (sz == 2 * page_size)
            {
              GlobalStorage& gs(Processor::getGlobalStorage());
              MMU& mem(gs.getMMU());
              physical_addr_t pl1(mem.getFreePage());
              physical_addr_t pl2(mem.getFreePage());
              PageDirectory * pd(mmu_pdt.entries_log[pd_index]);
              assert(pd != NULL);
              PageEntry &e1(pd->entries_phys[page_index]), &e2(
                  pd->entries_phys[page_index + 1]);
              e1.address = pl1 >> 12;
              e2.address = pl2 >> 12;
              e1.global = e2.global = true;
              e1.read_write = e2.read_write = true;
              e1.execute_disable = e2.execute_disable = false;
              e1.present = e2.present = true;
              *view |= (0b11 << pos);
              return (void*) (curaddr);
            }
          else
            {
              assert(sz == page_size);
              GlobalStorage& gs(Processor::getGlobalStorage());
              MMU& mem(gs.getMMU());
              physical_addr_t pl(mem.getFreePage());
              PageDirectory * pd(mmu_pdt.entries_log[pd_index]);
              assert(pd != NULL);
              PageEntry& e(pd->entries_phys[page_index]);
              e.address = pl >> 12;
              e.global = true;
              e.read_write = true;
              e.execute_disable = false;
              e.present = true;
              *view |= (1 << pos);
              return (void*) (curaddr);
            }
        }
      // No page in the current view is good, go to next view
      view++;
    }
}
void
PageDirectory::operator delete(void* addr)
{
  PageDirectory::operator delete(addr, page_size);
}
void
PDT::operator delete(void* addr)
{
  PDT* self(static_cast<PDT*> (addr));
  for (int i(0); i < 512; ++i)
    {
      if (self->entries_log[i] != NULL)
        {
          self->entries_phys[i].present = false;
          delete self->entries_log[i];
        }
    }
  PageDirectory::operator delete(addr, 2 * page_size);
}
void
PDPT::operator delete(void* addr)
{
  PDPT* self(static_cast<PDPT*> (addr));
  for (int i(0); i < 512; ++i)
    {
      if (self->entries_log[i] != NULL)
        {
          self->entries_phys[i].present = false;
          delete self->entries_log[i];
        }
    }
  PageDirectory::operator delete(addr, 2 * page_size);
}
void
PML4::operator delete(void* addr)
{
  PML4* self(static_cast<PML4*> (addr));
  for (int i(0); i < 512; ++i)
    {
      if (self->entries_log[i] != NULL and !(self->entries_phys[i].global))
        {
          self->entries_phys[i].present = false;
          delete self->entries_log[i];
        }
    }
  PageDirectory::operator delete(addr, 2 * page_size);
}

PageTable::PageTable(bool new_pml4)
{
  if (new_pml4)
    {
      this->pml4 = new PML4;
      this->pml4->entries_log[511] = kernel_pdpt;
      PageEntry& pdpt(this->pml4->entries_phys[511]);
      pdpt.present = true;
      pdpt.read_write = true;
      pdpt.global = true;
      pdpt.address = this->getPage(kernel_pdpt) >> 12;
      this->cr3 = this->getPage(this->pml4) >> 12;
    }
  else
    {
      this->cr3 = (physical_addr_t(&initial_pml4) & CR3_ADDRESS_MASK);
      this->pml4 = (PML4*) (physical_addr_t(&initial_pml4) - TEXT_SEGMENT_MIN
          + KERNELSPACE_BEGIN);
      // drop boot pages
      // this->pml4->entries_phys[0].present = false;
      kernel_pdpt = (this->pml4->entries_log[511]);
      // enable our private space

        {
          kernel_pdpt->entries_log[0] = &mmu_pdt;
          PageEntry& cur_entry(kernel_pdpt->entries_phys[0]);
          cur_entry.address = this->getPage(&mmu_pdt) >> 12;
          cur_entry.execute_disable = false;
          cur_entry.read_write = true;
          cur_entry.global = true;
          cur_entry.present = true;
        }
        {
          mmu_pdt.entries_log[0] = &mmu_first_pd;
          PageEntry& cur_entry(mmu_pdt.entries_phys[0]);
          cur_entry.address = this->getPage(&mmu_first_pd) >> 12;
          cur_entry.execute_disable = false;
          cur_entry.read_write = true;
          cur_entry.global = true;
          cur_entry.present = true;
        }
      free_bitmap = allocator_bitmap;
    }
}
void
PageTable::moveToKernel() {
  for(int i(0);i<256;++i) {
    PageEntry& e(this->pml4->entries_phys[i]);
    e.address = 0;
    e.present = false;
    this->pml4->entries_log[i] = NULL;
  }
}
PageTable::~PageTable()
{
  delete this->pml4;
}

struct LogicalAddress
{
  LogicalAddress(uint64_t u) :
    offset(u & 0xFFF), table((u & 0x3FF00) >> 12), directory((u & 0x7FE00000)
        >> 21), dir_ptr((u & 0xFFC0000000) >> 30), pml4((u & 0x1FF8000000000)
        >> 39)
  {
    return;
  }
  unsigned int offset :12;
  unsigned int table :9;
  unsigned int directory :9;
  unsigned int dir_ptr :9;
  unsigned int pml4 :9;
  unsigned int reserved :16;
}__attribute__((packed));

void
PageTable::setPage(void* log, physical_addr_t physical, bool present,
    unsigned int acl)
{
  PageEntry* cur_entry = this->getPageEntry(log, true);
  cur_entry->address = physical >> 12;
  cur_entry->read_write = (acl & PageTable::write) == PageTable::write;
  //cur_entry->execute_disable = !(acl & PageTable::execute);
  cur_entry->execute_disable = false;
  cur_entry->present = present;
  //cur_directory->entries_log[l.table] = (PageDirectory*) log;
  invalidateTLB(log);
}

physical_addr_t
PageTable::getPage(void* log)
{
  PageEntry* cur_entry = this->getPageEntry(log, false);
  assert(cur_entry != NULL);
  if (cur_entry->huge)
    return (((cur_entry->address) << 12) + (uintptr_t(log) % 0x200000));
  else
    return (cur_entry->address) << 12;
}

PageEntry*
PageTable::getPageEntry(void* log, bool modify)
{
  LogicalAddress l((uint64_t) log);
  PML4* pml4(this->pml4);
  PDPT*& pdpt(pml4->entries_log[l.pml4]); // to pdpt
  PageEntry& pdpt_entry(pml4->entries_phys[l.pml4]);
  if (pdpt == NULL)
    {
      if (!modify)
        return NULL;
      pdpt = new PDPT;
      memset(pdpt, 0, sizeof(PDPT));
      pdpt_entry.address = (this->getPage(pdpt)) >> 12;
      pdpt_entry.read_write = true;
      if (log >= (void*) USER_END)
        pdpt_entry.global = true;
      pdpt_entry.present = true;
    }
  PageEntry& pdt_entry = pdpt->entries_phys[l.dir_ptr];
  PDT*& pdt = pdpt->entries_log[l.dir_ptr];
  if (pdt == NULL)
    {
      if (!modify)
        return NULL;
      pdt = new PDT;
      memset(pdt, 0, sizeof(PDT));
      pdt_entry.address = (this->getPage(pdt)) >> 12;
      pdt_entry.read_write = true;
      if (log >= (void*) USER_END)
        pdt_entry.global = true;
      pdt_entry.present = true;
    }
  PageEntry& pd_entry = pdt->entries_phys[l.directory];
  if (pd_entry.present && pd_entry.huge)
    {
      if (modify)
        panic("Tried to modify a 2MB page");
      else
        return &pd_entry;
    }
  PageDirectory*& final_dir = pdt->entries_log[l.directory];
  if (final_dir == NULL)
    {
      if (!modify)
        return NULL;
      final_dir = new PageDirectory;
      memset(final_dir, 0, sizeof(PageDirectory));
      pd_entry.address = (this->getPage(final_dir)) >> 12;
      pd_entry.read_write = true;
      if (log >= (void*) USER_END)
        pd_entry.global = true;
      pd_entry.present = true;
    }
  return final_dir->entries_phys + l.table;
}

void
PageTable::load()
{
  physical_addr_t cr3(this->cr3);
  asm volatile("mov cr3,%0" : "=r" (cr3));
}
