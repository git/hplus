/* kernel-sync.cpp - short file description */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <kernel-sync.hpp>

void
mutex_lock(mutex_t* lock)
{
  while (true)
    {
      while (*lock)
        {
          asm("pause");
        }
      if(__sync_lock_test_and_set(lock,true)==0)
        return;
    }
}

void
mutex_unlock(mutex_t* lock)
{
  __sync_lock_release(lock);
}
