/* paging.hpp - processor support for memory paging */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef PAGING_HPP_
#define PAGING_HPP_

#include <bits/flags.h>
#include <architecture.hpp>
#include <stdlib.h>

typedef cr_t paging_cr_t;

struct PML4;
struct PageEntry;

class PageTable
{
public:
  enum access_control
  {
    read = 0x0, write = 0x1, execute = 0x2, acl_mask = 0x3
  };
  PageTable(bool copy_pml4);
  PageTable(const PageTable& pt);
  ~PageTable();
  void
  setPage(void* logical, physical_addr_t physical, bool present, unsigned int acl);
  physical_addr_t
  getPage(void* address);
  void
  load();
  void
  moveToKernel();
private:
  paging_cr_t cr3;
  PML4* pml4;
  PageEntry*
  getPageEntry(void* logical,bool modify);
  physical_addr_t
  dropPage(void* address);
};
#endif /* PAGING_HPP_ */
