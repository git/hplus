/* bootinfo.hpp - version independent interface to multiboot */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef BOOTINFO_HPP_
#define BOOTINFO_HPP_

#include <bits/multiboot.h>
#include <architecture.hpp>

typedef multiboot_info* opaque_boot_info;

class BootInfo {
public:
  class MemoryMap {
  public:
    MemoryMap(multiboot_mmap_entry* begin,multiboot_mmap_entry* end) : _start(begin), _end(end) {
      return;
    }
    class iterator {
    public:
      iterator(multiboot_mmap_entry* e) : me(e) {
        return;
      }
      physical_addr_t begin() {
        return physical_addr_t(this->me->addr);
      }
      physical_addr_t end() {
        return physical_addr_t(this->me->addr + this->me->len);
      }
      bool available() {
        return (this->me->type == MULTIBOOT_MEMORY_AVAILABLE);
      }
      iterator& operator++() {
        ++(this->me);
        return *this;
      }
      iterator& operator--() {
        --(this->me);
        return *this;
      }
      bool operator==(const iterator& e) {
        return this->me == e.me;
      }
      bool operator!=(const iterator& e) {
        return !(*this == e);
      }
    private:
      multiboot_mmap_entry* me;
    };
    iterator begin() {
      return iterator(this->_start);
    }
    iterator end() {
      return iterator(this->_end);
    }
  private:
    multiboot_mmap_entry* _start;
    multiboot_mmap_entry* _end;
  };
  class InitRamFS {
  public:
    InitRamFS(physical_addr_t begin,physical_addr_t end);
    void* begin() {
      return _begin;
    }
    void* end() {
      return _end;
    }
  private:
    void* _begin;
    void* _end;
  };
  BootInfo(unsigned long magic,opaque_boot_info i);
  MemoryMap& getMemoryMap() {
    return this->memory;
  }
  InitRamFS getInitRd() {
    return InitRamFS(initrd_begin,initrd_end);
  }
  char* getCmdLine() {
    return this->cmdline;
  }
  physical_addr_t getROMConfig() {
    return this->rom;
  }
private:
  MemoryMap memory;
  physical_addr_t initrd_begin;
  physical_addr_t initrd_end;
  physical_addr_t rom;
  char* cmdline;
};

#endif /* BOOTINFO_HPP_ */
