/* syscall.hpp - system call interface */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef SYSCALL_HPP_
#define SYSCALL_HPP_

#include <errno.h>

class Thread;

class SysCallIface
{
public:
  typedef err_t (*syscall_handler)(Thread*,unsigned long arg[6]);
  enum system_call_no
    {
      // file management
      open,
      read,
      write,
      //truncate,
      //allocate,
      lseek,
      sync,
      fsync,
      //select,
      poll,
      close,
      ioctl,
      fcntl,

      // filesystem
      stat,
      fstat,
      statfs,
      fstatfs,
      link,
      flink,
      symlink,
      unlink,
      utimes,
      rename,
      access,
      chmod,
      fchmod,
      chown,
      fchown,
      chroot,
      chdir,
      fchdir,
      getcwd,
      mknod,

      // memory
      mmap,
      mremap,
      munmap,
      msync,
      mlock,
      mlockall,
      munlock,
      munlockall,
      mmerge,
      sbrk,

      // process control
      fork,
      exit,
      wait4,
      execve,
      sleep,
      nanosleep,

      // signals
      kill,
      sigaction,
      sigprocmask,
      siggetmask,
      sigsuspend,
      sigtimedwait,
      sigreturn,

      // thread control
      thread_create,
      thread_getid,
      thread_cancel,
      thread_kill,
      thread_acquirelock,

      // process table and security
      proc_getjobinfo,
      proc_setjobinfo,
      proc_getgroups,
      proc_setgroups,
      proc_getuid,
      proc_setuid,
      proc_setcap,
      proc_setumask,

      // scheduler control
      sched_getcpu,
      sched_getpriority,
      sched_setpriority,
      sched_getaffinity,
      sched_setaffinity,

      // timers
      getitimer,
      setitimer,
      alarm,
      timer_create,
      timer_delete,
      timer_settime,
      timer_gettime,

      // system configuration
      getrlimit,
      setrlimit,
      swapon,
      swapoff,

      // time and date
      clock_gettime,
      clock_settime,
      clock_nanosleep

    };
  SysCallIface();
  ~SysCallIface();
  void
  setSystemCall(system_call_no id,syscall_handler h);
  void
  unsetSystemCall(system_call_no id);
};

#endif /* SYSCALL_HPP_ */
