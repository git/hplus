/* processor.hpp - processor management and initialization */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef PROCESSOR_HPP_
#define PROCESSOR_HPP_

#include <bits/flags.h>
#include <architecture.hpp>
#include <task.hpp>
#include <stddef.h>

class SMP;
class Processor;
class TaskStateSegment;
class GlobalStorage;

class Processor
{
  friend class SMP;
  struct LocalStorage {
    unsigned long id; // processor id
    Task* t; // the original task
    TaskStateSegment* tss; // to use in system calls
    GlobalStorage* glob; // info common to all processors
  };
public:
  typedef void
  (*exception_handler_t)();
  enum interrupt_code
  {
    divide_error = 0,
    overflow = 4,
    array_out_of_bounds = 5,
    invalid_instruction = 6,
    fpu_not_available = 7,
    double_fault = 8,
    invalid_task = 10,
    invalid_segment = 11,
    stack_fault = 12,
    general_protection = 13,
    page_fault = 14,
    fpu_error = 16,
    align_error = 17,
    machine_check = 18,
    simd_error = 19,
    interrupt_start = 23,
    rescheduling_irq = 23,
    timer_irq = 24,
    system_call_fallback = 80,
    interrupt_end = 256
  };
  void
  setInterrupt(interrupt_code c, exception_handler_t);
  void
  unsetInterrupt(interrupt_code c);
  void
  sendIRQ(interrupt_code c);
  static inline void
  suspend(void)
  {
    asm("hlt");
  }
  static inline void
  disable_interrupts()
  {
    asm("cli");
  }
  static inline void
  enable_interrupts()
  {
    asm("sti");
  }
  static inline bool interrupts_enabled() {
    uint64_t flags;
    asm("pushf; pop %0" : "=r" (flags));
    return (flags & FLAG_IF);
  }
  static inline Processor&
  self() {
    return *((Processor*)(Processor::read_msr(MSR_GS_BASE)));
  }
  static inline
  Task& getTask() {
    Task* t;
    asm ("mov %0,gs:%1" : "=r" (t) : "i" ((offsetof(LocalStorage,t))));
    return *t;
  }
  static inline
  void ud() {
    asm ("ud2");
  }
  static inline
  GlobalStorage& getGlobalStorage() {
    GlobalStorage* gs;
    asm ("mov %0,gs:%1" : "=r" (gs) : "i" ((offsetof(LocalStorage,glob))));
    return *gs;
  }
  bool operator==(const Processor& p) const {
    return this->ls.id == p.ls.id;
  }
  bool operator!=(const Processor& p) const {
    return !(*this == p);
  }
  static
  uint64_t time() {
    uint64_t res;
    asm("lfence; rdtsc" : "=A" (res));
    return res;
  }
private:
  Processor(GlobalStorage&,unsigned int);
  static void
  write_msr(uint32_t regid,uint64_t val) {
    uint32_t v1(val & 0xFFFFFFFF);
    uint32_t v2(val >> 32);
    asm volatile("wrmsr" : : "c" (regid), "d" (v2), "a" (v1));
  }
  static uint64_t
  read_msr(uint32_t regid) {
    uint64_t val;
    asm ("rdmsr" : "=A" (val) : "c" (regid));
    return val;
  }
  LocalStorage ls;
};

class SMP {
public:
  SMP(GlobalStorage& gs);
  void
  addProcessor();
private:
  unsigned int proc_cnt;
  Processor* procs;
};

#endif /* PROCESSOR_HPP_ */
