/* architecture.hpp - architecture dependent types and constants */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef ARCHITECTURE_HPP_
#define ARCHITECTURE_HPP_

#ifdef ASM_FILE

#define jump_to_symbol(x) jmp x

#else

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;
typedef signed char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long int64_t;

typedef uint64_t physical_addr_t;
typedef uint64_t uintptr_t;
typedef uint64_t size_t;
typedef uint64_t offset_t;
typedef uint64_t cr_t; // control register
typedef int64_t ptrdiff_t;
typedef int64_t ssize_t;

typedef uint64_t uint128_t __attribute__((vector_size(128)));
typedef uint64_t uint256_t __attribute__((vector_size(256)));
const size_t page_size = 4096;
const size_t huge_page_size = 2*1024*1024;

#endif /* ASM_FILE */
#endif /* ARCHITECTURE_HPP_ */
