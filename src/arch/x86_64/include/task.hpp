/* task.hpp - task management */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef TASK_HPP_
#define TASK_HPP_

#ifdef ASM_FILE

#define SIZEOF_TASK 672

#else

#include <architecture.hpp>
#include <paging.hpp>

class Task {
public:
  Task() {
    paging_cr_t cr3;
    asm("mov %0,cr3" : "=r" (cr3));
    this->cr3 = cr3;
  }
  paging_cr_t getPagingCR() {
    return this->cr3;
  }
  void* getProgramCounter() {
    return this->rip;
  }
  void dispatch(void);
  void syscallReturn(uint64_t value);
  void* getStack() const {
    return stack;
  }
private:
  enum registers { rax, rbx, rcx, rdx, rbp, rsi, rdi, r8, r9, r10, r11, r12, r13, r14 };
  enum segments { cs, ss };
  paging_cr_t cr3;
  void* stack;
  void* rip;
  uint64_t general_registers[15];
  uint64_t flags;
  uint16_t segments[2];
  uint8_t fpu_state[512] __attribute__((aligned(16)));
};

#define SIZEOF_TASK sizeof(Task)

#endif /* ASM_FILE */
#endif /* TASK_HPP_ */
