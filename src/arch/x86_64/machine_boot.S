/* boot.S - bootstrap the kernel */
/* Copyright (C) 1999, 2001 Free Software Foundation, Inc.
                 2009 Giovanni Campagna

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

#define ASM_FILE        1
#include <bits/multiboot.h>
#include <bits/flags.h>
#include <bits/addr_space_layout.h>

		 .code32
		 .section ".boot","awx"
		 .type bootstrap,@function
		 .globl bootstrap
bootstrap:
         /* Place parameters where required by ABI */
         mov     %ebx,%esi
         mov     %eax,%edi

		 /* Begin entering 64-bit mode */
		 cli
		 lgdt	 initial_gdt
		 mov     $KERNEL_DS, %edx
		 mov     %edx, %ss
		 ljmp    *(startup_32_pointer)

startup_32:
		 mov     $(PAGE_PRESENT | PAGE_READWRITE), %ebx
		 orl	 %ebx, initial_pml4
		 orl     %ebx, (initial_pml4+8*511)
		 orl	 %ebx, (pdpt_low)
		 orl     %ebx, (pdt_high)
		 mov     $(PAGE_PRESENT | PAGE_READWRITE | PAGE_GLOBAL), %ebx
		 orl     %ebx, (pdpt_high+8*510)
		 orl     %ebx, (pdt_high+8)
		 movl	 $initial_pml4, %eax
		 mov	 %eax, %cr3
		 mov	 %cr4, %eax
		 orl	 $(CR4_TIMESTAMP_DISABLE | CR4_PAGE_GLOBAL_ENABLE | CR4_DEBUG_EXT | CR4_PAE), %eax
		 mov	 %eax, %cr4
		 mov	 $MSR_EFER, %ecx
		 rdmsr
		 orl	 $MSR_EFER_LME, %eax
		 wrmsr
		 mov	 %cr0, %eax
		 orl     $(CR0_PAGING | CR0_WRITE_PROTECT | CR0_ALIGNMENT_MASK | CR0_PROTECTED_MODE), %eax
		 mov	 %eax, %cr0

		 lgdt	 initial_gdt
		 mov     $KERNEL_DS, %edx
		 mov     %edx, %ss
		 ljmp	 *(startup_64_pointer)

		 .code64
startup_64:
		 mov	 %edx,%ss
		 mov 	 %edx,%ds
		 mov	 %edx,%es
		 mov	 %edx,%fs
		 mov     %edx,%gs
		 xor     %edx,%edx
		 lldt	 %dx
		 movabsq $(stack + STACK_SIZE), %rsp
		 lidt    idtr

		 movabsq $secondary_startup_64,%rbp
		 jmp	 *%rbp

startup_32_pointer:
		 .long   startup_32
		 .short  KERNEL_CS32
startup_64_pointer:
		 .long	 startup_64
		 .short  KERNEL_CS

/* Segmentation data structures */
		 .align  4
initial_gdt:
		 .word   initial_gdt_end - initial_gdt
		 .long   initial_gdt
		 .word   0
		 /* Kernel comp. code segment */
		 .long	 SEGMENT_LOW_DWORD
		 .long	 (SEGMENT_HIGH_DWORD | SEGMENT_CODE_32)
		 /* Kernel code segment */
		 .long	 SEGMENT_LOW_DWORD
		 .long	 (SEGMENT_HIGH_DWORD | SEGMENT_CODE_64)
         /* Kernel data segment */
         .long	 SEGMENT_LOW_DWORD
         .long	 (SEGMENT_HIGH_DWORD | SEGMENT_DATA)
initial_gdt_end:
		 .size	initial_gdt, (initial_gdt_end - initial_gdt)

/* Interrupt Description Table Register */
idtr:
        .word	0
        .quad	0

/* Paging structures */
		.globl initial_pml4
		.align	4096
initial_pml4:
		/* First 512GB block, identity mapped pages */
		.quad   pdpt_low
		/* The rest of address space, not yet used */
		.fill   510, 8, 0x0
		/* Kernel address space */
		.quad   pdpt_high

		/* Logical addresses of the pdpts */
		.fill 	511, 8, 0x0
		.quad   (pdpt_high - TEXT_SEGMENT_MIN + KERNELSPACE_BEGIN)

		.size pml4, .-pml4

		.align 	4096
pdpt_low:
		/* First 1GB block */
		.quad	pdt_low
		/* The remaining 511 GBs */
		.fill 	511, 8, 0x0
		.size pdpt_low, .-pdpt_low

.macro  huge_page at:req, max:req
.quad   (PAGE_PRESENT | PAGE_HUGE | PAGE_READWRITE | \at)
.if     \max-\at > 0
huge_page     (\at + 0x200000),\max
.endif
.endm

		.align 	4096
pdt_low:
		/* Identity maps for bootstrapping */
		huge_page LOW_MAPPED_MIN,LOW_MAPPED_MAX
		/* The remaining entries are null */
		.fill	(512 - ((LOW_MAPPED_MAX - LOW_MAPPED_MIN) / 0x200000))-1 , 8, 0x0
		.size pdt_low, .-pdt_low

		.align 4096
pdt_high:
		/* Text, data and malloc space just to get started */
		huge_page INIT_MAPPED_MIN,INIT_MAPPED_MAX
		/* Other pages / page tables will be added as needed */
		.fill   (512 - ((INIT_MAPPED_MAX - INIT_MAPPED_MIN) / 0x200000))-1, 8, 0x0

		/* Logical addresses of the page directories
		   They're all null as we only have huge pages
		*/
		.fill 	512, 8, 0x0
		.size pdt_high, .-pdt_high

		.align 4096
pdpt_high:
		/* First 510 GBs */
		.fill	510, 8, 0x0
		/* Last 2 GBs */
		.quad   pdt_high
		.quad 	0
		/* Logical addresses of the pdts */
		.fill 	510, 8, 0x0
		.quad   (pdt_high - TEXT_SEGMENT_MIN + KERNELSPACE_BEGIN)
		.quad   0
		.size pdpt_high, .-pdpt_high

		 .text
secondary_startup_64:

		 push	 $0
		 popfq

         /* Sign extend parameters */
         movslq	 %edi,%rdi
         movslq  %esi,%rsi

		 /* Stop stack unwinding */
		 push    $0
         /* Now enter the C main function... */
         call    cmain

         .data
         .globl  gdt
gdt:
		 .long   gdt_end-gdt
		 .long   0
		 /* Kernel comp. code segment */
		 .long	 SEGMENT_LOW_DWORD
		 .long	 (SEGMENT_HIGH_DWORD | SEGMENT_CODE_32)
		 /* Kernel code segment */
		 .long	 SEGMENT_LOW_DWORD
		 .long	 (SEGMENT_HIGH_DWORD | SEGMENT_CODE_64)
         /* Kernel data segment */
         .long	 SEGMENT_LOW_DWORD
         .long	 (SEGMENT_HIGH_DWORD | SEGMENT_DATA)
         /*	64-bit userspace code segment */
         .long   SEGMENT_LOW_DWORD
         .long 	 (SEGMENT_HIGH_DWORD | SEGMENT_CODE_64 | SEGMENT_USERSPACE)
         /* 32-bit userspace code segment */
         .long	 SEGMENT_LOW_DWORD
         .long 	 (SEGMENT_HIGH_DWORD | SEGMENT_CODE_32 | SEGMENT_USERSPACE)
         /* Userspace data segment */
         .long 	 SEGMENT_LOW_DWORD
         .long	 (SEGMENT_HIGH_DWORD | SEGMENT_DATA | SEGMENT_USERSPACE)
         /* Task State Segment */
         .globl tss_descr
tss_descr:
         .long   SEGMENT_LOW_DWORD
         .long   (SEGMENT_TSS)
         .long   0x0
         .long   0x0
         /* Local storage segment */
		 .globl	ls_descr
		 .quad   0x0
gdt_end:
		 .size gdt,gdt_end-gdt
		 .size tss_descr,gdt_end-tss_descr

