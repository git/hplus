/* addr_space_layout.h - address space layout on x86-64 */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef ADDR_SPACE_LAYOUT_H_
#define ADDR_SPACE_LAYOUT_H_

/* Virtual addresses */

/* NULL addresses, with a guarantee that nothing is there */
#define NULL_BEGIN          0x0000000000000000
#define NULL_END            0x0000000000000FFF

/* Userspace
 * TODO: add separation for text, data, mmap and shared libraries
 *       (that could be done in /lib64/ld64-gnu.so.1)
 */
#define USER_BEGIN          0x0000000000001000
#define USER_TEXT_BEGIN     0x0000000000400000
#define USER_DYN_BEGIN      0x0000080000000000
#define USER_END            0x00007FFFFFFFFFFF

/* Kernel space */
#define KERNELSPACE_RSRVD   0xFFFFFFFF80000000
#define KERNELSPACE_BEGIN   0xFFFFFFFF80200000
#if !(defined(ASM_FILE) || defined(LD_SCRIPT))
extern char _boot_end;
extern char _end;
extern char _bss_end;
#define KTEXT_BEGIN         (KERNELSPACE_BEGIN - TEXT_SEGMENT_MIN + uintptr_t(&_boot_end))
#define KTEXT_END           (uintptr_t(&_bss_end))
#else
#define KTEXT_BEGIN         (_boot_end - TEXT_SEGMENT_MIN + KERNELSPACE_BEGIN)
#define KTEXT_END           (_bss_end)
#endif
#define KMALLOC_END         0xFFFFFFFF8F000000
#define KMMAP_BEGIN         KMALLOC_END
#define KMMAP_END           0xFFFFFFFFFF000000
#define KERNELSPACE_END     0xFFFFFFFFFFFFF000 // (the last page)
/* We use the low part of high addresses for placing page tables
   (beyond those already present in the kernel image) */
#define KMMU_BEGIN          0xFFFFFF8000000000
#define KMMU_END            0xFFFFFF8100000000

/* Physical addresses */
#define TEXT_SEGMENT_MIN    0x00200000
#define BOOT_LOAD           (TEXT_SEGMENT_MIN + 0x1000)
#if !(defined(ASM_FILE) || defined(LD_SCRIPT))
#define TEXT_LOAD           (physical_addr_t(&_boot_end))
#define TEXT_SEGMENT_END    (physical_addr_t(uintptr_t(&_bss_end) + TEXT_SEGMENT_MIN))
#endif

/* 10 MBs (2 for reserved memory areas, 6 for mirroring high addresses,
 * 2 for multiboot structures
 */
#define LOW_MAPPED_MIN      0x00000000
#define LOW_MAPPED_MAX      0x00A00000

/* 6 MBs (1 for text, 3 for data+bss, 2 for initial malloc space) */
#define INIT_MAPPED_MIN     0x00000000
#define INIT_MAPPED_MAX     (INIT_MAPPED_MIN + 0x600000)

/* all memory we think is available, is memory we own */
#define BSS_SIZE            (INIT_MAPPED_MAX - INIT_MAPPED_MIN)
#endif /* ADDR_SPACE_LAYOUT_H_ */
