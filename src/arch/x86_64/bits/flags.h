/* flags.h - architecture dependent flags */
/* Copyright (C) 2010 Giovanni Campagna

   This file is part of the H+ kernel, a microkernel for the GNU system.      

   H+ is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef FLAGS_H_
#define FLAGS_H_

/* The size of our stack (16KB). */
#define STACK_SIZE              0x4000
#define BOOT_STACK_SIZE         0x2000

#define CR0_PAGING              0x80000000
#define CR0_CACHE_DISABLE       0x40000000
#define CR0_NOT_WRITE_THROUGH   0x20000000
#define CR0_ALIGNMENT_MASK      0x00040000
#define CR0_WRITE_PROTECT       0x00010000
#define CR0_NUMERIC_ERROR       0x00000020
#define CR0_EXTENSION_TYPE      0x00000010
#define CR0_FPU_TASK_SWITCHED   0x00000008
#define CR0_FPU_EMULATION       0x00000004
#define CR0_MONITOR_COPROCESSOR 0x00000002
#define CR0_PROTECTED_MODE      0x00000001

#define CR4_VIRTUAL8086         0x00000001
#define CR4_VIRTUALINTERRUPTS   0x00000002
#define CR4_TIMESTAMP_DISABLE   0x00000004
#define CR4_DEBUG_EXT           0x00000008
#define CR4_PAGE_SIZE_EXT       0x00000010
#define CR4_PAE                 0x00000020
#define CR4_MC_ENABLE           0x00000040
#define CR4_PAGE_GLOBAL_ENABLE  0x00000080
#define CR4_PERF_COUNTER        0x00000100
#define CR4_FXSTOR_SUPPORT      0x00000200
#define CR4_SIMD_EXCEPT_SUPPORT 0x00000400
#define CR4_VMX_ENABLE          0x00002000
#define CR4_SMX_ENABLE          0x00004000
#define CR4_XSTOR_SUPPORT       0x00040000

#define SEGMENT_USERSPACE       0x00006000
#define SEGMENT_HIGH_DWORD      0x008F8000
#define SEGMENT_LOW_DWORD       0x0000FFFF
#define SEGMENT_DATA            0x00001300
#define SEGMENT_CODE_32         0x00401900
#define SEGMENT_CODE_64         0x00201900
#define SEGMENT_READCODE        0x00000200
#define SEGMENT_LDT             0x00C0E200
#define SEGMENT_TSS             0x00008900
#define SS_LDT                  0x00000004
#define SS_USERSPACE            0x00000003
#define KERNEL_CS32             (1 << 3)
#define KERNEL_CS               (2 << 3)
#define KERNEL_DS               (3 << 3)
#define KERNEL_TSSSEL           (7 << 3)

#define CR3_CACHE_DISABLE       0x00000010
#define CR3_WRITE_THROUGH       0x00000008
#define CR3_ADDRESS_MASK_HIGH   0x0000FFFF
#define CR3_ADDRESS_MASK_LOW    0xFFFFF000
#define CR3_ADDRESS_MASK        0x0000FFFFFFFFF000

#define PAGE_PRESENT            0x00000001
#define PAGE_READWRITE          0x00000002
#define PAGE_USERSPACE          0x00000004
#define PAGE_WRITE_THROUGH      0x00000008
#define PAGE_CACHE_DISABLE      0x00000010
#define PAGE_HUGE               0x00000080
#define PAGE_GLOBAL             0x00000100
#define PAGE_ADDRESS_MASK_HIGH  0x000FFFFF
#define PAGE_ADDRESS_MASK_LOW   0xFFFFF000
#define PAGE_EXECUTE_DISABLE    0x80000000

#define IDTE_PRESENT            0x00008000
#define IDTE_INT_GATE           0x00000E00
#define IDTE_TRAP_GATE          0x00000F00

#define MSR_EFER                0xC0000080
#define MSR_EFER_LME            0x00000100
#define MSR_EFER_LMA            0x00000400
#define MSR_FS_BASE             0xC0000100
#define MSR_GS_BASE             0xC0000101
#define MSR_KERNEL_GS_BASE      0xC0000102

#define FLAG_IF                 0x0000000000000200

#define PAGEFAULT_PROTECTION    0x00000001

#endif /* FLAGS_H_ */
