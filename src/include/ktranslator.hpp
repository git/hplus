/* ktranslator.hpp - kernel mode filesystem translator */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef KTRANSLATOR_HPP_
#define KTRANSLATOR_HPP_

#include <filesystem.hpp>

class KTranslator {
public:
  virtual err_t linkstat(Filesystem::Node& parent,const char* name,size_t len,Filesystem::Link& out) = 0;
  virtual err_t nodestat(Filesystem::Node& who,stat* out);
  virtual err_t link(Filesystem::Node& parent,const char* name,size_t namelen,Filesystem::Node& child) = 0;
  virtual err_t unlink(Filesystem::Node& parent,const char* name,size_t namelen) = 0;
  virtual err_t create(Filesystem::Node& parent,uint8_t type,ino_t* out) = 0;
  virtual err_t open(uint32_t& cid,Filesystem::Node& node,int flags,mode_t mode) = 0;
  virtual err_t close(uint32_t cid);
  virtual err_t read(uint32_t cid,void* at,size_t& len,off_t file_off) = 0;
  virtual err_t write(uint32_t cid,void* at,size_t& len,off_t file_off) = 0;
  virtual err_t fdctl(uint32_t cid,uint32_t& out,uint32_t cmd,...) = 0;
  virtual err_t ioctl(uint32_t cid,uint32_t& out,uint32_t cmd,...) = 0;
};

#endif /* KTRANSLATOR_HPP_ */
