/* mmu.hpp - memory management unit - header file */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef MMU_HPP_
#define MMU_HPP_

#include <bootinfo.hpp>
#include <architecture.hpp>
#include <paging.hpp>
#include <processor.hpp>
#include <kernel-sync.hpp>
#include <filesystem.hpp>
#include <map>

class AddressSpace;

class PhysicalPageView
{
public:
  struct AddressRange
  {
    physical_addr_t from;
    physical_addr_t to;
    static bool
    size_compare(const AddressRange& a, const AddressRange& b);
  };
  struct view_t
  {
    /* Possible combinations are:
     * missing: free = 1, exists = 0
     * reserved: free = 0, exists = 0
     * available: free = 1, exists = 1
     * mapped: free = 0, exists = 1
     */
    unsigned long free;
    unsigned long exists;
  };
  enum memory_type
  {
    missing = 0b10, reserved = 0b00, available = 0b11, mapped = 0b01
  };
  PhysicalPageView(BootInfo::MemoryMap& map, bool quiet);
  PhysicalPageView(size_t sz);
  void
  resize(size_t page_no);
  size_t
  getSize() const;
  AddressRange
  getFree();
  void
  markRange(physical_addr_t from, physical_addr_t to, memory_type as);
  bool
  rangeAssert(physical_addr_t from, physical_addr_t to, memory_type assertion);
  void
  markPage(physical_addr_t at, memory_type as);
  memory_type
  getPage(physical_addr_t addr);
private:
  inline memory_type
  page_from_view(view_t view, uint8_t offset);
  size_t mem_size;
  size_t free_size;
  size_t map_size;
  view_t* view;
  view_t* first_free_view;
  view_t* view_end;
};

class MMU
{
  friend class PageDirectory;
public:
  struct AddressRange
  {
    char* begin;
    char* end;
    bool
    operator<(const AddressRange& oth) const
    {
      return this->end <= oth.begin;
    }
    bool
    good()
    {
      return begin != NULL and end != NULL;
    }
  };
  enum flags_value
  {
    read = PageTable::read,
    write = PageTable::write,
    execute = PageTable::execute,
    reserved = 0x8,
    public_flag_mask = 0xF,
    file_source = 0x10,
    merged_source = 0x20,
    merged_dest = 0x40,
    copy_on_write = 0x80,
  };
  MMU(BootInfo::MemoryMap& m, bool quiet);
  ~MMU();
  void
  start();

  bool
  addMapping(AddressSpace*, const AddressRange&, unsigned int flags);
  bool
  dropMapping(AddressSpace* sp, const AddressRange&);
  AddressRange
  createMMap(AddressSpace* sp, size_t len);
  bool
  merge(AddressSpace* sp_src, const AddressRange& src, AddressSpace* sp_dst,
      const AddressRange& dst, unsigned int dst_flags);
  bool checkPermission(AddressSpace* sp,const AddressRange& rng,unsigned int flags);

  // simpler memory management
  void*
  extend(AddressSpace*, ptrdiff_t offset);

  bool
  fault(AddressSpace*, const AddressRange&, physical_addr_t at);

  // interrupt handling
  static
  void
  do_fault(uintptr_t address, unsigned long flags);
private:
  struct MemoryMap
  {
    Filesystem::MemoryMap* file_src;
    // the map we fault from
    AddressSpace* sp_src;
    const AddressRange* range_src;
    MemoryMap* map_src;
    // the map we fault to
    AddressSpace* sp_dst;
    const AddressRange* range_dst;
    MemoryMap* map_dst;
    unsigned int flags;
  };
  MMU(const MMU&);
  bool
  fault(AddressSpace*, const AddressRange&);
  bool
  fault(AddressSpace*, void* addr);
  bool
  unfault(AddressSpace* sp, const AddressRange&);
  bool
  unfault(AddressSpace* sp, void* addr);
  void
  moveToSwap(AddressSpace* sp, const AddressRange&);
  // low level memory access
  physical_addr_t
  getFreePage();
  void
  releaseFreePage(physical_addr_t);
  PhysicalPageView memory;
  PhysicalPageView swapspace;
  AddressRange kernel_datasegment;
  char* max_kernel_datasegment;
  char* min_kernel_datasegment;
public:
  typedef std::map<AddressRange, MemoryMap> maptree;
};

class AddressSpace
{
  friend class MMU;
public:
  // Initialize for GlobalStorage (kernel space)
  AddressSpace(const GlobalStorage& gs);
  // Initialize normally (empty space)
  AddressSpace();
  // Initialize by copy (fork space)
  AddressSpace(const AddressSpace&);
  ~AddressSpace();
  void
  load();
  static AddressSpace*
  current();
  physical_addr_t
  resolveAddress(void* log);
private:
  MMU::maptree mmaps;
  const MMU::AddressRange* datasegment; // the memory range for sbrk
  PageTable pt;
#if 0
  uintptr_t load_start;
  uintptr_t min_break;
  uintptr_t max_inited;
  uintptr_t current_break;
#endif
  mutex_t lock;
};

#endif /* MMU_HPP_ */
