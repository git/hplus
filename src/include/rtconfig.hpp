/* rtconfig.hpp - run time configuration */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef RTCONFIG_HPP_
#define RTCONFIG_HPP_

#include <exec.hpp>
#define ARGC_MAX 10

class RTConfig {
public:
  RTConfig(const char* cmdline);
  bool quiet() {
    return _quiet;
  }
  ExecCommand getInitCmd();
private:
  int argc;
  int init_cmd;
  char* argv[ARGC_MAX];
  char* cmdline_end;
  char* cmdline_begin;
  bool _quiet;
};

#endif /* RTCONFIG_HPP_ */
