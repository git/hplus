/* handle.hpp - pointer with implicit locking */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef HANDLE_HPP_
#define HANDLE_HPP_

#include <assert.h>
#include <kernel-sync.hpp>

template<class T>
class handle {
public:
  typedef T value_type;
  typedef T* pointer;
  typedef T& reference;
  handle(reference r,mutex_t* lock) : object(&r), implicit_lock(lock) {
    mutex_lock(implicit_lock);
  }
  handle(const handle<T>& h) {
    assert(object == NULL);
    assert(h.object != NULL);
    object = h.object;
  }
  ~handle() {
    mutex_unlock(implicit_lock);
    object = NULL;
  }
  reference operator *() const {
    return *object;
  }
  pointer operator ->() const {
    return object;
  }
private:
  pointer object;
  mutex_t* implicit_lock;
};

class scoped_lock {
public:
  scoped_lock(mutex_t& l) : lock(&l) {
    mutex_lock(lock);
  }
  ~scoped_lock() {
    mutex_unlock(lock);
  }
private:
  mutex_t* lock;
};

#endif /* HANDLE_HPP_ */
