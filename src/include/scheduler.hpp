/* scheduler.hpp - process and thread management */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCHEDULER_HPP_
#define SCHEDULER_HPP_

#include <stdlib.h>
#include <architecture.hpp>
#include <processor.hpp>
#include <syscall.hpp>
#include <paging.hpp>
#include <task.hpp>
#include <mmu.hpp>
#include <handle.hpp>
#include <filesystem.hpp>
#include <map>

class Scheduler;
class Thread;

typedef void
(*thread_call_t)(Task&);

class Process
{
  friend class Scheduler;
  friend class Thread;
public:
  pid_t
  getPid()
  {
    return this->self;
  }
  AddressSpace&
  getAddressSpace()
  {
    return *(this->space);
  }
  thread_id_t
  createThread(thread_call_t entry_point);
  void
  destroyThread(thread_id_t id);
  Thread*
  getThread(thread_id_t id) const;
  Thread*
  getThread(Task& t) const;
  void
  queueSignal(siginfo_t* sig);
  void
  kill();
  void
  cleanup();
private:
  Process(Process* parent, pid_t self);
  Process(const Process&);
  AddressSpace* space;
  Filesystem::Node root;
  Filesystem::Node pwd;
  std::map<thread_id_t, Thread*> by_id;
  std::map<void*, Thread*> by_stack;
  pid_t self;
  pid_t parent;
  int sched_abspriority;
  int sched_relpriority; // normalized nice value
  enum { SCHED_FIFO, SCHED_RR, SCHED_OTHER } sched_policy;
};

class Scheduler
{
public:
  Scheduler();
  ~Scheduler();
  void
  start(Processor& proc);
  Process&
  createProcess(pid_t parent);
  void
  destroyProcess(pid_t who);
  Process*
  getProcess(pid_t who) const;
  Process*
  getProcess(Task& t) const;
  void
  dispatchNext();
private:
  Scheduler(const Scheduler&);
  std::map<pid_t, Process*> by_pid;
  std::map<paging_cr_t, Process*> by_pages;
};

class Thread
{
  friend class Process;
  friend class Scheduler;
public:
  enum status_t
  {
    running,
    runnable_syscall,
    runnable_interrupt,
    lock_wait,
    timed_wait,
    signal_wait,
    external_wait,
    error
  };
  void
  dispatch();
  void
  cancel();
  void
  queueSignal(siginfo_t* sig);
  void
  mark(status_t as);
  static bool
  priority_sort(const Thread& one, const Thread& two);
  Process*
  getProcess()
  {
    return proc;
  }
private:
  Thread(Process* proc, thread_id_t id, thread_call_t entry_point);
  Task context;
  Process* proc;
  Processor* on;
  void* sighandler;
  size_t stack_size;
  thread_id_t id;
  status_t status;
  sigset_t sigmask;
  sigset_t sigpending;
  siginfo_t* pending_signals[siginfo_t::SIGRTMIN];
  std::map<rtsiginfo_t::sigid_t, rtsiginfo_t*> pending_rtsignals;
  unsigned long syscall_retvalue;
  unsigned long last_scheduling_time;
};

#endif /* SCHEDULER_HPP_ */
