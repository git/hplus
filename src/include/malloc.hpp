/* malloc.hpp - kernel memory allocator */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/* This file includes code from the GNU C Library
 Copyright (C) 1996-2006, 2007, 2008, 2009 Free Software Foundation, Inc.
 Contributed by Wolfram Gloger <wg@malloc.de>
 and Doug Lea <dl@cs.oswego.edu>, 2001.

 This is a version (aka ptmalloc2) of malloc/free/realloc written by
 Doug Lea and adapted to multiple threads/arenas by Wolfram Gloger.

 There have been substantial changes made after the integration into
 glibc in all parts of the code.  Do not look for much commonality
 with the ptmalloc2 version.

 * Version ptmalloc2-20011215
 based on:
 VERSION 2.7.0 Sun Mar 11 14:14:06 2001  Doug Lea  (dl at gee)
 */

#ifndef MALLOC_HPP_
#define MALLOC_HPP_

#include <stdlib.h>
#include <architecture.hpp>
#include <kernel-sync.hpp>
#include <mmu.hpp>
#include <new>


#define NBINS             128

class Allocator
{
public:
  struct malloc_chunk
  {

    size_t prev_size; /* Size of previous chunk (if free).  */
    size_t size; /* Size in bytes, including overhead. */

    struct malloc_chunk* fd; /* double links -- used only if free. */
    struct malloc_chunk* bk;

    /* Only used for large blocks: pointer to next larger size.  */
    struct malloc_chunk* fd_nextsize; /* double links -- used only if free. */
    struct malloc_chunk* bk_nextsize;
  };
  typedef malloc_chunk* mchunkptr;
  typedef malloc_chunk* mbinptr;
  typedef malloc_chunk* mfastbinptr;
  Allocator(MMU& mem,bool is_main);
  ~Allocator();
  int mallopt(int param_number,int value);
  void*
  int_malloc(size_t size);
  void*
  int_malloc(size_t size, size_t align);
  void*
  int_pvalloc(size_t size);
  void
  int_free(malloc_chunk*);
  void*
  int_realloc(malloc_chunk*, size_t, size_t);
  void**
  int_icalloc(size_t, size_t, void**);
  void**
  int_icomalloc(size_t, size_t*, void**);
  void
  acquire_lock();
  void
  release_lock();
  void
  munmap_chunk(mchunkptr p);
  void
  consolidate();
  int
  trim(size_t);
  static void
  report_error(const char* str, void* ptr);
private:
  template<bool one_size,bool also_clear>
    void**
    ialloc(size_t, size_t*, void**);
  void
  init_state();
  void*
  require_memory(size_t);
  int
  release_memory(size_t);
  void*
  mem2mem_check(void *p, size_t sz);
  int
  top_check(void);

  // debug checks
  void
  check_chunk(mchunkptr p);
  void
  check_free_chunk(mchunkptr p);
  void
  check_inuse_chunk(mchunkptr p);
  void
  check_remalloced_chunk(mchunkptr p, size_t s);
  void
  check_malloced_chunk(mchunkptr p, size_t s);
  void
  check_malloc_state();

  MMU* mmu;
  // For all the following stuff, see docs in GNU Libc implementation.
private:
  /* Serialize access.  */
  mutex_t mutex;

  /* Flags (formerly in max_fast).  */
  int flags;

  /* Fastbins */
  // See the definition of NFASTBINS in the implementation
  mfastbinptr
      fastbinsY[(((((unsigned int) (((((80 * sizeof(size_t) / 4))
          + sizeof(size_t) + (2 * sizeof(size_t) - 1)
          < (unsigned long) ((((offsetof(struct malloc_chunk,fd_nextsize)) + (2
              * sizeof(size_t) - 1)) & ~(2 * sizeof(size_t) - 1)))) ? (unsigned long) ((((offsetof(struct malloc_chunk,fd_nextsize))
          + (2 * sizeof(size_t) - 1)) & ~(2 * sizeof(size_t) - 1)))
          : (((80 * sizeof(size_t) / 4)) + sizeof(size_t) + (2 * sizeof(size_t)
              - 1)) & ~(2 * sizeof(size_t) - 1)))) >> (sizeof(size_t) == 8 ? 4
          : 3)) - 2) + 1)];

  /* Base of the topmost chunk -- not otherwise kept in a bin */
  mchunkptr top;

  /* The remainder from the most recent split of a small request */
  mchunkptr last_remainder;

  /* Normal bins packed as described above */
  // See the definition of NBINS
  mchunkptr bins[128 * 2 - 2];

  /* Bitmap of bins */
  // See the definition of BINMAPSIZE
  unsigned int binmap[(128 / (1U << 5))];

  /* Linked list */
  Allocator *next;

#ifdef PER_THREAD
  /* Linked list for free arenas.  */
  struct malloc_state *next_free;
#endif

  /* Memory allocated from the system in this arena.  */
  size_t system_mem;
  size_t max_system_mem;

};

// normal new
void *
operator new(size_t);
void *
operator new[](size_t);

#if 0
// just construct, not allocate
inline void *
operator new(size_t sz, void * p) {
  return p;
}
inline void *
operator new[](size_t sz, void * p) {
  return p;
}
#endif

// allocate aligned
void *
operator new(size_t, size_t);
void *
operator new[](size_t, size_t);

// normal delete
void
operator delete(void *);
void
operator delete[](void *);

#if 0
// delete but not deallocate
void
operator delete(void *, void *);
void
operator delete[](void *, void *);
#endif

namespace KMemory
{
  void*
  malloc(size_t size);
  void*
  malloc(size_t size, size_t align);
  void*
  pvalloc(size_t size);
  void*
  calloc(size_t n, size_t element_size);
  void**
  independent_calloc(size_t n_elements, size_t element_size, void** chunks);
  void**
  independent_comalloc(size_t n_elements, size_t sizes[], void** chunks);
  void*
  realloc(void*, size_t);
  void
  free(void*);
  bool
  trim(size_t left);
}

#endif /* MALLOC_HPP_ */
