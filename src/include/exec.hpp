/* exec.hpp - interface for file execution */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef EXEC_HPP_
#define EXEC_HPP_

class ExecCommand
{
public:
  ExecCommand(const char* file, char* const* argv, char* const * envp) :
    _filename(file), _argv(argv), _envp(envp)
  {
    return;
  }
  const char* getFilename() const {
    return _filename;
  }
private:
  const char* _filename;
  char* const * _argv;
  char* const * _envp;
};

#endif /* EXEC_HPP_ */
