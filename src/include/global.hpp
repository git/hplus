/* global.hpp - class containing global kernel structures */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef GLOBAL_HPP_
#define GLOBAL_HPP_

#include <bootinfo.hpp>
#include <processor.hpp>
#include <kernel-sync.hpp>
#include <rtconfig.hpp>
#include <mmu.hpp>
#include <malloc.hpp>
#include <scheduler.hpp>
#include <filesystem.hpp>
#include <handle.hpp>

class GlobalStorage
{
public:
  GlobalStorage(BootInfo& b);
  handle<Scheduler>
  getScheduler() {
    return handle<Scheduler>(s,&sched_lock);
  }
  MMU&
  getMMU()
  {
    return mem;
  }
  AddressSpace&
  getKernelSpace() {
    return kernel_space;
  }
  RTConfig&
  getConfig() {
    return conf;
  }
  Filesystem& getFilesystem() {
    return fs;
  }
private:
  GlobalStorage(const GlobalStorage&);
  /* DO NOT change the order of these variables, as it would change
     the order they're initialized.
   */
  RTConfig conf;
  MMU mem;
  AddressSpace kernel_space;
  Allocator mem_arena;
  SMP multiproc;
  Scheduler s;
  Filesystem fs;

  /* These variables are just locks, you can rearrange them if
     you like, and probably also intermix them with useful objects,
     but I wouldn't bet on it.
   */
  mutex_t sched_lock;
};

#endif /* GLOBAL_HPP_ */
