/* filesystem.hpp - virtual filesystem layer */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef FILESYSTEM_HPP_
#define FILESYSTEM_HPP_

#include <stdlib.h>
#include <vector>

class Process;
class Thread;
class FileDescriptor;
class KTranslator;

struct FileLock {
  enum { read, write } l_type;
  off_t file_off;
  size_t len;
  Process* owner;
};

struct sigsyscall_t : siginfo_t
{
  enum syscall_t
  {
    linkstat,
    nodestat,
    link,
    unlink,
    creat,
    open,
    close,
    read,
    write,
    ioctl,
    fdctl
  } syscall;
  uint16_t trans_id;
  sigsyscall_t(syscall_t sys)
  {
    si_signo = SIGSYSCALL;
    si_value.val_ptr = &syscall;
    si_code = SI_ASYNCIO;
    si_pid = 0;
    si_uid = 0;
    si_addr = NULL;
    syscall = sys;
  }
};
/* Calls with a name and a parent */
struct tree_call : sigsyscall_t
{
  ino_t parent;
  char name[NAME_MAX];
  tree_call(syscall_t sys) :
    sigsyscall_t(sys)
  {
    return;
  }
};
struct lstat_call : tree_call
{
  int flags;
  ino_t node;
  char* redirection;
  pid_t cache_info;
  mode_t type;
  uint8_t special_type;
  lstat_call() :
    tree_call(linkstat)
  {
    si_valuesz = sizeof(lstat_call) - sizeof(siginfo_t);
  }
};
struct nstat_call : tree_call
{
  stat node_status;
  nstat_call() :
    tree_call(nodestat)
  {
    si_valuesz = sizeof(nstat_call) - sizeof(siginfo_t);
  }
};
struct link_call : tree_call
{
  ino_t child;
  link_call() :
    tree_call(link)
  {
    si_valuesz = sizeof(link_call) - sizeof(siginfo_t);
  }
};
struct unlink_call : tree_call
{
  unlink_call() :
    tree_call(unlink)
  {
    si_valuesz = sizeof(unlink_call) - sizeof(siginfo_t);
  }
};
struct creat_call : sigsyscall_t
{
  ino_t parent;
  ino_t child;
  uint8_t special_type;
  creat_call() :
    sigsyscall_t(creat)
  {
    si_valuesz = sizeof(creat_call) - sizeof(siginfo_t);
  }
};
/* Calls with a file description (connection id) */
struct fd_call : sigsyscall_t
{
  uint32_t cid;
  fd_call(syscall_t sys) :
    sigsyscall_t(sys)
  {
    return;
  }
};
struct open_call : fd_call
{
  ino_t node;
  int flags;
  mode_t mode;
  open_call() :
    fd_call(open)
  {
    si_valuesz = sizeof(open_call) - sizeof(siginfo_t);
  }
};
struct close_call : fd_call
{
  close_call() :
    fd_call(close)
  {
    si_valuesz = sizeof(close_call) - sizeof(siginfo_t);
  }
};
/* Read/write calls */
struct rw_call : fd_call
{
  void* buffer;
  size_t size;
  off_t fileoffset;
  rw_call(syscall_t sys) :
    fd_call(sys)
  {
    si_valuesz = sizeof(rw_call) - sizeof(siginfo_t);
  }
};
/* Ioctl/Fdctl */
struct xctl_call : fd_call
{
  uint32_t out;
  uint32_t cmd;
  unsigned long args[5];
  xctl_call(syscall_t sys) :
    fd_call(sys)
  {
    si_valuesz = sizeof(xctl_call) - sizeof(siginfo_t);
  }
};


class Filesystem
{
public:
  class Node;
  class Link
  {
    Node*
    resolve();
    bool
    canAccess(mode_t);
  private:
    char* name;
    size_t name_len;
    Node* node;
    int flags;
    pid_t cache_info;
    mode_t type;
    /* for special files (that are registered both in the translator
     * that handles them and in the translator that hosts them)
     */
    uint8_t special_type;
  };
  class Node
  {
  public:
    Link*
    resolveRelative(Process&, const char* path, mode_t mode);
    translator_id_t
    getTranslator() {
      return this->trans_id;
    }
    ino_t
    getInode() {
      return this->inode_id;
    }
  private:
    translator_id_t trans_id;
    ino_t inode_id;
    std::vector<Link> children;
    std::vector<FileLock> locks;
  };

  class MemoryMap
  {
  public:
    bool
    fill(void* at, size_t len);
  private:
    off_t file_off;
    uintptr_t mem_off;
    FileDescriptor* fdes;
  };
  Filesystem() {
    return;
  }
  KTranslator* getKernelTranslator(uint16_t id) {
    return this->kern_trans[id];
  }
  static err_t enter_fsyscall();
private:
  Filesystem::Node* root;
  translator_id_t special_file[256];
  KTranslator* kern_trans[256];
};

class FileDescription
{
  friend class FileDescriptor;
public:
  FileDescription(Process&, Filesystem::Node&, mode_t);
  ~FileDescription();
private:
  Filesystem::Node& node;
  Process& process;
  uint32_t conn_id;
  mode_t mode;
  uint8_t ref_count;
};

/* File management system calls:
 * - start is called by Filesystem::enter_fsyscall, sets up the sigsyscall_t object to be
 *   sent to the translator; returns EINPROGRESS if no problem, else an error
 * - end is called when the translator is done, to finish the system call work and to leave
 *   the calling thread in a resumable state
 * - sync prepares the operation and schedules immediately the translator (with a priority of
 *   100) so that it returns from the signal exactly inside sync; to be used for kernel read/
 *   write
 */
class FileDescriptor
{
public:
  FileDescriptor(Process&, Filesystem::Node&, int flags, mode_t);
  FileDescriptor(const FileDescriptor&);
  ~FileDescriptor();
  ssize_t
  end_read(Thread* caller,rw_call* op);
  err_t
  start_read(Thread* caller,rw_call* out,void* buf, size_t count, off_t file_off);
  ssize_t
  end_write(Thread* caller,rw_call* op);
  err_t
  start_write(Thread* caller,rw_call* out,void* buf, size_t count, off_t file_off);
  err_t
  sync_read(void* buf,size_t count,off_t file_off);
  err_t
  sync_write(void* buf,size_t count,off_t file_off);

  // kernel managed flags
  int
  getFlags();
  bool
  setFlags(int newflags);

  // record locks
  bool start_acquireLock(Thread* own,xctl_call* op,FileLock& fl,bool wait);
  bool end_acquireLock(Thread* own,xctl_call* op);
  bool releaseLock(FileLock& fl);
  bool checkLock(FileLock& fl);

  // ioctl / user fdctl
  bool
  start_xctl(Thread* caller,xctl_call* op);
  uint32_t
  end_xctl(Thread* caller,xctl_call* op);
  bool
  sync_ioctl(uint32_t op,...);
private:
  FileDescription* desc;
  uint32_t conn_id;
  int flags;
  err_t errno;
};

#endif /* FILESYSTEM_HPP_ */
