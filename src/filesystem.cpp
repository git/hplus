/* filesystem.cpp - virtual filesystem layer (implementation) */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib/stdlib.h>
#include <stdlib/errno.h>
#include <filesystem.hpp>
#include <global.hpp>

bool
Filesystem::MemoryMap::fill(void* at, size_t len)
{
  if (this->fdes->sync_read(at, len, (uintptr_t(at) - mem_off + file_off))
      != ENOERR)
    return false;
  return true;
}

err_t
FileDescriptor::start_write(Thread* caller, rw_call* out, void* buf,
    size_t count, off_t file_off)
{
  if (this->desc == NULL)
    return EBADF;
  if ((this->desc->mode & O_WRITE) == 0)
    return EBADF;
  Filesystem::Node& n(this->desc->node);
  translator_id_t trans = n.getTranslator();
  GlobalStorage& gs(Processor::getGlobalStorage());
  out->si_pid = trans.pid;
  out->trans_id = trans.trans_id;
  if (trans.pid == 0)
    { // kernel translator, just use user buffers
      out->buffer = buf;
      out->size = count;
      out->fileoffset = file_off;
    }
  else
    { // estabilish shared memory
      handle<Scheduler> s(gs.getScheduler());
      Process* server(s->getProcess(trans.pid));
      Process* client(caller->getProcess());
      AddressSpace& sp_server(server->getAddressSpace());
      AddressSpace& sp_client(client->getAddressSpace());
      MMU& mmu(gs.getMMU());
      uintptr_t start_addr((uintptr_t) buf);
      uintptr_t end_addr(start_addr + count);
      // used so that the first page aligned address in the client buffer
      // corresponds to the server buffer (so we can merge the two maps)
      ptrdiff_t correction(0);
      if (start_addr % page_size != 0)
        correction = page_size - (start_addr % page_size);
      MMU::AddressRange mem(mmu.createMMap(&sp_server, (count + correction
          + page_size) & ~(page_size - 1)));
      if (!mem.good())
        {
          return ENOBUFS;
        }
      if (start_addr % page_size == 0 and end_addr % page_size == 0)
        {
          // No copy involved, just merge the pages
          MMU::AddressRange client_mem;
          client_mem.begin = (char*) start_addr;
          client_mem.end = (char*) end_addr;
          if (!mmu.merge(&sp_client, client_mem, &sp_server, mem, MMU::read))
            return EFAULT;
        }
      else
        {
          MMU::AddressRange client_mem;
          client_mem.begin = (char*) (start_addr & ~(page_size - 1));
          client_mem.end = (char*) ((end_addr + page_size) & ~(page_size - 1));
          if (!mmu.checkPermission(&sp_client, client_mem, MMU::read))
            return EFAULT;
          char* tmp_buf_one(NULL);
          char* tmp_buf_two(NULL);
          MMU::AddressRange first_part_serv, first_part_kern, first_part_client;
          if (correction != 0)
            { // start is not aligned, we must copy
              // enable some of the memory in the server
              first_part_serv.begin = mem.begin;
              first_part_serv.end = mem.begin + page_size;
              mmu.addMapping(&sp_server, first_part_serv, MMU::read
                  | MMU::write);
              // acquire the memory we need from the client
              first_part_kern = mmu.createMMap(NULL, page_size);
              tmp_buf_one = first_part_kern.begin;
              first_part_client.begin = (char*) (start_addr & ~(page_size - 1));
              first_part_client.end = (char*) ((start_addr & ~(page_size - 1))
                  + page_size);
              mmu.merge(&sp_client, first_part_client, NULL, first_part_kern,
                  MMU::read);
            }
          MMU::AddressRange second_part_serv, second_part_kern,
              second_part_client;
          if ((correction + count) % page_size != 0)
            { // end is not aligned
              // enable some of the memory in the server
              second_part_serv.begin = mem.end - page_size;
              second_part_serv.end = mem.end;
              mmu.addMapping(&sp_server, second_part_serv, MMU::read
                  | MMU::write);
              // acquire the memory we need from the client
              second_part_kern = mmu.createMMap(NULL, page_size);
              tmp_buf_two = second_part_kern.begin;
              second_part_client.begin = (char*) (end_addr & ~(page_size - 1));
              second_part_client.end = (char*) ((end_addr & ~(page_size - 1))
                  + page_size);
              mmu.merge(&sp_client, second_part_client, NULL, second_part_kern,
                  MMU::read);
            }
          sp_server.load();
          if (tmp_buf_one)
            {
              memcpy(tmp_buf_one, (char*) (first_part_serv.begin) + correction,
                  page_size - correction);
              // drop the temp buffer
              mmu.dropMapping(NULL, first_part_kern);
            }
          if (tmp_buf_two)
            {
              memcpy(tmp_buf_two, (char*) (second_part_serv.begin), (end_addr
                  % page_size));
              // drop the temp buffer
              mmu.dropMapping(NULL, second_part_kern);
            }
          MMU::AddressRange remaining_client;
          remaining_client.begin = (char*) ((start_addr + page_size)
              & ~(page_size - 1));
          remaining_client.end = (char*) (end_addr & ~(page_size - 1));
          MMU::AddressRange remaining_server;
          remaining_server.begin = correction == 0 ? mem.begin : mem.begin
              + page_size;
          remaining_server.end = (end_addr % page_size == 0) ? mem.end
              : mem.end - page_size;
          mmu.merge(&sp_client, remaining_client, &sp_server, remaining_server,
              MMU::read);
        }
      out->buffer = mem.begin + correction;
      out->size = count;
      out->fileoffset = file_off;
    }
  return EINPROGRESS;
}
err_t
FileDescriptor::start_read(Thread* caller, rw_call* out, void* buf,
    size_t count, off_t file_off)
{
  if (this->desc == NULL)
    return EBADF;
  if ((this->desc->mode & O_READ) == 0)
    return EBADF;
  Filesystem::Node& n(this->desc->node);
  translator_id_t trans = n.getTranslator();
  GlobalStorage& gs(Processor::getGlobalStorage());
  out->si_pid = trans.pid;
  out->trans_id = trans.trans_id;
  if (trans.pid == 0)
    { // kernel translator, just use user buffers
      out->buffer = buf;
      out->size = count;
      out->fileoffset = file_off;
    }
  else
    { // estabilish shared memory
      handle<Scheduler> s(gs.getScheduler());
      Process* server(s->getProcess(trans.pid));
      Process* client(caller->getProcess());
      AddressSpace& sp_server(server->getAddressSpace());
      AddressSpace& sp_client(client->getAddressSpace());
      MMU& mmu(gs.getMMU());
      uintptr_t start_addr((uintptr_t) buf);
      uintptr_t end_addr(start_addr + count);
      // used so that the first page aligned address in the client buffer
      // corresponds to the server buffer (so we can merge the two maps)
      ptrdiff_t correction(0);
      if (start_addr % page_size != 0)
        correction = page_size - (start_addr % page_size);
      MMU::AddressRange mem(mmu.createMMap(&sp_server, (count + correction
          + page_size) & ~(page_size - 1)));
      if (!mem.good())
        {
          return ENOBUFS;
        }
      if (start_addr % page_size == 0 and end_addr % page_size == 0)
        {
          // No copy involved, just merge the pages
          MMU::AddressRange client_mem;
          client_mem.begin = (char*) start_addr;
          client_mem.end = (char*) end_addr;
          if (!mmu.merge(&sp_client, client_mem, &sp_server, mem, MMU::read
              | MMU::write))
            return EFAULT;
        }
      else
        {
          MMU::AddressRange client_mem;
          client_mem.begin = (char*) (start_addr & ~(page_size - 1));
          client_mem.end = (char*) ((end_addr + page_size) & ~(page_size - 1));
          if (!mmu.checkPermission(&sp_client, client_mem, MMU::read
              | MMU::write))
            return EFAULT;

          MMU::AddressRange first_part_serv;
          if (correction != 0)
            { // start is not aligned, we will copy when returning
              // enable some of the memory in the server
              first_part_serv.begin = mem.begin;
              first_part_serv.end = mem.begin + page_size;
              mmu.addMapping(&sp_server, first_part_serv, MMU::read
                  | MMU::write);
            }
          MMU::AddressRange second_part_serv;
          if ((correction + count) % page_size != 0)
            { // end is not aligned
              // enable some of the memory in the server
              // again, we will copy when returning
              second_part_serv.begin = mem.end - page_size;
              second_part_serv.begin = mem.end;
              mmu.addMapping(&sp_server, second_part_serv, MMU::read
                  | MMU::write);
            }
          MMU::AddressRange remaining_client;
          remaining_client.begin = (char*) ((correction == 0) ? ((start_addr
              + page_size) & ~(page_size - 1)) : start_addr);
          remaining_client.end
              = (char*) ((end_addr % page_size == 0) ? (end_addr) : (end_addr
                  & ~(page_size - 1)));
          MMU::AddressRange remaining_server;
          remaining_server.begin = correction == 0 ? mem.begin : mem.begin
              + page_size;
          remaining_server.end = (end_addr % page_size == 0) ? mem.end
              : mem.end - page_size;
          mmu.merge(&sp_client, remaining_client, &sp_server, remaining_server,
              MMU::read | MMU::write);
        }
      out->buffer = (char*) (mem.begin) + correction;
      out->size = count;
      out->fileoffset = file_off;
    }
  return EINPROGRESS;
}
