/* rtconfig.cpp - run time configuration (implementation) */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/* This file includes code from Getopt, part of the GNU C Library.
 Copyright (C) 1987-1996,1998-2004,2008,2009 Free Software Foundation, Inc.
 */

/* Although this is derived on the normal Glibc Getopt, it has been modified:
 * - order is not preserved, irrespective of what POSIX says
 * - the interface has been made OO
 * - long options start only with --
 * - panic for bad options cannot be prevented by setting opterr
 */

#include <config.h>
#include <stdlib/stdlib.h>
#include <panic.hpp>
#include <rtconfig.hpp>

/* Describe the long-named options requested by the application.
 The LONG_OPTIONS argument to getopt_long or getopt_long_only is a vector
 of `struct option' terminated by an element containing a name which is
 zero.

 The field `has_arg' is:
 no_argument          (or 0) if the option does not take an argument,
 required_argument    (or 1) if the option requires an argument,
 optional_argument    (or 2) if the option takes an optional argument.

 If the field `flag' is not NULL, it points to a variable that is set
 to the value given in the field `val' when the option is found, but
 left unchanged if the option is not found.

 To have a long-named option do something other than set an `int' to
 a compiled-in constant, such as set a value from `optarg', set the
 option's `flag' field to zero and its `val' field to a nonzero
 value (the equivalent single-letter option character, if there is
 one).  For long options that have a zero `flag' field, `getopt'
 returns the contents of the `val' field.  */

struct option
{
  const char *name;
  /* has_arg can't be an enum because some compilers complain about
   type mismatches in all the code that assumes it is an int.  */
  enum
  {
    no_argument, required_argument, optional_argument
  } has_arg;
  int *flag;
  int val;
};

class GetOpt
{
public:
  int optind;
  int optopt;
  char *optarg;

private:
  /* Internal members.  */

  /* The next char to be scanned in the option-element
   in which the last option character we returned was found.
   This allows us to pick up the scan where we left off.

   If this is zero, or a null string, it means resume the scan
   by advancing to the next ARGV-element.  */
  char *__nextchar;

  /* Describe how to deal with options that follow non-option ARGV-elements.

   If the caller did not specify anything,
   the default is REQUIRE_ORDER if the environment variable
   POSIXLY_CORRECT is defined, PERMUTE otherwise.

   REQUIRE_ORDER means don't recognize them as options;
   stop option processing when the first non-option is seen.
   This is what Unix does.
   This mode of operation is selected by either setting the environment
   variable POSIXLY_CORRECT, or using `+' as the first character
   of the list of option characters.

   PERMUTE is the default.  We permute the contents of ARGV as we
   scan, so that eventually all the non-options are at the end.
   This allows options to be given in any order, even with programs
   that were not written to expect this.

   RETURN_IN_ORDER is an option available to programs that were
   written to expect options and other ARGV-elements in any order
   and that care about the ordering of the two.  We describe each
   non-option ARGV-element as if it were the argument of an option
   with character code 1.  Using `-' as the first character of the
   list of option characters selects this mode of operation.

   The special argument `--' forces an end of option-scanning regardless
   of the value of `ordering'.  In the case of RETURN_IN_ORDER, only
   `--' can cause `getopt' to return -1 with `optind' != ARGC.  */

  const enum
  {
    REQUIRE_ORDER, PERMUTE, RETURN_IN_ORDER
  } __ordering;

  /* Handle permutation of arguments.  */

  /* Describe the part of ARGV that contains non-options that have
   been skipped.  `first_nonopt' is the index in ARGV of the first
   of them; `last_nonopt' is the index after the last of them.  */

  int __first_nonopt;
  int __last_nonopt;

  void
  exchange(char** argv);
public:
  GetOpt();
  int
  get(int argc, char * const *argv, const char *optstring,
      const option *longopts, int *longind);
};

/* Exchange two adjacent subsequences of ARGV.
 One subsequence is elements [first_nonopt,last_nonopt)
 which contains all the non-options that have been skipped so far.
 The other is elements [last_nonopt,optind), which contains all
 the options processed since those non-options were skipped.

 `first_nonopt' and `last_nonopt' are relocated so that they describe
 the new indices of the non-options in ARGV after they are moved.  */

void
GetOpt::exchange(char **argv)
{
  int bottom = this->__first_nonopt;
  int middle = this->__last_nonopt;
  int top = this->optind;
  char *tem;

  /* Exchange the shorter segment with the far end of the longer segment.
   That puts the shorter segment into the right place.
   It leaves the longer segment in the right place overall,
   but it consists of two parts that need to be swapped next.  */

  while (top > middle && middle > bottom)
    {
      if (top - middle > middle - bottom)
        {
          /* Bottom segment is the short one.  */
          int len = middle - bottom;
          register int i;

          /* Swap it with the top part of the top segment.  */
          for (i = 0; i < len; i++)
            {
              tem = argv[bottom + i];
              argv[bottom + i] = argv[top - (middle - bottom) + i];
              argv[top - (middle - bottom) + i] = tem;
            }
          /* Exclude the moved bottom segment from further swapping.  */
          top -= len;
        }
      else
        {
          /* Top segment is the short one.  */
          int len = top - middle;
          register int i;

          /* Swap it with the bottom part of the bottom segment.  */
          for (i = 0; i < len; i++)
            {
              tem = argv[bottom + i];
              argv[bottom + i] = argv[middle + i];
              argv[middle + i] = tem;
            }
          /* Exclude the moved top segment from further swapping.  */
          bottom += len;
        }
    }

  /* Update records for the slots the non-options now occupy.  */

  this->__first_nonopt += (this->optind - this->__last_nonopt);
  this->__last_nonopt = this->optind;
}

/* Initialize the internal data when the first call is made.
 * Start parsing at argument 1.  */

GetOpt::GetOpt() :
  optind(1), __nextchar(NULL), __ordering(PERMUTE), __first_nonopt(1),
      __last_nonopt(1)
{
  return;
}

/* Scan elements of ARGV (whose length is ARGC) for option characters
 given in OPTSTRING.

 If an element of ARGV starts with '-', and is not exactly "-" or "--",
 then it is an option element.  The characters of this element
 (aside from the initial '-') are option characters.  If `getopt'
 is called repeatedly, it returns successively each of the option characters
 from each of the option elements.

 If `getopt' finds another option character, it returns that character,
 updating `optind' and `nextchar' so that the next call to `getopt' can
 resume the scan with the following option character or ARGV-element.

 If there are no more option characters, `getopt' returns -1.
 Then `optind' is the index in ARGV of the first ARGV-element
 that is not an option.  (The ARGV-elements have been permuted
 so that those that are not options now come last.)

 OPTSTRING is a string containing the legitimate option characters.
 If an option character is seen that is not listed in OPTSTRING,
 return '?' after printing an error message.  If you set `opterr' to
 zero, the error message is suppressed but we still return '?'.

 If a char in OPTSTRING is followed by a colon, that means it wants an arg,
 so the following text in the same ARGV-element, or the text of the following
 ARGV-element, is returned in `optarg'.  Two colons mean an option that
 wants an optional arg; if there is text in the current ARGV-element,
 it is returned in `optarg', otherwise `optarg' is set to zero.

 If OPTSTRING starts with `-' or `+', it requests different methods of
 handling the non-option ARGV-elements.
 See the comments about RETURN_IN_ORDER and REQUIRE_ORDER, above.

 Long-named options begin with `--' instead of `-'.
 Their names may be abbreviated as long as the abbreviation is unique
 or is an exact match for some defined option.  If they have an
 argument, it follows the option name in the same ARGV-element, separated
 from the option name by a `=', or else the in next ARGV-element.
 When `getopt' finds a long-named option, it returns 0 if that option's
 `flag' field is nonzero, the value of the option's `val' field
 if the `flag' field is zero.

 The elements of ARGV aren't really const, because we permute them.
 But we pretend they're const in the prototype to be compatible
 with other systems.

 LONGOPTS is a vector of `struct option' terminated by an
 element containing a name which is zero.

 LONGIND returns the index in LONGOPT of the long-named option found.
 It is only valid when a long-named option has been found by the most
 recent call.

 If LONG_ONLY is nonzero, '-' as well as '--' can introduce
 long-named options.  */

int
GetOpt::get(int argc, char * const *argv, const char *optstring,
    const struct option *longopts, int *longind)
{
  if (argc < 1)
    return -1;

  this->optarg = NULL;

  /* Test whether ARGV[optind] points to a non-option argument.
   Either it does not have option syntax, or there is an environment flag
   from the shell indicating it is not an option.  The later information
   is only used when the used in the GNU libc (so never here)  */
# define NONOPTION_P (argv[this->optind][0] != '-' || argv[this->optind][1] == '\0')

  if (this->__nextchar == NULL || *this->__nextchar == '\0')
    {
      /* Advance to the next ARGV-element.  */

      /* Give FIRST_NONOPT & LAST_NONOPT rational values if OPTIND has been
       moved back by the user (who may also have changed the arguments).  */
      if (this->__last_nonopt > this->optind)
        this->__last_nonopt = this->optind;
      if (this->__first_nonopt > this->optind)
        this->__first_nonopt = this->optind;

      if (this->__ordering == PERMUTE)
        {
          /* If we have just processed some options following some non-options,
           exchange them so that the options come first.  */

          if (this->__first_nonopt != this->__last_nonopt
              && this->__last_nonopt != this->optind)
            this->exchange((char **) argv);
          else if (this->__last_nonopt != this->optind)
            this->__first_nonopt = this->optind;

          /* Skip any additional non-options
           and extend the range of non-options previously skipped.  */

          while (this->optind < argc && NONOPTION_P)
            this->optind++;
          this->__last_nonopt = this->optind;
        }

      /* The special ARGV-element `--' means premature end of options.
       Skip it like a null option,
       then exchange with previous non-options as if it were an option,
       then skip everything else like a non-option.  */

      if (this->optind != argc && !strcmp(argv[this->optind], "--"))
        {
          this->optind++;

          if (this->__first_nonopt != this->__last_nonopt
              && this->__last_nonopt != this->optind)
            this->exchange((char **) argv);
          else if (this->__first_nonopt == this->__last_nonopt)
            this->__first_nonopt = this->optind;
          this->__last_nonopt = argc;

          this->optind = argc;
        }

      /* If we have done all the ARGV-elements, stop the scan
       and back over any non-options that we skipped and permuted.  */

      if (this->optind == argc)
        {
          /* Set the next-arg-index to point at the non-options
           that we previously skipped, so the caller will digest them.  */
          if (this->__first_nonopt != this->__last_nonopt)
            this->optind = this->__first_nonopt;
          return -1;
        }

      /* If we have come to a non-option and did not permute it,
       either stop the scan or describe it to the caller and pass it by.  */

      if (NONOPTION_P)
        {
          if (this->__ordering == REQUIRE_ORDER)
            return -1;
          this->optarg = argv[this->optind++];
          return 1;
        }

      /* We have found another option-ARGV-element.
       Skip the initial punctuation.  */

      this->__nextchar = (argv[this->optind] + 1 + (longopts != NULL
          && argv[this->optind][1] == '-'));
    }

  /* Decode the current option-ARGV-element.  */

  /* Check whether the ARGV-element is a long option.

   If long_only and the ARGV-element has the form "-f", where f is
   a valid short option, don't consider it an abbreviated form of
   a long option that starts with f.  Otherwise there would be no
   way to give the -f short option.

   On the other hand, if there's a long option "fubar" and
   the ARGV-element is "-fu", do consider that an abbreviation of
   the long option, just like "--fu", and not "-f" with arg "u".

   This distinction seems to be the most useful approach.  */

  if (longopts != NULL && (argv[this->optind][1] == '-'))
    {
      char *nameend;
      const struct option *p;
      const struct option *pfound = NULL;
      int exact = 0;
      int ambig = 0;
      int indfound = -1;
      int option_index;

      for (nameend = this->__nextchar; *nameend && *nameend != '='; nameend++)
        /* Do nothing.  */;

      /* Test all long options for either exact match
       or abbreviated matches.  */
      for (p = longopts, option_index = 0; p->name; p++, option_index++)
        if (!strncmp(p->name, this->__nextchar, nameend - this->__nextchar))
          {
            if ((unsigned int) (nameend - this->__nextchar)
                == (unsigned int) strlen(p->name))
              {
                /* Exact match found.  */
                pfound = p;
                indfound = option_index;
                exact = 1;
                break;
              }
            else if (pfound == NULL)
              {
                /* First nonexact match found.  */
                pfound = p;
                indfound = option_index;
              }
            else if (false || pfound->has_arg != p->has_arg || pfound->flag
                != p->flag || pfound->val != p->val)
              /* Second or later nonexact match found.  */
              ambig = 1;
          }

      if (ambig && !exact)
        {
          printf("Option '%s' is ambiguous\n", argv[this->optind]);
          this->__nextchar += strlen(this->__nextchar);
          this->optind++;
          this->optopt = 0;
          return '?';
        }

      if (pfound != NULL)
        {
          option_index = indfound;
          this->optind++;
          if (*nameend)
            {
              /* Don't test has_arg with >, because some C compilers don't
               allow it to be used on enums.  */
              if (pfound->has_arg)
                this->optarg = nameend + 1;
              else
                {
                  if (argv[this->optind - 1][1] == '-')
                    {
                      /* --option */
                      printf("Option '--%s' doesn't allow an argument\n",
                          pfound->name);
                    }
                  else
                    {
                      /* +option or -option */
                      printf("Option '%c%s' doesn't allow an argument\n",
                          argv[this->optind - 1][0], pfound->name);
                    }

                  this->__nextchar += strlen(this->__nextchar);

                  this->optopt = pfound->val;
                  return '?';
                }
            }
          else if (pfound->has_arg == 1)
            {
              if (this->optind < argc)
                this->optarg = argv[this->optind++];
              else
                {
                  printf("Option '%s' requires an argument\n",
                      argv[this->optind - 1]);
                  this->__nextchar += strlen(this->__nextchar);
                  this->optopt = pfound->val;
                  return optstring[0] == ':' ? ':' : '?';
                }
            }
          this->__nextchar += strlen(this->__nextchar);
          if (longind != NULL)
            *longind = option_index;
          if (pfound->flag)
            {
              *(pfound->flag) = pfound->val;
              return 0;
            }
          return pfound->val;
        }

      /* Can't find it as a long option.  If this is not getopt_long_only,
       or the option starts with '--' or is not a valid short
       option, then it's an error.
       Otherwise interpret it as a short option.  */
      if (!(false) || argv[this->optind][1] == '-' || strchr(optstring,
          *this->__nextchar) == NULL)
        {

          if (argv[this->optind][1] == '-')
            {
              /* --option */
              printf("Unrecognized option '--%s'\n", this->__nextchar);
            }
          else
            {
              /* +option or -option */
              printf("Unrecognized option '%c%s'\n", argv[this->optind][0],
                  this->__nextchar);
            }
          this->__nextchar = (char *) "";
          this->optind++;
          this->optopt = 0;
          return '?';
        }
    }

  /* Look at and handle the next short option-character.  */

    {
      char c = *this->__nextchar++;
      char *temp = strchr(optstring, c);

      /* Increment `optind' when we start to process its last character.  */
      if (*this->__nextchar == '\0')
        ++this->optind;

      if (temp == NULL || c == ':')
        {
          printf("Invalid option -- '%c'\n", c);
          this->optopt = c;
          return '?';
        }
      /* Convenience. Treat POSIX -W foo same as long option --foo */
      if (temp[0] == 'W' && temp[1] == ';')
        {
          char *nameend;
          const struct option *p;
          const struct option *pfound = NULL;
          int exact = 0;
          int ambig = 0;
          int indfound = 0;
          int option_index;

          /* This is an option that requires an argument.  */
          if (*this->__nextchar != '\0')
            {
              this->optarg = this->__nextchar;
              /* If we end this ARGV-element by taking the rest as an arg,
               we must advance to the next element now.  */
              this->optind++;
            }
          else if (this->optind == argc)
            {
              printf("Option requires an argument -- '%c'\n", c);
              this->optopt = c;
              if (optstring[0] == ':')
                c = ':';
              else
                c = '?';
              return c;
            }
          else
            /* We already incremented `d->optind' once;
             increment it again when taking next ARGV-elt as argument.  */
            this->optarg = argv[this->optind++];

          /* optarg is now the argument, see if it's in the
           table of longopts.  */

          for (this->__nextchar = nameend = this->optarg; *nameend && *nameend
              != '='; nameend++)
            /* Do nothing.  */;

          /* Test all long options for either exact match
           or abbreviated matches.  */
          for (p = longopts, option_index = 0; p->name; p++, option_index++)
            if (!strncmp(p->name, this->__nextchar, nameend - this->__nextchar))
              {
                if ((unsigned int) (nameend - this->__nextchar) == strlen(
                    p->name))
                  {
                    /* Exact match found.  */
                    pfound = p;
                    indfound = option_index;
                    exact = 1;
                    break;
                  }
                else if (pfound == NULL)
                  {
                    /* First nonexact match found.  */
                    pfound = p;
                    indfound = option_index;
                  }
                else
                  /* Second or later nonexact match found.  */
                  ambig = 1;
              }
          if (ambig && !exact)
            {
              printf("Option '-W %s' is ambiguous\n", argv[this->optind]);
              this->__nextchar += strlen(this->__nextchar);
              this->optind++;
              return '?';
            }
          if (pfound != NULL)
            {
              option_index = indfound;
              if (*nameend)
                {
                  /* Don't test has_arg with >, because some C compilers don't
                   allow it to be used on enums.  */
                  if (pfound->has_arg)
                    this->optarg = nameend + 1;
                  else
                    {
                      printf("Option '-W %s' doesn't allow an argument\n",
                          pfound->name);
                      this->__nextchar += strlen(this->__nextchar);
                      return '?';
                    }
                }
              else if (pfound->has_arg == 1)
                {
                  if (this->optind < argc)
                    this->optarg = argv[this->optind++];
                  else
                    {
                      printf("Option '%s' requires an argument\n",
                          argv[this->optind - 1]);
                      this->__nextchar += strlen(this->__nextchar);
                      return optstring[0] == ':' ? ':' : '?';
                    }
                }
              this->__nextchar += strlen(this->__nextchar);
              if (longind != NULL)
                *longind = option_index;
              if (pfound->flag)
                {
                  *(pfound->flag) = pfound->val;
                  return 0;
                }
              return pfound->val;
            }
          this->__nextchar = NULL;
          return 'W'; /* Let the application handle it.   */
        }
      if (temp[1] == ':')
        {
          if (temp[2] == ':')
            {
              /* This is an option that accepts an argument optionally.  */
              if (*this->__nextchar != '\0')
                {
                  this->optarg = this->__nextchar;
                  this->optind++;
                }
              else
                this->optarg = NULL;
              this->__nextchar = NULL;
            }
          else
            {
              /* This is an option that requires an argument.  */
              if (*this->__nextchar != '\0')
                {
                  this->optarg = this->__nextchar;
                  /* If we end this ARGV-element by taking the rest as an arg,
                   we must advance to the next element now.  */
                  this->optind++;
                }
              else if (this->optind == argc)
                {
                  printf("Option requires an argument -- '%c'\n", c);
                  this->optopt = c;
                  if (optstring[0] == ':')
                    c = ':';
                  else
                    c = '?';
                }
              else
                /* We already incremented `optind' once;
                 increment it again when taking next ARGV-elt as argument.  */
                this->optarg = argv[this->optind++];
              this->__nextchar = NULL;
            }
        }
      return c;
    }
}

RTConfig::RTConfig(const char* cmdline) :  argc(0), cmdline_begin((char*)cmdline), _quiet(false)
{
  if(cmdline == NULL)
    return;

  // separator for the next argument
  char sep(' ');
  char* c;
  for (c = ((char*) cmdline); *c != 0; ++c)
    {
      if (argc == ARGC_MAX)
        panic("Too many options in the command line");
      while (*c == ' ')
        ++c;
      if(*c == '"') {
        ++c;
        sep = '"';
      } else
        sep = ' ';
      argv[argc] = c;
      ++argc;
      while(*c != sep and *c != 0)
        ++c;
      *c = 0;
    }
  cmdline_end = c;
  option opts[] =
    {
      { "quiet", option::no_argument, NULL, 'q' },
      { NULL, option::no_argument, NULL, 0 } };
  GetOpt go;
  while (true)
    {
      int optindex;
      char c = go.get(argc, argv, "q", opts, &optindex);
      if(c == -1)
        break;
      switch (c) {
      case '?':
        panic();
        break;
      case 'q':
        _quiet = true;
        break;
      default:
        panic("Getopt bug");
      }
    }
  if(go.optind == argc)
    panic("Missing init command line");
  else
    this->init_cmd = go.optind;
  if(!_quiet)
      printf("Loading " PACKAGE_STRING "...\n");
}

ExecCommand
RTConfig::getInitCmd() {
  char* newcmdline = new char[cmdline_end - cmdline_begin];
  char** newargv = new char*[argc - init_cmd];
  memset(newargv,0,sizeof(char*)*(argc-init_cmd));
  size_t pos(0);
  for(int i(0);i<(argc - init_cmd);++i) {
    char* src = argv[i+init_cmd];
    char* dst = newargv[i] = newcmdline+pos;
    while(*src)
      *dst++ = *src++;
    *dst = 0;
    pos = dst - (newcmdline+pos)+1;
  }
  char** envp = new char*[1];
  envp[0] = NULL;
  return ExecCommand(newargv[0],newargv,envp);
}
