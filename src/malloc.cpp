/* malloc.cpp - kernel memory allocator (implementation) */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system .

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/* This file includes code from the GNU C Library
 Copyright (C) 1996-2006, 2007, 2008, 2009 Free Software Foundation, Inc.
 Contributed by Wolfram Gloger <wg@malloc.de>
 and Doug Lea <dl@cs.oswego.edu>, 2001.

 This is a version (aka ptmalloc2) of malloc/free/realloc written by
 Doug Lea and adapted to multiple threads/arenas by Wolfram Gloger.

 There have been substantial changesmade after the integration into
 glibc in all parts of the code.  Do not look for much commonality
 with the ptmalloc2 version.

 * Version ptmalloc2-20011215
 based on:
 VERSION 2.7.0 Sun Mar 11 14:14:06 2001  Doug Lea  (dl at gee)
 */

#include <stdlib/stdlib.h>
#include <stdlib/stdarg.h>
#include <stdlib/assert.h>
#include <architecture.hpp>
#include <kernel-sync.hpp>
#include <mmu.hpp>
#include <panic.hpp>
#define MALLOC_IMPL
#include <malloc.hpp>

/* The corresponding word size */
#define SIZE_SZ                (sizeof(size_t))

/*
 MALLOC_ALIGNMENT is the minimum alignment for malloc'ed chunks.
 It must be a power of two at least 2 * SIZE_SZ, even on machines
 for which smaller alignments would suffice. It may be defined as
 larger than this though. Note however that code and data structures
 are optimized for the case of 8-byte alignment.
 */

#ifndef MALLOC_ALIGNMENT
#define MALLOC_ALIGNMENT       (2 * SIZE_SZ)
#endif

/* The corresponding bit mask value */
#define MALLOC_ALIGN_MASK      (MALLOC_ALIGNMENT - 1)

/*
 TRIM_FASTBINS controls whether free() of a very small chunk can
 immediately lead to trimming. Setting to true (1) can reduce memory
 footprint, but will almost always slow down programs that use a lot
 of small chunks.

 This would be true if mmap/sbrk were system calls. As instead they
 are just local calls, trimming memory is almost always the right
 thing to do.
 */

#define TRIM_FASTBINS  1

/*
 HAVE_MEMCPY should be defined if you are not otherwise using
 ANSI STD C, but still have memcpy and memset in your C library
 and want to use them in calloc and realloc. Otherwise simple
 macro versions are defined below.

 USE_MEMCPY should be defined as 1 if you actually want to
 have memset and memcpy called. People report that the macro
 versions are faster than libc versions on some systems.

 Even if USE_MEMCPY is set to 1, loops to copy/clear small chunks
 (of <= 36 bytes) are manually unrolled in realloc and calloc.
 */

#define USE_MEMCPY 1

/* Force a value to be in a register and stop the compiler referring
 to the source (mostly memory location) again.  */
#define force_reg(val) \
  ({ __typeof (val) _v; asm ("" : "=r" (_v) : "0" (val)); _v; })

/*  MORECORE_CLEARS           (default 1)
 The degree to which the routine mapped to MORECORE zeroes out
 memory: never (0), only for newly allocated space (1) or always
 (2).  The distinction between (1) and (2) is necessary because on
 some systems, if the application first decrements and then
 increments the break value, the contents of the reallocated space
 are unspecified.
 */

#define MORECORE_CLEARS 0

/*
 Define HAVE_MMAP as true to optionally make malloc() use mmap() to
 allocate very large blocks.  These will be returned to the
 operating system immediately after a free(). Also, if mmap
 is available, it is used as a backup strategy in cases where
 MORECORE fails to provide space from system.

 This malloc is best tuned to work with mmap for large requests.
 If you do not have mmap, operations involving very large chunks (1MB
 or so) may be slower than you'd like.
 */
/* Unfortunately, mmap causes a recursive lock at the moment, so
   we'd better not use it.
 */
#define HAVE_MMAP 0

/*
 Standard unix mmap using /dev/zero clears memory so calloc doesn't
 need to.
 */
#define MMAP_CLEARS 1

/*
 MMAP_AS_MORECORE_SIZE is the minimum mmap size argument to use if
 sbrk fails, and mmap is used as a backup (which is done only if
 HAVE_MMAP).  The value must be a multiple of page size.  This
 backup strategy generally applies only when systems have "holes" in
 address space, so sbrk cannot perform contiguous expansion, but
 there is still space available on system.  On systems for which
 this is known to be useful (i.e. most linux kernels), this occurs
 only when programs allocate huge amounts of memory.  Between this,
 and the fact that mmap regions tend to be limited, the size should
 be large, to avoid too many mmap calls and thus avoid running out
 of kernel resources.
 */
#define MMAP_AS_MORECORE_SIZE (1024 * 1024)

/* Define USE_ARENAS to enable support for multiple `arenas'.  These
 are allocated using mmap(), are necessary for threads and
 occasionally useful to overcome address space limitations affecting
 sbrk(). Currently not implemented. */

#define USE_ARENAS 0

/* mallopt tuning options */

/*
 M_MXFAST is the maximum request size used for "fastbins", special bins
 that hold returned chunks without consolidating their spaces. This
 enables future requests for chunks of the same size to be handled
 very quickly, but can increase fragmentation, and thus increase the
 overall memory footprint of a program.

 This malloc manages fastbins very conservatively yet still
 efficiently, so fragmentation is rarely a problem for values less
 than or equal to the default.  The maximum supported value of MXFAST
 is 80. You wouldn't want it any higher than this anyway.  Fastbins
 are designed especially for use with many small structs, objects or
 strings -- the default handles structs/objects/arrays with sizes up
 to 8 4byte fields, or small strings representing words, tokens,
 etc. Using fastbins for larger objects normally worsens
 fragmentation without improving speed.

 M_MXFAST is set in REQUEST size units. It is internally used in
 chunksize units, which adds padding and alignment.  You can reduce
 M_MXFAST to 0 to disable all use of fastbins.  This causes the malloc
 algorithm to be a closer approximation of fifo-best-fit in all cases,
 not just for larger requests, but will generally cause it to be
 slower.
 */

/* M_MXFAST is a standard SVID/XPG tuning option, usually listed in malloc.h */
#ifndef M_MXFAST
#define M_MXFAST            1
#endif

#ifndef DEFAULT_MXFAST
#define DEFAULT_MXFAST     (64 * SIZE_SZ / 4)
#endif

/*
 M_TRIM_THRESHOLD is the maximum amount of unused top-most memory
 to keep before releasing via malloc_trim in free().

 Automatic trimming is mainly useful in long-lived programs.
 Because trimming via sbrk can be slow on some systems, and can
 sometimes be wasteful (in cases where programs immediately
 afterward allocate more large chunks) the value should be high
 enough so that your overall system performance would improve by
 releasing this much memory.

 The trim threshold and the mmap control parameters (see below)
 can be traded off with one another. Trimming and mmapping are
 two different ways of releasing unused memory back to the
 system. Between these two, it is often possible to keep
 system-level demands of a long-lived program down to a bare
 minimum. For example, in one test suite of sessions measuring
 the XF86 X server on Linux, using a trim threshold of 128K and a
 mmap threshold of 192K led to near-minimal long term resource
 consumption.

 If you are using this malloc in a long-lived program, it should
 pay to experiment with these values.  As a rough guide, you
 might set to a value close to the average size of a process
 (program) running on your system.  Releasing this much memory
 would allow such a process to run in memory.  Generally, it's
 worth it to tune for trimming rather tham memory mapping when a
 program undergoes phases where several large chunks are
 allocated and released in ways that can reuse each other's
 storage, perhaps mixed with phases where there are no such
 chunks at all.  And in well-behaved long-lived programs,
 controlling release of large blocks via trimming versus mapping
 is usually faster.

 However, in most programs, these parameters serve mainly as
 protection against the system-level effects of carrying around
 massive amounts of unneeded memory. Since frequent calls to
 sbrk, mmap, and munmap otherwise degrade performance, the default
 parameters are set to relatively high values that serve only as
 safeguards.

 The trim value It must be greater than page size to have any useful
 effect.  To disable trimming completely, you can set to
 (unsigned long)(-1)

 Trim settings interact with fastbin (MXFAST) settings: Unless
 TRIM_FASTBINS is defined, automatic trimming never takes place upon
 freeing a chunk with size less than or equal to MXFAST. Trimming is
 instead delayed until subsequent freeing of larger chunks. However,
 you can still force an attempted trim by calling malloc_trim.

 Also, trimming is not generally possible in cases where
 the main arena is obtained via mmap.

 Note that the trick some people use of mallocing a huge space and
 then freeing it at program startup, in an attempt to reserve system
 memory, doesn't have the intended effect under automatic trimming,
 since that memory will immediately be returned to the system.
 */

#define M_TRIM_THRESHOLD       -1

#ifndef DEFAULT_TRIM_THRESHOLD
#define DEFAULT_TRIM_THRESHOLD (128 * 1024)
#endif

/*
 M_TOP_PAD is the amount of extra `padding' space to allocate or
 retain whenever sbrk is called. It is used in two ways internally:

 * When sbrk is called to extend the top of the arena to satisfy
 a new malloc request, this much padding is added to the sbrk
 request.

 * When malloc_trim is called automatically from free(),
 it is used as the `pad' argument.

 In both cases, the actual amount of padding is rounded
 so that the end of the arena is always a system page boundary.

 The main reason for using padding is to avoid calling sbrk so
 often. Having even a small pad greatly reduces the likelihood
 that nearly every malloc request during program start-up (or
 after trimming) will invoke sbrk, which needlessly wastes
 time.

 Automatic rounding-up to page-size units is normally sufficient
 to avoid measurable overhead, so the default is 0.  However, in
 systems where sbrk is relatively slow, it can pay to increase
 this value, at the expense of carrying around more memory than
 the program needs.
 */

#define M_TOP_PAD              -2

#ifndef DEFAULT_TOP_PAD
#define DEFAULT_TOP_PAD        (0)
#endif

/*
 MMAP_THRESHOLD_MAX and _MIN are the bounds on the dynamically
 adjusted MMAP_THRESHOLD.
 */

#ifndef DEFAULT_MMAP_THRESHOLD_MIN
#define DEFAULT_MMAP_THRESHOLD_MIN (128 * 1024)
#endif

#ifndef DEFAULT_MMAP_THRESHOLD_MAX
/* For 32-bit platforms we cannot increase the maximum mmap
 threshold much because it is also the minimum value for the
 maximum heap size and its alignment.  Going above 512k (i.e., 1M
 for new heaps) wastes too much address space.  */
# if __WORDSIZE == 32
#  define DEFAULT_MMAP_THRESHOLD_MAX (512 * 1024)
# else
#  define DEFAULT_MMAP_THRESHOLD_MAX (4 * 1024 * 1024 * sizeof(long))
# endif
#endif

/*
 M_MMAP_THRESHOLD is the request size threshold for using mmap()
 to service a request. Requests of at least this size that cannot
 be allocated using already-existing space will be serviced via mmap.
 (If enough normal freed space already exists it is used instead.)

 Using mmap segregates relatively large chunks of memory so that
 they can be individually obtained and released from the host
 system. A request serviced through mmap is never reused by any
 other request (at least not directly; the system may just so
 happen to remap successive requests to the same locations).

 Segregating space in this way has the benefits that:

 1. Mmapped space can ALWAYS be individually released back
 to the system, which helps keep the system level memory
 demands of a long-lived program low.
 2. Mapped memory can never become `locked' between
 other chunks, as can happen with normally allocated chunks, which
 means that even trimming via malloc_trim would not release them.
 3. On some systems with "holes" in address spaces, mmap can obtain
 memory that sbrk cannot.

 However, it has the disadvantages that:

 1. The space cannot be reclaimed, consolidated, and then
 used to service later requests, as happens with normal chunks.
 2. It can lead to more wastage because of mmap page alignment
 requirements
 3. It causes malloc performance to be more dependent on host
 system memory management support routines which may vary in
 implementation quality and may impose arbitrary
 limitations. Generally, servicing a request via normal
 malloc steps is faster than going through a system's mmap.

 The advantages of mmap nearly always outweigh disadvantages for
 "large" chunks, but the value of "large" varies across systems.  The
 default is an empirically derived value that works well in most
 systems.


 Update in 2006:
 The above was written in 2001. Since then the world has changed a lot.
 Memory got bigger. Applications got bigger. The virtual address space
 layout in 32 bit linux changed.

 In the new situation, brk() and mmap space is shared and there are no
 artificial limits on brk size imposed by the kernel. What is more,
 applications have started using transient allocations larger than the
 128Kb as was imagined in 2001.

 The price for mmap is also high now; each time glibc mmaps from the
 kernel, the kernel is forced to zero out the memory it gives to the
 application. Zeroing memory is expensive and eats a lot of cache and
 memory bandwidth. This has nothing to do with the efficiency of the
 virtual memory system, by doing mmap the kernel just has no choice but
 to zero.

 In 2001, the kernel had a maximum size for brk() which was about 800
 megabytes on 32 bit x86, at that point brk() would hit the first
 mmaped shared libaries and couldn't expand anymore. With current 2.6
 kernels, the VA space layout is different and brk() and mmap
 both can span the entire heap at will.

 Rather than using a static threshold for the brk/mmap tradeoff,
 we are now using a simple dynamic one. The goal is still to avoid
 fragmentation. The old goals we kept are
 1) try to get the long lived large allocations to use mmap()
 2) really large allocations should always use mmap()
 and we're adding now:
 3) transient allocations should use brk() to avoid forcing the kernel
 having to zero memory over and over again

 The implementation works with a sliding threshold, which is by default
 limited to go between 128Kb and 32Mb (64Mb for 64 bitmachines) and starts
 out at 128Kb as per the 2001 default.

 This allows us to satisfy requirement 1) under the assumption that long
 lived allocations are made early in the process' lifespan, before it has
 started doing dynamic allocations of the same size (which will
 increase the threshold).

 The upperbound on the threshold satisfies requirement 2)

 The threshold goes up in value when the application frees memory that was
 allocated with the mmap allocator. The idea is that once the application
 starts freeing memory of a certain size, it's highly probable that this is
 a size the application uses for transient allocations. This estimator
 is there to satisfy the new third requirement.

 */

#define M_MMAP_THRESHOLD      -3

#ifndef DEFAULT_MMAP_THRESHOLD
#define DEFAULT_MMAP_THRESHOLD DEFAULT_MMAP_THRESHOLD_MIN
#endif

/*
 M_MMAP_MAX is the maximum number of requests to simultaneously
 service using mmap. This parameter exists because
 some systems have a limited number of internal tables for
 use by mmap, and using more than a few of them may degrade
 performance.

 The default is set to a value that serves only as a safeguard.
 Setting to 0 disables use of mmap for servicing large requests.  If
 HAVE_MMAP is not set, the default value is 0, and attempts to set it
 to non-zero values in mallopt will fail.
 */

#define M_MMAP_MAX             -4

#ifndef DEFAULT_MMAP_MAX
#if HAVE_MMAP
#define DEFAULT_MMAP_MAX       (65536)
#else
#define DEFAULT_MMAP_MAX       (0)
#endif
#endif

/* Forward declarations.  */

/* Internal routines.  */
#if 0
static void*
malloc_check(size_t sz, const void *caller);
static void
free_check(void* mem, const void *caller);
static void*
realloc_check(void* oldmem, size_t bytes, const void *caller);
static void*
memalign_check(size_t alignment, size_t bytes, const void *caller);
#ifndef NO_THREADS
# ifdef _LIBC
#  if USE___THREAD || !defined SHARED
/* These routines are never needed in this configuration.  */
#   define NO_STARTER
#  endif
# endif
# ifdef NO_STARTER
#  undef NO_STARTER
# else
static void*
malloc_starter(size_t sz, const void *caller);
static void*
memalign_starter(size_t aln, size_t sz, const void *caller);
static void
free_starter(void* mem, const void *caller);
# endif
static void*
malloc_atfork(size_t sz, const void *caller);
static void
free_atfork(void* mem, const void *caller);
#endif
#endif

/*
 -----------------------  Chunk representations -----------------------
 */

/*
 malloc_chunk details:

 (The following includes lightly edited explanations by Colin Plumb.)

 Chunks of memory are maintained using a `boundary tag' method as
 described in e.g., Knuth or Standish.  (See the paper by Paul
 Wilson ftp://ftp.cs.utexas.edu/pub/garbage/allocsrv.ps for a
 survey of such techniques.)  Sizes of free chunks are stored both
 in the front of each chunk and at the end.  This makes
 consolidating fragmented chunks into bigger chunks very fast.  The
 size fields also hold bits representing whether chunks are free or
 in use.

 An allocated chunk looks like this:


 chunk-> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             Size of previous chunk, if allocated            | |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             Size of chunk, in bytes                       |M|P|
 mem-> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             User data starts here...                          .
 .                                                               .
 .             (malloc_usable_size() bytes)                      .
 .                                                               |
 nextchunk-> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             Size of chunk                                     |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


 Where "chunk" is the front of the chunk for the purpose of most of
 the malloc code, but "mem" is the pointer that is returned to the
 user.  "Nextchunk" is the beginning of the next contiguous chunk.

 Chunks always begin on even word boundries, so the mem portion
 (which is returned to the user) is also on an even word boundary, and
 thus at least double-word aligned.

 Free chunks are stored in circular doubly-linked lists, and look like this:

 chunk-> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             Size of previous chunk                            |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 `head:' |             Size of chunk, in bytes                         |P|
 mem-> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             Forward pointer to next chunk in list             |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             Back pointer to previous chunk in list            |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |             Unused space (may be 0 bytes long)                .
 .                                                               .
 .                                                               |
 nextchunk-> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 `foot:' |             Size of chunk, in bytes                           |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

 The P (PREV_INUSE) bit, stored in the unused low-order bit of the
 chunk size (which is always a multiple of two words), is an in-use
 bit for the *previous* chunk.  If that bit is *clear*, then the
 word before the current chunk size contains the previous chunk
 size, and can be used to find the front of the previous chunk.
 The very first chunk allocated always has this bit set,
 preventing access to non-existent (or non-owned) memory. If
 prev_inuse is set for any given chunk, then you CANNOT determine
 the size of the previous chunk, and might even get a memory
 addressing fault when trying to do so.

 Note that the `foot' of the current chunk is actually represented
 as the prev_size of the NEXT chunk. This makes it easier to
 deal with alignments etc but can be very confusing when trying
 to extend or adapt this code.

 The two exceptions to all this are

 1. The special chunk `top' doesn't bother using the
 trailing size field since there is no next contiguous chunk
 that would have to index off it. After initialization, `top'
 is forced to always exist.  If it would become less than
 MINSIZE bytes long, it is replenished.

 2. Chunks allocated via mmap, which have the second-lowest-order
 bit M (IS_MMAPPED) set in their size fields.  Because they are
 allocated one-by-one, each must contain its own trailing size field.

 */

/*
 ---------- Size and alignment checks and conversions ----------
 */

/* conversion from malloc headers to user pointers, and back */

#define chunk2mem(p)   ((void*)((char*)(p) + 2*SIZE_SZ))
#define mem2chunk(mem) ((Allocator::mchunkptr)((char*)(mem) - 2*SIZE_SZ))

/* The smallest possible chunk */
#define MIN_CHUNK_SIZE        (offsetof(Allocator::malloc_chunk, fd_nextsize))

/* The smallest size we can malloc is an aligned minimal chunk */

#define MINSIZE  \
  (unsigned long)(((MIN_CHUNK_SIZE+MALLOC_ALIGN_MASK) & ~MALLOC_ALIGN_MASK))

/* Check if m has acceptable alignment */

#define aligned_OK(m)  (((unsigned long)(m) & MALLOC_ALIGN_MASK) == 0)

#define misaligned_chunk(p) \
  ((uintptr_t)(MALLOC_ALIGNMENT == 2 * SIZE_SZ ? (p) : chunk2mem (p)) \
   & MALLOC_ALIGN_MASK)

/*
 Check if a request is so large that it would wrap around zero when
 padded and aligned. To simplify some other code, the bound is made
 low enough so that adding MINSIZE will also not wrap around zero.
 */

#define REQUEST_OUT_OF_RANGE(req)                                 \
  ((unsigned long)(req) >=                                        \
   (unsigned long)(size_t)(-2 * MINSIZE))

/* pad request bytes into a usable size -- internal version */

#define request2size(req)                                         \
  (((req) + SIZE_SZ + MALLOC_ALIGN_MASK < MINSIZE)  ?             \
   MINSIZE :                                                      \
   ((req) + SIZE_SZ + MALLOC_ALIGN_MASK) & ~MALLOC_ALIGN_MASK)

/*  Same, except also perform argument check */

#define checked_request2size(req, sz)                             \
  if (REQUEST_OUT_OF_RANGE(req)) {                                \
    return 0;                                                     \
  }                                                               \
  (sz) = request2size(req);

/*
 --------------- Physical chunk operations ---------------
 */

/* size field is or'ed with PREV_INUSE when previous adjacent chunk in use */
#define PREV_INUSE 0x1

/* extract inuse bit of previous chunk */
#define prev_inuse(p)       ((p)->size & PREV_INUSE)

/* size field is or'ed with IS_MMAPPED if the chunk was obtained with mmap() */
#define IS_MMAPPED 0x2

/* check for mmap()'ed chunk */
#define chunk_is_mmapped(p) ((p)->size & IS_MMAPPED)

/* size field is or'ed with NON_MAIN_ARENA if the chunk was obtained
 from a non-main arena.  This is only set immediately before handing
 the chunk to the user, if necessary.  */
#define NON_MAIN_ARENA 0x4

/* check for chunk from non-main arena */
#define chunk_non_main_arena(p) ((p)->size & NON_MAIN_ARENA)

/*
 Bits to mask off when extracting size

 Note: IS_MMAPPED is intentionally not masked off from size field in
 macros for which mmapped chunks should never be seen. This should
 cause helpful core dumps to occur if it is tried by accident by
 people extending or adapting this malloc.
 */
#define SIZE_BITS (PREV_INUSE|IS_MMAPPED|NON_MAIN_ARENA)

/* Get size, ignoring use bits */
#define chunksize(p)         ((p)->size & ~(SIZE_BITS))

/* Ptr to next physical malloc_chunk. */
#define next_chunk(p) ((Allocator::mchunkptr)( ((char*)(p)) + ((p)->size & ~SIZE_BITS) ))

/* Ptr to previous physical malloc_chunk */
#define prev_chunk(p) ((Allocator::mchunkptr)( ((char*)(p)) - ((p)->prev_size) ))

/* Treat space at ptr + offset as a chunk */
#define chunk_at_offset(p, s)  ((Allocator::mchunkptr)(((char*)(p)) + (s)))

/* extract p's inuse bit */
#define inuse(p)\
((((Allocator::mchunkptr)(((char*)(p))+((p)->size & ~SIZE_BITS)))->size) & PREV_INUSE)

/* set/clear chunk as being inuse without otherwise disturbing */
#define set_inuse(p)\
((Allocator::mchunkptr)(((char*)(p)) + ((p)->size & ~SIZE_BITS)))->size |= PREV_INUSE

#define clear_inuse(p)\
((Allocator::mchunkptr)(((char*)(p)) + ((p)->size & ~SIZE_BITS)))->size &= ~(PREV_INUSE)

/* check/set/clear inuse bits in known places */
#define inuse_bit_at_offset(p, s)\
 (((Allocator::mchunkptr)(((char*)(p)) + (s)))->size & PREV_INUSE)

#define set_inuse_bit_at_offset(p, s)\
 (((Allocator::mchunkptr)(((char*)(p)) + (s)))->size |= PREV_INUSE)

#define clear_inuse_bit_at_offset(p, s)\
 (((Allocator::mchunkptr)(((char*)(p)) + (s)))->size &= ~(PREV_INUSE))

/* Set size at head, without disturbing its use bit */
#define set_head_size(p, s)  ((p)->size = (((p)->size & SIZE_BITS) | (s)))

/* Set size/use field */
#define set_head(p, s)       ((p)->size = (s))

/* Set size at footer (only when chunk is not in use) */
#define set_foot(p, s)       (((Allocator::mchunkptr)((char*)(p) + (s)))->prev_size = (s))

/*
 -------------------- Internal data structures --------------------

 All internal state is held in an instance of malloc_state defined
 below. There are no other static variables, except in two optional
 cases:
 * If USE_MALLOC_LOCK is defined, the mALLOC_MUTEx declared above.
 * If HAVE_MMAP is true, but mmap doesn't support
 MAP_ANONYMOUS, a dummy file descriptor for mmap.

 Beware of lots of tricks that minimize the total bookkeeping space
 requirements. The result is a little over 1K bytes (for 4byte
 pointers and size_t.)
 */

/*
 Bins

 An array of bin headers for free chunks. Each bin is doubly
 linked.  The bins are approximately proportionally (log) spaced.
 There are a lot of these bins (128). This may look excessive, but
 works very well in practice.  Most bins hold sizes that are
 unusual as malloc request sizes, but are more usual for fragments
 and consolidated sets of chunks, which is what these bins hold, so
 they can be found quickly.  All procedures maintain the invariant
 that no consolidated chunk physically borders another one, so each
 chunk in a list is known to be preceeded and followed by either
 inuse chunks or the ends of memory.

 Chunks in bins are kept in size order, with ties going to the
 approximately least recently used chunk. Ordering isn't needed
 for the small bins, which all contain the same-sized chunks, but
 facilitates best-fit allocation for larger chunks. These lists
 are just sequential. Keeping them in order almost never requires
 enough traversal to warrant using fancier ordered data
 structures.

 Chunks of the same size are linked with the most
 recently freed at the front, and allocations are taken from the
 back.  This results in LRU (FIFO) allocation order, which tends
 to give each chunk an equal opportunity to be consolidated with
 adjacent freed chunks, resulting in larger free chunks and less
 fragmentation.

 To simplify use in double-linked lists, each bin header acts
 as a malloc_chunk. This avoids special-casing for headers.
 But to conserve space and improve locality, we allocate
 only the fd/bk pointers of bins, and then use repositioning tricks
 to treat these as the fields of a malloc_chunk*.
 */

/* addressing -- note that bin_at(0) does not exist */
#define bin_at(m, i) \
  (Allocator::mbinptr) (((char *) &((m)->bins[((i) - 1) * 2]))                           \
             - offsetof (Allocator::malloc_chunk, fd))

/* analog of ++bin */
#define next_bin(b)  ((Allocator::mbinptr)((char*)(b) + (sizeof(Allocator::mchunkptr)<<1)))

/* Reminders about list directionality within bins */
#define first(b)     ((b)->fd)
#define last(b)      ((b)->bk)

/* Take a chunk off a bin list */
#define unlink(P, BK, FD) {                                            \
  FD = P->fd;                                                          \
  BK = P->bk;                                                          \
  if (__builtin_expect (FD->bk != P || BK->fd != P, 0))                \
    Allocator::report_error ("corrupted double-linked list", P);       \
  else {                                                               \
    FD->bk = BK;                                                       \
    BK->fd = FD;                                                       \
    if (!in_smallbin_range (P->size)                                   \
        && __builtin_expect (P->fd_nextsize != NULL, 0)) {             \
      assert (P->fd_nextsize->bk_nextsize == P);                       \
      assert (P->bk_nextsize->fd_nextsize == P);                       \
      if (FD->fd_nextsize == NULL) {                                   \
        if (P->fd_nextsize == P)                                       \
          FD->fd_nextsize = FD->bk_nextsize = FD;                      \
        else {                                                         \
          FD->fd_nextsize = P->fd_nextsize;                            \
          FD->bk_nextsize = P->bk_nextsize;                            \
          P->fd_nextsize->bk_nextsize = FD;                            \
          P->bk_nextsize->fd_nextsize = FD;                            \
        }                                                              \
      } else {                                                         \
        P->fd_nextsize->bk_nextsize = P->bk_nextsize;                  \
        P->bk_nextsize->fd_nextsize = P->fd_nextsize;                  \
      }                                                                \
    }                                                                  \
  }                                                                    \
}

/*
 Indexing

 Bins for sizes < 512 bytes contain chunks of all the same size, spaced
 8 bytes apart. Larger bins are approximately logarithmically spaced:

 64 bins of size       8
 32 bins of size      64
 16 bins of size     512
 8 bins of size    4096
 4 bins of size   32768
 2 bins of size  262144
 1 bin  of size what's left

 There is actually a little bit of slop in the numbers in bin_index
 for the sake of speed. This makes no difference elsewhere.

 The bins top out around 1MB because we expect to service large
 requests via mmap.
 */

#define NBINS             128
#define NSMALLBINS         64
#define SMALLBIN_WIDTH    MALLOC_ALIGNMENT
#define MIN_LARGE_SIZE    (NSMALLBINS * SMALLBIN_WIDTH)

#define in_smallbin_range(sz)  \
  ((unsigned long)(sz) < (unsigned long)MIN_LARGE_SIZE)

#define smallbin_index(sz) \
  (SMALLBIN_WIDTH == 16 ? (((unsigned)(sz)) >> 4) : (((unsigned)(sz)) >> 3))

#define largebin_index_32(sz)                                                \
(((((unsigned long)(sz)) >>  6) <= 38)?  56 + (((unsigned long)(sz)) >>  6): \
 ((((unsigned long)(sz)) >>  9) <= 20)?  91 + (((unsigned long)(sz)) >>  9): \
 ((((unsigned long)(sz)) >> 12) <= 10)? 110 + (((unsigned long)(sz)) >> 12): \
 ((((unsigned long)(sz)) >> 15) <=  4)? 119 + (((unsigned long)(sz)) >> 15): \
 ((((unsigned long)(sz)) >> 18) <=  2)? 124 + (((unsigned long)(sz)) >> 18): \
                                        126)

// XXX It remains to be seen whether it is good to keep the widths of
//     the buckets the same or whether it should be scaled by a factor
//     of two as well.
#define largebin_index_64(sz)                                                \
(((((unsigned long)(sz)) >>  6) <= 48)?  48 + (((unsigned long)(sz)) >>  6): \
 ((((unsigned long)(sz)) >>  9) <= 20)?  91 + (((unsigned long)(sz)) >>  9): \
 ((((unsigned long)(sz)) >> 12) <= 10)? 110 + (((unsigned long)(sz)) >> 12): \
 ((((unsigned long)(sz)) >> 15) <=  4)? 119 + (((unsigned long)(sz)) >> 15): \
 ((((unsigned long)(sz)) >> 18) <=  2)? 124 + (((unsigned long)(sz)) >> 18): \
                                        126)

#define largebin_index(sz) \
  (SIZE_SZ == 8 ? largebin_index_64 (sz) : largebin_index_32 (sz))

#define bin_index(sz) \
 ((in_smallbin_range(sz)) ? smallbin_index(sz) : largebin_index(sz))

/*
 Unsorted chunks

 All remainders from chunk splits, as well as all returned chunks,
 are first placed in the "unsorted" bin. They are then placed
 in regular bins after malloc gives them ONE chance to be used before
 binning. So, basically, the unsorted_chunks list acts as a queue,
 with chunks being placed on it in free (and malloc_consolidate),
 and taken off (to be either used or placed in bins) in malloc.

 The NON_MAIN_ARENA flag is never set for unsorted chunks, so it
 does not have to be taken into account in size comparisons.
 */

/* The otherwise unindexable 1-bin is used to hold unsorted chunks. */
#define unsorted_chunks(M)          (bin_at(M, 1))

/*
 Top

 The top-most available chunk (i.e., the one bordering the end of
 available memory) is treated specially. It is never included in
 any bin, is used only if no other chunk is available, and is
 released back to the system if it is very large (see
 M_TRIM_THRESHOLD).  Because top initially
 points to its own bin with initial zero size, thus forcing
 extension on the first malloc request, we avoid having any special
 code in malloc to check whether it even exists yet. But we still
 need to do so when getting memory from system, so we make
 initial_top treat the bin as a legal but unusable chunk during the
 interval between initialization and the first call to
 sYSMALLOc. (This is somewhat delicate, since it relies on
 the 2 preceding words to be zero during this interval as well.)
 */

/* Conveniently, the unsorted bin can be used as dummy top on first call */
#define initial_top(M)              (unsorted_chunks(M))

/*
 Binmap

 To help compensate for the large number of bins, a one-level index
 structure is used for bin-by-bin searching.  `binmap' is a
 bitvector recording whether bins are definitely empty so they can
 be skipped over during during traversals.  The bits are NOT always
 cleared as soon as bins are empty, but instead only
 when they are noticed to be empty during traversal in malloc.
 */

/* Conservatively use 32 bits per map word, even if on 64bit system */
#define BINMAPSHIFT      5
#define BITSPERMAP       (1U << BINMAPSHIFT)
#define BINMAPSIZE       (NBINS / BITSPERMAP)

#define idx2block(i)     ((i) >> BINMAPSHIFT)
#define idx2bit(i)       ((1U << ((i) & ((1U << BINMAPSHIFT)-1))))

#define mark_bin(m,i)    ((m)->binmap[idx2block(i)] |=  idx2bit(i))
#define unmark_bin(m,i)  ((m)->binmap[idx2block(i)] &= ~(idx2bit(i)))
#define get_binmap(m,i)  ((m)->binmap[idx2block(i)] &   idx2bit(i))

/*
 Fastbins

 An array of lists holding recently freed small chunks.  Fastbins
 are not doubly linked.  It is faster to single-link them, and
 since chunks are never removed from the middles of these lists,
 double linking is not necessary. Also, unlike regular bins, they
 are not even processed in FIFO order (they use faster LIFO) since
 ordering doesn't much matter in the transient contexts in which
 fastbins are normally used.

 Chunks in fastbins keep their inuse bit set, so they cannot
 be consolidated with other free chunks. malloc_consolidate
 releases all chunks in fastbins and consolidates them with
 other free chunks.
 */

#define fastbin(ar_ptr, idx) ((ar_ptr)->fastbinsY[idx])

/* offset 2 to use otherwise unindexable first 2 bins */
#define fastbin_index(sz) \
  ((((unsigned int)(sz)) >> (SIZE_SZ == 8 ? 4 : 3)) - 2)

/* The maximum fastbin request size we support */
#define MAX_FAST_SIZE     (80 * SIZE_SZ / 4)

#define NFASTBINS  (fastbin_index(request2size(MAX_FAST_SIZE))+1)

/*
 FASTBIN_CONSOLIDATION_THRESHOLD is the size of a chunk in free()
 that triggers automatic consolidation of possibly-surrounding
 fastbin chunks. This is a heuristic, so the exact value should not
 matter too much. It is defined at half the default trim threshold as a
 compromise heuristic to only attempt consolidation if it is likely
 to lead to trimming. However, it is not dynamically tunable, since
 consolidation reduces fragmentation surrounding large chunks even
 if trimming is not used.
 */

#define FASTBIN_CONSOLIDATION_THRESHOLD  (65536UL)

/*
 Since the lowest 2 bits in max_fast don't matter in size comparisons,
 they are used as flags.
 */

/*
 FASTCHUNKS_BIT held in max_fast indicates that there are probably
 some fastbin chunks. It is set true on entering a chunk into any
 fastbin, and cleared only in malloc_consolidate.

 The truth value is inverted so that have_fastchunks will be true
 upon startup (since statics are zero-filled), simplifying
 initialization checks.
 */

#define FASTCHUNKS_BIT        (1U)

#define have_fastchunks(M)     (((M)->flags &  FASTCHUNKS_BIT) == 0)
#define clear_fastchunks(M)    ((M)->flags |=  FASTCHUNKS_BIT)
#define set_fastchunks(M)      ((M)->flags &= ~FASTCHUNKS_BIT)

/*
 NONCONTIGUOUS_BIT indicates that MORECORE does not return contiguous
 regions.  Otherwise, contiguity is exploited in merging together,
 when possible, results from consecutive MORECORE calls.

 The initial value comes from MORECORE_CONTIGUOUS, but is
 changed dynamically if mmap is ever used as an sbrk substitute.
 */

#define NONCONTIGUOUS_BIT     (2U)

#define contiguous(M)          (((M)->flags &  NONCONTIGUOUS_BIT) == 0)
#define noncontiguous(M)       (((M)->flags &  NONCONTIGUOUS_BIT) != 0)
#define set_noncontiguous(M)   ((M)->flags |=  NONCONTIGUOUS_BIT)
#define set_contiguous(M)      ((M)->flags &= ~NONCONTIGUOUS_BIT)

/*
 Set value of max_fast.
 Use impossibly small value if 0.
 Precondition: there are no existing fastbin chunks.
 Setting the value clears fastchunk bit but preserves noncontiguous bit.
 */

#define set_max_fast(s) \
  global_max_fast = ((s) == 0)? SMALLBIN_WIDTH: request2size(s)
#define get_max_fast() global_max_fast

/*
 ----------- Internal state representation and initialization -----------
 */

struct malloc_par
{
  /* Tunable parameters */
  unsigned long trim_threshold;
  size_t top_pad;
  size_t mmap_threshold;
#ifdef PER_THREAD
  size_t arena_test;
  size_t arena_max;
#endif

  /* Memory map support */
  int n_mmaps;
  int n_mmaps_max;
  int max_n_mmaps;
  /* the mmap_threshold is dynamic, until the user sets
   it manually, at which point we need to disable any
   dynamic behavior. */
  int no_dyn_threshold;

  /* Statistics */
  size_t mmapped_mem;
  /*INTERNAL_SIZE_T  sbrked_mem;*/
  /*INTERNAL_SIZE_T  max_sbrked_mem;*/
  size_t max_mmapped_mem;
  size_t max_total_mem; /* only kept for NO_THREADS */

  /* First address handed out by MORECORE/sbrk.  */
  char* sbrk_base;
};

/* There are several instances of this struct ("arenas") in this
 malloc.  If you are adapting this malloc in a way that does NOT use
 a static or mmapped malloc_state, you MUST explicitly zero-fill it
 before using. This malloc relies on the property that malloc_state
 is initialized to all zeroes (as is true of C statics).  */

static Allocator* main_arena;

/* There is only one instance of the malloc parameters.  */

static struct malloc_par mp_;

#ifdef PER_THREAD
/*  Non public mallopt parameters.  */
#define M_ARENA_TEST -7
#define M_ARENA_MAX  -8
#endif

/* Maximum size of memory handled in fastbins.  */
static size_t global_max_fast;

/*
 Initialize state struct.
 */

Allocator::Allocator(MMU& m, bool is_main) :
  mmu(&m)
{
  int i;
  mbinptr bin;

  /* Establish circular links for normal bins */
  for (i = 1; i < NBINS; ++i)
    {
      bin = bin_at(this,i);
      bin->fd = bin->bk = bin;
    }

  set_contiguous(this);
  if (is_main) {
    main_arena = this;
    set_max_fast(DEFAULT_MXFAST);
  }
  this->flags |= FASTCHUNKS_BIT;

  this->top = initial_top(this);
}

/* ------------------ Testing support ----------------------------------*/

static int perturb_byte;

#define alloc_perturb(p, n) memset (p, (perturb_byte ^ 0xff) & 0xff, n)
#define free_perturb(p, n) memset (p, perturb_byte & 0xff, n)

/* ------------------- Support for multiple arenas -------------------- */
/* There is only one arena, main_arena. */

#define arena_get(ar_ptr, sz) do { \
  ar_ptr = main_arena; \
  ar_ptr->acquire_lock(); \
} while(0)
#define arena_for_chunk(ptr) (main_arena)

/*
 Debugging support

 These routines make a number of assertions about the states
 of data structures that should be true at all times. If any
 are not true, it's very likely that a user program has somehow
 trashed memory. (It's also possible that there is a coding error
 in malloc. In which case, please report it!)
 */

/*
 Properties of all chunks
 */

void
Allocator::check_chunk(mchunkptr p)
{
  unsigned long sz = chunksize(p);
  /* min and max possible addresses assuming contiguous allocation */
  char* max_address = (char*) (this->top) + chunksize(this->top);
  char* min_address = max_address - this->system_mem;

  if (!chunk_is_mmapped(p))
    {

      /* Has legal address ... */
      if (p != this->top)
        {
          if (contiguous(this))
            {
              assert(((char*)p) >= min_address);
              assert(((char*)p + sz) <= ((char*)(this->top)));
            }
        }
      else
        {
          /* top size is always at least MINSIZE */
          assert((unsigned long)(sz) >= MINSIZE);
          /* top predecessor always marked inuse */
          assert(prev_inuse(p));
        }

    }
  else
    {
#if HAVE_MMAP
      /* address is outside main heap  */
      if (contiguous(this) && this->top != initial_top(this))
        {
          assert(((char*)p) < min_address || ((char*)p) >= max_address);
        }
      /* chunk is page-aligned */
      assert(((p->prev_size + sz) & (page_size-1)) == 0);
      /* mem is aligned */
      assert(aligned_OK(chunk2mem(p)));
#else
      /* force an appropriate assert violation if debug set */
      assert(!chunk_is_mmapped(p));
#endif
    }
}

/*
 Properties of free chunks
 */

void
Allocator::check_free_chunk(mchunkptr p)
{
  size_t sz = p->size & ~(PREV_INUSE | NON_MAIN_ARENA);
  mchunkptr next = chunk_at_offset(p, sz);

  this->check_chunk(p);

  /* Chunk must claim to be free ... */
  assert(!inuse(p));
  assert (!chunk_is_mmapped(p));

  /* Unless a special marker, must have OK fields */
  if ((unsigned long) (sz) >= MINSIZE)
    {
      assert((sz & MALLOC_ALIGN_MASK) == 0);
      assert(aligned_OK(chunk2mem(p)));
      /* ... matching footer field */
      assert(next->prev_size == sz);
      /* ... and is fully consolidated */
      assert(prev_inuse(p));
      assert (next == this->top || inuse(next));

      /* ... and has minimally sane links */
      assert(p->fd->bk == p);
      assert(p->bk->fd == p);
    }
  else
    /* markers are always of size SIZE_SZ */
    assert(sz == SIZE_SZ);
}

/*
 Properties of inuse chunks
 */

void
Allocator::check_inuse_chunk(mchunkptr p)
{
  mchunkptr next;

  this->check_chunk(p);

  if (chunk_is_mmapped(p))
    return; /* mmapped chunks have no next/prev */

  /* Check whether it claims to be in use ... */
  assert(inuse(p));

  next = next_chunk(p);

  /* ... and is surrounded by OK chunks.
   Since more things can be checked with free chunks than inuse ones,
   if an inuse chunk borders them and debug is on, it's worth doing them.
   */
  if (!prev_inuse(p))
    {
      /* Note that we cannot even look at prev unless it is not inuse */
      mchunkptr prv = prev_chunk(p);
      assert(next_chunk(prv) == p);
      this->check_free_chunk(prv);
    }

  if (next == this->top)
    {
      assert(prev_inuse(next));
      assert(chunksize(next) >= MINSIZE);
    }
  else if (!inuse(next))
    this->check_free_chunk(next);
}

/*
 Properties of chunks recycled from fastbins
 */

void
Allocator::check_remalloced_chunk(mchunkptr p, size_t s)
{
  size_t sz = p->size & ~(PREV_INUSE | NON_MAIN_ARENA);

  if (!chunk_is_mmapped(p))
    {
      assert(this == arena_for_chunk(p));
      if (chunk_non_main_arena(p))
        assert(this != main_arena);
      else
        assert(this == main_arena);
    }

  this->check_inuse_chunk(p);

  /* Legal size ... */
  assert((sz & MALLOC_ALIGN_MASK) == 0);
  assert((unsigned long)(sz) >= MINSIZE);
  /* ... and alignment */
  assert(aligned_OK(chunk2mem(p)));
  /* chunk is less than MINSIZE more than request */
  assert((long)(sz) - (long)(s) >= 0);
  assert((long)(sz) - (long)(s + MINSIZE) < 0);
}

/*
 Properties of nonrecycled chunks at the point they are malloced
 */

void
Allocator::check_malloced_chunk(mchunkptr p, size_t s)
{
  /* same as recycled case ... */
  this->check_remalloced_chunk(p, s);

  /*
   ... plus,  must obey implementation invariant that prev_inuse is
   always true of any allocated chunk; i.e., that each allocated
   chunk borders either a previously allocated and still in-use
   chunk, or the base of its memory arena. This is ensured
   by making all allocations from the the `lowest' part of any found
   chunk.  This does not necessarily hold however for chunks
   recycled via fastbins.
   */

  assert(prev_inuse(p));
}

/*
 Properties of malloc_state.

 This may be useful for debugging malloc, as well as detecting user
 programmer errors that somehow write into malloc_state.

 If you are extending or experimenting with this malloc, you can
 probably figure out how to hack this routine to print out or
 display chunk addresses, sizes, bins, and other instrumentation.
 */

void
Allocator::check_malloc_state()
{
  unsigned int i;
  mchunkptr p;
  mchunkptr q;
  mbinptr b;
  unsigned int idx;
  size_t size;
  unsigned long total = 0;
  unsigned int max_fast_bin;

  /* internal size_t must be no wider than pointer type */
  // assert(sizeof(size_t) <= sizeof(char*));

  /* alignment is a power of 2 */
  // assert((MALLOC_ALIGNMENT & (MALLOC_ALIGNMENT-1)) == 0);

  /* cannot run remaining checks until fully initialized */
  if (this->top == 0 || this->top == initial_top(this))
    return;

  /* A contiguous main_arena is consistent with sbrk_base.  */
#if 0
  /* Actually, should be. It turns out that it is not */
  if (this == main_arena && contiguous(this))
    assert((char*)mp_.sbrk_base + this->system_mem ==
        (char*)this->top + chunksize(this->top));
#endif

  /* properties of fastbins */

  /* max_fast is in allowed range */
  assert((get_max_fast () & ~1) <= request2size(MAX_FAST_SIZE));

  max_fast_bin = fastbin_index(get_max_fast ());

  for (i = 0; i < NFASTBINS; ++i)
    {
      p = this->fastbinsY[i];

      /* The following test can only be performed for the main arena.
       While mallopt calls malloc_consolidate to get rid of all fast
       bins (especially those larger than the new maximum) this does
       only happen for the main arena.  Trying to do this for any
       other arena would mean those arenas have to be locked and
       malloc_consolidate be called for them.  This is excessive.  And
       even if this is acceptable to somebody it still cannot solve
       the problem completely since if the arena is locked a
       concurrent malloc call might create a new arena which then
       could use the newly invalid fast bins.  */

      /* all bins past max_fast are empty */
      if (this == main_arena && i > max_fast_bin)
        assert(p == 0);

      while (p != 0)
        {
          /* each chunk claims to be inuse */
          this->check_inuse_chunk(p);
          total += chunksize(p);
          /* chunk belongs in this bin */
          assert(fastbin_index(chunksize(p)) == i);
          p = p->fd;
        }
    }

  if (total != 0)
    assert(have_fastchunks(this));
  // if I still understand basic logic, the following is not needed
  //else if (!have_fastchunks(this))
  //assert(total == 0);

  /* check normal bins */
  for (i = 1; i < NBINS; ++i)
    {
      b = bin_at(this,i);

      /* binmap is accurate (except for bin 1 == unsorted_chunks) */
      if (i >= 2)
        {
          unsigned int binbit = get_binmap(this,i);
          int empty = last(b) == b;
          if (!binbit)
            assert(empty);
          else if (!empty)
            assert(binbit);
        }

      for (p = last(b); p != b; p = p->bk)
        {
          /* each chunk claims to be free */
          this->check_free_chunk(p);
          size = chunksize(p);
          total += size;
          if (i >= 2)
            {
              /* chunk belongs in bin */
              idx = bin_index(size);
              assert(idx == i);
              /* lists are sorted */
              assert(p->bk == b ||
                  (unsigned long)chunksize(p->bk) >= (unsigned long)chunksize(p));

              if (!in_smallbin_range(size))
                {
                  if (p->fd_nextsize != NULL)
                    {
                      if (p->fd_nextsize == p)
                        assert (p->bk_nextsize == p);
                      else
                        {
                          if (p->fd_nextsize == first (b))
                            assert (chunksize (p) < chunksize (p->fd_nextsize));
                          else
                            assert (chunksize (p) > chunksize (p->fd_nextsize));

                          if (p == first (b))
                            assert (chunksize (p) > chunksize (p->bk_nextsize));
                          else
                            assert (chunksize (p) < chunksize (p->bk_nextsize));
                        }
                    }
                  else
                    assert (p->bk_nextsize == NULL);
                }
            }
          else if (!in_smallbin_range(size))
            assert (p->fd_nextsize == NULL && p->bk_nextsize == NULL);
          /* chunk is followed by a legal chain of inuse chunks */
          for (q = next_chunk(p); (q != this->top && inuse(q)
              && (unsigned long) (chunksize(q)) >= MINSIZE); q = next_chunk(q))
            this->check_inuse_chunk(q);
        }
    }

  /* top chunk is OK */
  this->check_chunk(this->top);

  /* sanity checks for statistics */

  assert(mp_.n_mmaps <= mp_.max_n_mmaps);

  assert((unsigned long)(this->system_mem) <=
      (unsigned long)(this->max_system_mem));

  assert((unsigned long)(mp_.mmapped_mem) <=
      (unsigned long)(mp_.max_mmapped_mem));

}

/* ----------- Routines dealing with system allocation -------------- */

/*
 sysmalloc handles malloc cases requiring more memory from the system.
 On entry, it is assumed that av->top does not have enough
 space to service request for nb bytes, thus requiring that av->top
 be extended or replaced.
 */

void*
Allocator::require_memory(size_t nb)
{
  mchunkptr old_top; /* incoming value of av->top */
  size_t old_size; /* its size */
  char* old_end; /* its end address */

  long size; /* arg to first MORECORE or mmap call */
  char* brk; /* return value from MORECORE */

  long correction; /* arg to 2nd MORECORE call */
  char* snd_brk; /* 2nd return val */

  size_t front_misalign; /* unusable bytes at front of new space */
  size_t end_misalign; /* partial page left at end of new space */
  char* aligned_brk; /* aligned offset into brk */

  mchunkptr p; /* the allocated/returned chunk */
  mchunkptr remainder; /* remainder from allocation */
  unsigned long remainder_size; /* its size */

  unsigned long sum; /* for updating stats */

  size_t pagemask = page_size - 1;
  bool tried_mmap = false;

  /*
   If have mmap, and the request size meets the mmap threshold, and
   the system supports mmap, and there are few enough currently
   allocated mmapped regions, try to directly map this request
   rather than expanding top.
   */

  if ((unsigned long) (nb) >= (unsigned long) (mp_.mmap_threshold)
      && (mp_.n_mmaps < mp_.n_mmaps_max))
    {
      //try_mmap:
      /*
       Round up size to nearest page.  For mmapped chunks, the overhead
       is one SIZE_SZ unit larger than for normal chunks, because there
       is no following chunk whose prev_size field could be used.
       */
      /* See the front_misalign handling below, for glibc there is no
       need for further alignments.  */
      size = (nb + SIZE_SZ + pagemask) & ~pagemask;
      tried_mmap = true;

      /* Don't try if size wraps around 0 */
      if ((unsigned long) (size) > (unsigned long) (nb))
        {
          MMU::AddressRange range(this->mmu->createMMap(NULL, size));
          if (range.good() && this->mmu->addMapping(NULL, range, MMU::read | MMU::write))
            {
              /*
               The offset to the start of the mmapped region is stored
               in the prev_size field of the chunk. This allows us to adjust
               returned start address to meet alignment requirements here
               and in memalign(), and still be able to compute proper
               address argument for later munmap in free() and realloc().
               */

              /* For glibc, chunk2mem increases the address by 2*SIZE_SZ and
               MALLOC_ALIGN_MASK is 2*SIZE_SZ-1.  Each mmap'ed area is page
               aligned and therefore definitely MALLOC_ALIGN_MASK-aligned.  */
              assert (((size_t)chunk2mem(range.begin) & MALLOC_ALIGN_MASK) == 0);
                {
                  p = (mchunkptr) range.end;
                  set_head(p, size|IS_MMAPPED);
                }

              /* update statistics */

              if (++mp_.n_mmaps > mp_.max_n_mmaps)
                mp_.max_n_mmaps = mp_.n_mmaps;

              sum = mp_.mmapped_mem += size;
              if (sum > (unsigned long) (mp_.max_mmapped_mem))
                mp_.max_mmapped_mem = sum;
              this->check_chunk(p);

              return chunk2mem(p);
            }
        }
    }

  /* Record incoming configuration of top */

  old_top = this->top;
  old_size = chunksize(old_top);
  old_end = (char*) (chunk_at_offset(old_top, old_size));

  brk = snd_brk = (char*) (-1);

  /*
   If not the first time through, we require old_size to be
   at least MINSIZE and to have prev_inuse set.
   */

  assert((old_top == initial_top(this) && old_size == 0) ||
      ((unsigned long) (old_size) >= MINSIZE &&
          prev_inuse(old_top) &&
          ((unsigned long)old_end & pagemask) == 0));

  /* Precondition: not enough current space to satisfy nb request */
  assert((unsigned long)(old_size) < (unsigned long)(nb + MINSIZE));

#ifndef ATOMIC_FASTBINS
  /* Precondition: all fastbins are consolidated */
  assert(!have_fastchunks(this));
#endif
  if (this != main_arena)
    {
#if USE_ARENAS
      heap_info *old_heap, *heap;
      size_t old_heap_size;

      /* First try to extend the current heap. */
      old_heap = heap_for_ptr(old_top);
      old_heap_size = old_heap->size;
      if ((long) (MINSIZE + nb - old_size) > 0
          && grow_heap(old_heap, MINSIZE + nb - old_size) == 0)
        {
          this->system_mem += old_heap->size - old_heap_size;
          arena_mem += old_heap->size - old_heap_size;
          set_head(old_top, (((char *)old_heap + old_heap->size) - (char *)old_top)
              | PREV_INUSE);
        }
      else if ((heap = new_heap(nb + (MINSIZE + sizeof(*heap)), mp_.top_pad)))
        {
          /* Use a newly allocated heap.  */
          heap->ar_ptr = av;
          heap->prev = old_heap;
          av->system_mem += heap->size;
          arena_mem += heap->size;
          /* Set up the new top.  */
          top(av) = chunk_at_offset(heap, sizeof(*heap));
          set_head(top(av), (heap->size - sizeof(*heap)) | PREV_INUSE);

          /* Setup fencepost and free the old top chunk. */
          /* The fencepost takes at least MINSIZE bytes, because it might
           become the top chunk again later.  Note that a footer is set
           up, too, although the chunk is marked in use. */
          old_size -= MINSIZE;
          set_head(chunk_at_offset(old_top, old_size + 2*SIZE_SZ), 0|PREV_INUSE);
          if (old_size >= MINSIZE)
            {
              set_head(chunk_at_offset(old_top, old_size), (2*SIZE_SZ)|PREV_INUSE);
              set_foot(chunk_at_offset(old_top, old_size), (2*SIZE_SZ));
              set_head(old_top, old_size|PREV_INUSE|NON_MAIN_ARENA);
#ifdef ATOMIC_FASTBINS
              _int_free(av, old_top, 1);
#else
              _int_free(av, old_top);
#endif
            }
          else
            {
              set_head(old_top, (old_size + 2*SIZE_SZ)|PREV_INUSE);
              set_foot(old_top, (old_size + 2*SIZE_SZ));
            }
        }
      else if (!tried_mmap)
      /* We can at least try to use to mmap memory.  */
      goto try_mmap;

    }
#else
  this->report_error("not implemented", NULL);
#endif
    }
  else
    { /* av == main_arena */

      /* Request enough space for nb + pad + overhead */

      size = nb + mp_.top_pad + MINSIZE;

      /*
       If contiguous, we can subtract out existing space that we hope to
       combine with new space. We add it back later only if
       we don't actually get contiguous space.
       */

      if (contiguous(this))
        size -= old_size;

      /*
       Round to a multiple of page size.
       If MORECORE is not contiguous, this ensures that we only call it
       with whole-page arguments.  And if MORECORE is contiguous and
       this is not first time through, this preserves page-alignment of
       previous calls. Otherwise, we correct to page-align below.
       */

      size = (size + pagemask) & ~pagemask;

      /*
       Don't try to call MORECORE if argument is so big as to appear
       negative. Note that since mmap takes size_t arg, it may succeed
       below even if we cannot call MORECORE.
       */
      if (size > 0)
        brk = (char*) (this->mmu->extend(NULL,size));

      if (brk == (char*) (-1))
        {
          /*
           If have mmap, try using it as a backup when MORECORE fails or
           cannot be used. This is worth doing on systems that have "holes" in
           address space, so sbrk cannot extend to give contiguous space, but
           space is available elsewhere.  Note that we ignore mmap max count
           and threshold limits, since the space will not be used as a
           segregated mmap region.
           */

          /* Cannot merge with old top, so add its size back in */
          if (contiguous(this))
            size = (size + old_size + pagemask) & ~pagemask;

          /* If we are relying on mmap as backup, then use larger units */
          if ((unsigned long) (size) < (unsigned long) (MMAP_AS_MORECORE_SIZE))
            size = MMAP_AS_MORECORE_SIZE;

          /* Don't try if size wraps around 0 */
          if ((unsigned long) (size) > (unsigned long) (nb))
            {
              MMU::AddressRange range(this->mmu->createMMap(NULL, size));
              if (range.good() && this->mmu->addMapping(NULL, range, MMU::read | MMU::write))
                {

                  /* We do not need, and cannot use, another sbrk call to find end */
                  brk = (char*) range.begin;
                  snd_brk = brk + size;

                  /*
                   Record that we no longer have a contiguous sbrk region.
                   After the first time mmap is used as backup, we do not
                   ever rely on contiguous space since this could incorrectly
                   bridge regions.
                   */
                  set_noncontiguous(this);
                }
            }
        }

      if (brk != (char*) (-1))
        {
          if (mp_.sbrk_base == 0)
            mp_.sbrk_base = brk;
          this->system_mem += size;

          /*
           If MORECORE extends previous space, we can likewise extend top size.
           */

          if (brk == old_end && snd_brk == (char*) (-1))
            set_head(old_top, (size + old_size) | PREV_INUSE);

          else if (contiguous(this) && old_size && brk < old_end)
            {
              /* Oops!  Someone else killed our space..  Can't touch anything.  */
              this->report_error("break adjusted to free malloc space", brk);
            }

          /*
           Otherwise, make adjustments:

           * If the first time through or noncontiguous, we need to call sbrk
           just to find out where the end of memory lies.

           * We need to ensure that all returned chunks from malloc will meet
           MALLOC_ALIGNMENT

           * If there was an intervening foreign sbrk, we need to adjust sbrk
           request size to account for fact that we will not be able to
           combine new space with existing space in old_top.

           * Almost all systems internally allocate whole pages at a time, in
           which case we might as well use the whole last page of request.
           So we allocate enough more memory to hit a page boundary now,
           which in turn causes future contiguous calls to page-align.
           */

          else
            {
              front_misalign = 0;
              end_misalign = 0;
              correction = 0;
              aligned_brk = brk;

              /* handle contiguous cases */
              if (contiguous(this))
                {

                  /* Count foreign sbrk as system_mem.  */
                  if (old_size)
                    this->system_mem += brk - old_end;

                  /* Guarantee alignment of first new chunk made from this space */

                  front_misalign = (size_t) chunk2mem(brk) & MALLOC_ALIGN_MASK;
                  if (front_misalign > 0)
                    {

                      /*
                       Skip over some bytes to arrive at an aligned position.
                       We don't need to specially mark these wasted front bytes.
                       They will never be accessed anyway because
                       prev_inuse of av->top (and any chunk created from its start)
                       is always true after initialization.
                       */

                      correction = MALLOC_ALIGNMENT - front_misalign;
                      aligned_brk += correction;
                    }

                  /*
                   If this isn't adjacent to existing space, then we will not
                   be able to merge with old_top space, so must add to 2nd request.
                   */

                  correction += old_size;

                  /* Extend the end address to hit a page boundary */
                  end_misalign = (size_t) (brk + size + correction);
                  correction += ((end_misalign + pagemask) & ~pagemask)
                      - end_misalign;

                  assert(correction >= 0);
                  snd_brk = (char*) (this->mmu->extend(NULL,correction));

                  /*
                   If can't allocate correction, try to at least find out current
                   brk.  It might be enough to proceed without failing.

                   Note that if second sbrk did NOT fail, we assume that space
                   is contiguous with first sbrk. This is a safe assumption unless
                   program is multithreaded but doesn't use locks and a foreign sbrk
                   occurred between our first and second calls.
                   */

                  if (snd_brk == (char*) (-1))
                    {
                      correction = 0;
                      snd_brk = (char*) (this->mmu->extend(NULL,0));
                    }
                }

              /* handle non-contiguous cases */
              else
                {
                  /* MORECORE/mmap must correctly align */
                  assert(((unsigned long)chunk2mem(brk) & MALLOC_ALIGN_MASK) == 0);

                  /* Find out current end of memory */
                  if (snd_brk == (char*) (-1))
                    {
                      snd_brk = (char*) (this->mmu->extend(NULL,0));
                    }
                }

              /* Adjust top based on results of second sbrk */
              if (snd_brk != (char*) (-1))
                {
                  this->top = (mchunkptr) aligned_brk;
                  set_head(this->top, (snd_brk - aligned_brk + correction) | PREV_INUSE);
                  this->system_mem += correction;

                  /*
                   If not the first time through, we either have a
                   gap due to foreign sbrk or a non-contiguous region.  Insert a
                   double fencepost at old_top to prevent consolidation with space
                   we don't own. These fenceposts are artificial chunks that are
                   marked as inuse and are in any case too small to use.  We need
                   two to make sizes and alignments work out.
                   */

                  if (old_size != 0)
                    {
                      /*
                       Shrink old_top to insert fenceposts, keeping size a
                       multiple of MALLOC_ALIGNMENT. We know there is at least
                       enough space in old_top to do this.
                       */
                      old_size = (old_size - 4 * SIZE_SZ) & ~MALLOC_ALIGN_MASK;
                      set_head(old_top, old_size | PREV_INUSE);

                      /*
                       Note that the following assignments completely overwrite
                       old_top when old_size was previously MINSIZE.  This is
                       intentional. We need the fencepost, even if old_top otherwise gets
                       lost.
                       */
                      chunk_at_offset(old_top, old_size )->size = (2 * SIZE_SZ)
                          | PREV_INUSE;

                      chunk_at_offset(old_top, old_size + 2*SIZE_SZ)->size = (2
                          * SIZE_SZ) | PREV_INUSE;

                      /* If possible, release the rest. */
                      if (old_size >= MINSIZE)
                        {
#ifdef ATOMIC_FASTBINS
                          this->_int_free(old_top, 1);
#else
                          this->int_free(old_top);
#endif
                        }

                    }
                }
            }

        }
    } /* if (av !=  &main_arena) */
  if ((unsigned long) this->system_mem > (unsigned long) (this->max_system_mem))
    this->max_system_mem = this->system_mem;
  this->check_malloc_state();

  /* finally, do the allocation */
  p = this->top;
  size = chunksize(p);

  /* check that one of the above allocation paths succeeded */
  if ((unsigned long) (size) >= (unsigned long) (nb + MINSIZE))
    {
      remainder_size = size - nb;
      remainder = chunk_at_offset(p, nb);
      this->top = remainder;
      set_head(p, nb | PREV_INUSE | (this != main_arena ? NON_MAIN_ARENA : 0));
      set_head(remainder, remainder_size | PREV_INUSE);
      this->check_malloced_chunk(p, nb);
      return chunk2mem(p);
    }
  return 0;
}

/*
 sYSTRIm is an inverse of sorts to sYSMALLOc.  It gives memory back
 to the system (via negative arguments to sbrk) if there is unused
 memory at the `high' end of the malloc pool. It is called
 automatically by free() when top space exceeds the trim
 threshold. It is also called by the public malloc_trim routine.  It
 returns 1 if it actually released any memory, else 0.
 */

int
Allocator::release_memory(size_t pad)
{
  long top_size; /* Amount of top-most memory */
  long extra; /* Amount to release */
  long released; /* Amount actually released */
  char* current_brk; /* address returned by pre-check sbrk call */
  char* new_brk; /* address returned by post-check sbrk call */
  size_t pagesz(page_size);

  top_size = chunksize(this->top);

  /* Release in pagesize units, keeping at least one page */
  extra = ((top_size - pad - MINSIZE + (pagesz - 1)) / pagesz - 1) * pagesz;

  if (extra > 0)
    {

      /*
       Only proceed if end of memory is where we last set it.
       This avoids problems if there were foreign sbrk calls.
       */
      current_brk = (char*) (this->mmu->extend(NULL,0));
      if (current_brk == (char*) (this->top) + top_size)
        {

          /*
           Attempt to release memory. We ignore MORECORE return value,
           and instead call again to find out where new end of memory is.
           This avoids problems if first call releases less than we asked,
           of if failure somehow altered brk value. (We could still
           encounter problems if it altered brk in some very bad way,
           but the only thing we can do is adjust anyway, which will cause
           some downstream failure.)
           */

          this->mmu->extend(NULL,-extra);
          new_brk = (char*) (this->mmu->extend(NULL,0));

          if (new_brk != (char*) -1)
            {
              released = (long) (current_brk - new_brk);
              if (released != 0)
                {
                  /* Success. Adjust top. */
                  this->system_mem -= released;
                  set_head(this->top, (top_size - released) | PREV_INUSE);
                  this->check_malloc_state();
                  return 1;
                }
            }
        }
    }
  return 0;
}

#ifdef HAVE_MMAP

void
Allocator::munmap_chunk(mchunkptr p)
{
  size_t size = chunksize(p);

  assert (chunk_is_mmapped(p));
  uintptr_t block = (uintptr_t) p - p->prev_size;
  size_t total_size = p->prev_size + size;
  /* Unfortunately we have to do the compilers job by hand here.  Normally
   we would test BLOCK and TOTAL-SIZE separately for compliance with the
   page size.  But gcc does not recognize the optimization possibility
   (in the moment at least) so we combine the two values into one before
   the bit test.  */
  if (__builtin_expect(((block | total_size) & (page_size - 1)) != 0, 0))
    {
      Allocator::report_error("munmap_chunk(): invalid pointer", chunk2mem (p));
      return;
    }

  mp_.n_mmaps--;
  mp_.mmapped_mem -= total_size;

  MMU::AddressRange r;
  r.begin = (char*) block;
  r.end = (char*) block + total_size;
  bool ret = this->mmu->dropMapping(NULL, r);

  /* munmap returns non-zero on failure */
  assert(ret);
}

#if HAVE_MREMAP

static Allocator::mchunkptr
mremap_chunk(Allocator::mchunkptr p, size_t new_size)
  {
    size_t page_mask = mp_.pagesize - 1;
    size_t offset = p->prev_size;
    size_t size = chunksize(p);
    char *cp;

    assert (chunk_is_mmapped(p));
    assert(((size + offset) & (mp_.pagesize-1)) == 0);

    /* Note the extra SIZE_SZ overhead as in mmap_chunk(). */
    new_size = (new_size + offset + SIZE_SZ + page_mask) & ~page_mask;

    /* No need to remap if the number of pages does not change.  */
    if (size + offset == new_size)
    return p;

    cp = (char *)mremap((char *)p - offset, size + offset, new_size,
        MREMAP_MAYMOVE);

    if (cp == MAP_FAILED) return 0;

    p = (mchunkptr)(cp + offset);

    assert(aligned_OK(chunk2mem(p)));

    assert((p->prev_size == offset));
    set_head(p, (new_size - offset)|IS_MMAPPED);

    mp_.mmapped_mem -= size + offset;
    mp_.mmapped_mem += new_size;
    if ((unsigned long)mp_.mmapped_mem > (unsigned long)mp_.max_mmapped_mem)
    mp_.max_mmapped_mem = mp_.mmapped_mem;
    return p;
  }

#endif /* HAVE_MREMAP */

#endif /* HAVE_MMAP */

/*------------------------ Public wrappers. --------------------------------*/

namespace KMemory
{

  void*
  malloc(size_t bytes)
  {
    Allocator* ar_ptr;
    void *victim;

#if USE_ARENAS
    arena_lookup(ar_ptr);
    arena_lock(ar_ptr, bytes);
#else
    ar_ptr = main_arena;
    if (!ar_ptr)
      return 0;
    ar_ptr->acquire_lock();
#endif
    victim = ar_ptr->int_malloc(bytes);
    if (!victim)
      {
        /* Maybe the failure is due to running out of mmapped areas. */
        if (ar_ptr != main_arena)
          {
            ar_ptr->release_lock();
            ar_ptr = main_arena;
            ar_ptr->acquire_lock();
            victim = ar_ptr->int_malloc(bytes);
            ar_ptr->release_lock();
          }
        else
          {
#if USE_ARENAS
            /* ... or sbrk() has failed and there is still a chance to mmap() */
            ar_ptr = arena_get2(ar_ptr->next ? ar_ptr : 0, bytes);
            (void) mutex_unlock(&main_arena.mutex);
            if (ar_ptr)
              {
                victim = _int_malloc(ar_ptr, bytes);
                (void) mutex_unlock(&ar_ptr->mutex);
              }
#endif
          }
      }
    else
      ar_ptr->release_lock();
    assert(!victim || chunk_is_mmapped(mem2chunk(victim)) ||
        ar_ptr == arena_for_chunk(mem2chunk(victim)));
    return victim;
  }

  void
  free(void* mem)
  {
    Allocator* ar_ptr;
    Allocator::mchunkptr p; /* chunk corresponding to mem */

    if (mem == 0) /* free(0) has no effect */
      return;

    p = mem2chunk(mem);

    ar_ptr = arena_for_chunk(p);
#if HAVE_MMAP
    if (chunk_is_mmapped(p)) /* release mmapped memory. */
      {
        /* see if the dynamic brk/mmap threshold needs adjusting */
        if (!mp_.no_dyn_threshold && p->size > mp_.mmap_threshold && p->size
            <= DEFAULT_MMAP_THRESHOLD_MAX)
          {
            mp_.mmap_threshold = chunksize (p);
            mp_.trim_threshold = 2 * mp_.mmap_threshold;
          }
        ar_ptr->munmap_chunk(p);
        return;
      }
#endif

#ifdef ATOMIC_FASTBINS
    _int_free(ar_ptr, p, 0);
#else
    ar_ptr->acquire_lock();
    ar_ptr->int_free(p);
    ar_ptr->release_lock();
#endif
  }

  void*
  realloc(void* oldmem, size_t bytes)
  {
    Allocator* ar_ptr;
    size_t nb; /* padded request size */

    void* newp; /* chunk to return */

    /* realloc of null is supposed to be same as malloc */
    if (oldmem == 0)
      return KMemory::malloc(bytes);

    /* chunk corresponding to oldmem */
    const Allocator::mchunkptr oldp = mem2chunk(oldmem);
    /* its size */
    const size_t oldsize = chunksize(oldp);

    /* Little security check which won't hurt performance: the
     allocator never wrapps around at the end of the address space.
     Therefore we can exclude some size values which might appear
     here by accident or by "design" from some intruder.  */
    if (__builtin_expect((uintptr_t) oldp > (uintptr_t) -oldsize, 0)
        || __builtin_expect(misaligned_chunk (oldp), 0))
      {
        Allocator::report_error("realloc(): invalid pointer", oldmem);
        return NULL;
      }

    checked_request2size(bytes, nb);

#if HAVE_MMAP
    if (chunk_is_mmapped(oldp))
      {
        void* newmem;

#if HAVE_MREMAP
        newp = mremap_chunk(oldp, nb);
        if (newp)
        return chunk2mem(newp);
#endif
        /* Note the extra SIZE_SZ overhead. */
        if (oldsize - SIZE_SZ >= nb)
          return oldmem; /* do nothing */
        /* Must alloc, copy, free. */
        newmem = KernelMemory::malloc(bytes);
        if (newmem == 0)
          return 0; /* propagate failure */
        memcpy(newmem, oldmem, oldsize - 2 * SIZE_SZ);
        arena_for_chunk(oldp)->munmap_chunk(oldp);
        return newmem;
      }
#endif

    ar_ptr = arena_for_chunk(oldp);
    ar_ptr->acquire_lock();

    newp = ar_ptr->int_realloc(oldp, oldsize, nb);

    ar_ptr->release_lock();
    assert(!newp || chunk_is_mmapped(mem2chunk(newp)) ||
        ar_ptr == arena_for_chunk(mem2chunk(newp)));

    if (newp == NULL)
      {
        /* Try harder to allocate memory in other arenas.  */
        newp = KMemory::malloc(bytes);
        if (newp != NULL)
          {
            memcpy(newp, oldmem, oldsize - SIZE_SZ);
#ifdef ATOMIC_FASTBINS
            _int_free(ar_ptr, oldp, 0);
#else
            ar_ptr->acquire_lock();
            ar_ptr->int_free(oldp);
            ar_ptr->release_lock();
#endif
          }
      }

    return newp;
  }

  void*
  malloc(size_t bytes, size_t alignment)
  {
    Allocator* ar_ptr;
    void * p;

    /* If need less alignment than we give anyway, just relay to malloc */
    if (alignment <= MALLOC_ALIGNMENT)
      return KMemory::malloc(bytes);

    /* Otherwise, ensure that it is at least a minimum chunk size */
    if (alignment < MINSIZE)
      alignment = MINSIZE;

    arena_get(ar_ptr, bytes + alignment + MINSIZE);
    if (!ar_ptr)
      return 0;
    p = ar_ptr->int_malloc(bytes, alignment);
    if (!p)
      {
        /* Maybe the failure is due to running out of mmapped areas. */
        if (ar_ptr != main_arena)
          {
            ar_ptr->release_lock();
            ar_ptr = main_arena;
            ar_ptr->acquire_lock();
            p = ar_ptr->int_malloc(bytes, alignment);
            ar_ptr->release_lock();
          }
        else
          {
#if USE_ARENAS
            /* ... or sbrk() has failed and there is still a chance to mmap() */
            mstate prev = ar_ptr->next ? ar_ptr : 0;
            (void)mutex_unlock(&ar_ptr->mutex);
            ar_ptr = arena_get2(prev, bytes);
            if(ar_ptr)
              {
                p = _int_memalign(ar_ptr, alignment, bytes);
                (void)mutex_unlock(&ar_ptr->mutex);
              }
#endif
          }
      }
    else
      ar_ptr->release_lock();
    assert(!p || chunk_is_mmapped(mem2chunk(p)) ||
        ar_ptr == arena_for_chunk(mem2chunk(p)));
    return p;
  }

  void*
  pvalloc(size_t bytes)
  {
    Allocator* ar_ptr;
    void * p;

    size_t pagesz = page_size;
    size_t page_mask = page_size - 1;
    size_t rounded_bytes = (bytes + page_mask) & ~(page_mask);

    arena_get(ar_ptr, bytes + 2*pagesz + MINSIZE);
    p = ar_ptr->int_pvalloc(bytes);
    ar_ptr->release_lock();
    if (!p)
      {
        /* Maybe the failure is due to running out of mmapped areas. */
        if (ar_ptr != main_arena)
          {
            main_arena->acquire_lock();
            p = main_arena->int_malloc(rounded_bytes, pagesz);
            main_arena->release_lock();
          }
        else
          {
#if USE_ARENAS
            /* ... or sbrk() has failed and there is still a chance to mmap() */
            ar_ptr = arena_get2(ar_ptr->next ? ar_ptr : 0,
                bytes + 2*pagesz + MINSIZE);
            if(ar_ptr)
              {
                p = _int_memalign(ar_ptr, pagesz, rounded_bytes);
                (void)mutex_unlock(&ar_ptr->mutex);
              }
#endif
          }
      }
    assert(!p || chunk_is_mmapped(mem2chunk(p)) ||
        ar_ptr == arena_for_chunk(mem2chunk(p)));

    return p;
  }

  void*
  calloc(size_t n, size_t elem_size)
  {
    Allocator* av;
    Allocator::mchunkptr p;
    size_t bytes, sz, csz;
    void* mem;
    unsigned long clearsize;
    unsigned long nclears;
    size_t* d;

    /* size_t is unsigned so the behavior on overflow is defined.  */
    bytes = n * elem_size;
#define HALF_INTERNAL_SIZE_T \
  (((size_t) 1) << (8 * sizeof (size_t) / 2))
    if (__builtin_expect((n | elem_size) >= HALF_INTERNAL_SIZE_T, 0))
      {
        if (elem_size != 0 && bytes / elem_size != n)
          {
            return 0;
          }
      }

    sz = bytes;

    arena_get(av, sz);
    if (!av)
      return 0;

    /* Check if we hand out the top chunk, in which case there may be no
     need to clear. */
#if MORECORE_CLEARS
    oldtop = top(av);
    oldtopsize = chunksize(top(av));
#if MORECORE_CLEARS < 2
    /* Only newly allocated memory is guaranteed to be cleared.  */
    if (av == &main_arena &&
        oldtopsize < mp_.sbrk_base + av->max_system_mem - (char *)oldtop)
    oldtopsize = (mp_.sbrk_base + av->max_system_mem - (char *)oldtop);
#endif
#if USE_ARENAS
    if (this != main_arena)
      {
        heap_info *heap = heap_for_ptr (oldtop);
        if (oldtopsize < (char *) heap + heap->mprotect_size - (char *) oldtop)
        oldtopsize = (char *) heap + heap->mprotect_size - (char *) oldtop;
      }
#endif
#endif
    mem = av->int_malloc(sz);

    /* Only clearing follows, so we can unlock early. */
    av->release_lock();

    assert(!mem || chunk_is_mmapped(mem2chunk(mem)) ||
        av == arena_for_chunk(mem2chunk(mem)));

    if (mem == 0)
      {
        /* Maybe the failure is due to running out of mmapped areas. */
        if (av != main_arena)
          {
            main_arena->acquire_lock();
            mem = main_arena->int_malloc(sz);
            main_arena->release_lock();
          }
        else
          {
#if USE_ARENAS
            /* ... or sbrk() has failed and there is still a chance to mmap() */
            (void)mutex_lock(&main_arena.mutex);
            av = arena_get2(av->next ? av : 0, sz);
            (void)mutex_unlock(&main_arena.mutex);
            if(av)
              {
                mem = _int_malloc(av, sz);
                (void)mutex_unlock(&av->mutex);
              }
#endif
          }
        if (mem == 0)
          return 0;
      }
    p = mem2chunk(mem);

    /* Two optional cases in which clearing not necessary */
#if HAVE_MMAP
    if (chunk_is_mmapped (p))
      {
        if (__builtin_expect(perturb_byte, 0))
          memset(mem, 0, sz);
        return mem;
      }
#endif

    csz = chunksize(p);

#if MORECORE_CLEARS
    if (perturb_byte == 0 && (p == oldtop && csz > oldtopsize))
      {
        /* clear only the bytes from non-freshly-sbrked memory */
        csz = oldtopsize;
      }
#endif

    /* Unroll clear of <= 36 bytes (72 if 8byte sizes).  We know that
     contents have an odd number of INTERNAL_SIZE_T-sized words;
     minimally 3.  */
    d = (size_t*) mem;
    clearsize = csz - SIZE_SZ;
    nclears = clearsize / sizeof(size_t);
    assert(nclears >= 3);

    if (nclears > 9)
      memset(d, 0, clearsize);

    else
      {
        *(d + 0) = 0;
        *(d + 1) = 0;
        *(d + 2) = 0;
        if (nclears > 4)
          {
            *(d + 3) = 0;
            *(d + 4) = 0;
            if (nclears > 6)
              {
                *(d + 5) = 0;
                *(d + 6) = 0;
                if (nclears > 8)
                  {
                    *(d + 7) = 0;
                    *(d + 8) = 0;
                  }
              }
          }
      }

    return mem;
  }

  void**
  independent_calloc(size_t n, size_t elem_size, void** chunks)
  {
    Allocator* ar_ptr;
    void** m;

    arena_get(ar_ptr, n * elem_size);
    if (!ar_ptr)
      return 0;

    m = ar_ptr->int_icalloc(n, elem_size, chunks);
    ar_ptr->release_lock();
    return m;
  }

  void**
  independent_comalloc(size_t n, size_t sizes[], void** chunks)
  {
    Allocator* ar_ptr;
    void** m;

    arena_get(ar_ptr, 0);
    if (!ar_ptr)
      return 0;

    m = ar_ptr->int_icomalloc(n, sizes, chunks);
    ar_ptr->release_lock();
    return m;
  }

  bool
  trim(size_t s)
  {
    bool result = 0;

    Allocator* ar_ptr = main_arena;
    do
      {
        ar_ptr->acquire_lock();
        result |= ar_ptr->trim(s);
        ar_ptr->release_lock();
#if USE_ARENAS
        ar_ptr = ar_ptr->next;
#endif
      }
    while (ar_ptr != main_arena);

    return result;
  }

}

/*
 ------------------------------ malloc ------------------------------
 */

void*
Allocator::int_malloc(size_t bytes)
{
  size_t nb; /* normalized request size */
  unsigned int idx; /* associated bin index */
  mbinptr bin; /* associated bin */

  mchunkptr victim; /* inspected/selected chunk */
  size_t size; /* its size */
  int victim_index; /* its bin index */

  mchunkptr remainder; /* remainder from a split */
  unsigned long remainder_size; /* its size */

  unsigned int block; /* bit map traverser */
  unsigned int bit; /* bit map traverser */
  unsigned int map; /* current word of binmap */

  mchunkptr fwd; /* misc temp for linking */
  mchunkptr bck; /* misc temp for linking */

  /*
   Convert request size to internal form by adding SIZE_SZ bytes
   overhead plus possibly more to obtain necessary alignment and/or
   to obtain a size of at least MINSIZE, the smallest allocatable
   size. Also, checked_request2size traps (returning 0) request sizes
   that are so large that they wrap around zero when padded and
   aligned.
   */

  checked_request2size(bytes, nb);

  /*
   If the size qualifies as a fastbin, first check corresponding bin.
   This code is safe to execute even if av is not yet initialized, so we
   can try it without checking, which saves some time on this fast path.
   */

  if ((unsigned long) (nb) <= (unsigned long) (get_max_fast ()))
    {
      idx = fastbin_index(nb);
      mfastbinptr* fb = &fastbin (this, idx);
#ifdef ATOMIC_FASTBINS
      mchunkptr pp = *fb;
      do
        {
          victim = pp;
          if (victim == NULL)
          break;
        }
      while ((pp = catomic_compare_and_exchange_val_acq (fb, victim->fd, victim))
          != victim);
#else
      victim = *fb;
#endif
      if (victim != 0)
        {
          if (__builtin_expect(fastbin_index (chunksize (victim)) != idx, 0))
            {
              Allocator::report_error("malloc(): memory corruption (fast)",
                  chunk2mem (victim));
              return NULL;
            }
#ifndef ATOMIC_FASTBINS
          *fb = victim->fd;
#endif
          this->check_remalloced_chunk(victim, nb);
          void *p = chunk2mem(victim);
          if (__builtin_expect(perturb_byte, 0))
            alloc_perturb (p, bytes);
          return p;
        }
    }

  /*
   If a small request, check regular bin.  Since these "smallbins"
   hold one size each, no searching within bins is necessary.
   (For a large request, we need to wait until unsorted chunks are
   processed to find best fit. But for small ones, fits are exact
   anyway, so we can check now, which is faster.)
   */

  if (in_smallbin_range(nb))
    {
      idx = smallbin_index(nb);
      bin = bin_at(this,idx);

      if ((victim = last(bin)) != bin)
        {
          if (victim == 0) /* initialization check */
            this->consolidate();
          else
            {
              bck = victim->bk;
              if (__builtin_expect(bck->fd != victim, 0))
                {
                  Allocator::report_error(
                      "malloc(): smallbin double linked list corrupted",
                      chunk2mem (victim));
                  return NULL;
                }
              set_inuse_bit_at_offset(victim, nb);
              bin->bk = bck;
              bck->fd = bin;

              if (this != main_arena)
                victim->size |= NON_MAIN_ARENA;
              this->check_malloced_chunk(victim, nb);
              void *p = chunk2mem(victim);
              if (__builtin_expect(perturb_byte, 0))
                alloc_perturb (p, bytes);
              return p;
            }
        }
    }

  /*
   If this is a large request, consolidate fastbins before continuing.
   While it might look excessive to kill all fastbins before
   even seeing if there is space available, this avoids
   fragmentation problems normally associated with fastbins.
   Also, in practice, programs tend to have runs of either small or
   large requests, but less often mixtures, so consolidation is not
   invoked all that often in most programs. And the programs that
   it is called frequently in otherwise tend to fragment.
   */

  else
    {
      idx = largebin_index(nb);
      if (have_fastchunks(this))
        this->consolidate();
    }

  /*
   Process recently freed or remaindered chunks, taking one only if
   it is exact fit, or, if this a small request, the chunk is remainder from
   the most recent non-exact fit.  Place other traversed chunks in
   bins.  Note that this step is the only place in any routine where
   chunks are placed in bins.

   The outer loop here is needed because we might not realize until
   near the end of malloc that we should have consolidated, so must
   do so and retry. This happens at most once, and only when we would
   otherwise need to expand memory to service a "small" request.
   */

  for (;;)
    {

      int iters = 0;
      while ((victim = unsorted_chunks(this)->bk) != unsorted_chunks(this))
        {
          bck = victim->bk;
          if (__builtin_expect(victim->size <= 2 * SIZE_SZ, 0)
              || __builtin_expect(victim->size > this->system_mem, 0))
            {
              Allocator::report_error("malloc(): memory corruption",
                  chunk2mem (victim));
              return NULL;
            }
          size = chunksize(victim);

          /*
           If a small request, try to use last remainder if it is the
           only chunk in unsorted bin.  This helps promote locality for
           runs of consecutive small requests. This is the only
           exception to best-fit, and applies only when there is
           no exact fit for a small chunk.
           */

          if (in_smallbin_range(nb) && bck == unsorted_chunks(this) && victim
              == this->last_remainder && (unsigned long) (size)
              > (unsigned long) (nb + MINSIZE))
            {

              /* split and reattach remainder */
              remainder_size = size - nb;
              remainder = chunk_at_offset(victim, nb);
              unsorted_chunks(this)->bk = unsorted_chunks(this)->fd = remainder;
              this->last_remainder = remainder;
              remainder->bk = remainder->fd = unsorted_chunks(this);
              if (!in_smallbin_range(remainder_size))
                {
                  remainder->fd_nextsize = NULL;
                  remainder->bk_nextsize = NULL;
                }

              set_head(victim, nb | PREV_INUSE |
                  (this != main_arena ? NON_MAIN_ARENA : 0));
              set_head(remainder, remainder_size | PREV_INUSE);
              set_foot(remainder, remainder_size);

              this->check_malloced_chunk(victim, nb);
              void *p = chunk2mem(victim);
              if (__builtin_expect(perturb_byte, 0))
                alloc_perturb (p, bytes);
              return p;
            }

          /* remove from unsorted list */
          unsorted_chunks(this)->bk = bck;
          bck->fd = unsorted_chunks(this);

          /* Take now instead of binning if exact fit */

          if (size == nb)
            {
              set_inuse_bit_at_offset(victim, size);
              if (this != main_arena)
                victim->size |= NON_MAIN_ARENA;
              this->check_malloced_chunk(victim, nb);
              void *p = chunk2mem(victim);
              if (__builtin_expect(perturb_byte, 0))
                alloc_perturb (p, bytes);
              return p;
            }

          /* place chunk in bin */

          if (in_smallbin_range(size))
            {
              victim_index = smallbin_index(size);
              bck = bin_at(this, victim_index);
              fwd = bck->fd;
            }
          else
            {
              victim_index = largebin_index(size);
              bck = bin_at(this, victim_index);
              fwd = bck->fd;

              /* maintain large bins in sorted order */
              if (fwd != bck)
                {
                  /* Or with inuse bit to speed comparisons */
                  size |= PREV_INUSE;
                  /* if smaller than smallest, bypass loop below */
                  assert((bck->bk->size & NON_MAIN_ARENA) == 0);
                  if ((unsigned long) (size) < (unsigned long) (bck->bk->size))
                    {
                      fwd = bck;
                      bck = bck->bk;

                      victim->fd_nextsize = fwd->fd;
                      victim->bk_nextsize = fwd->fd->bk_nextsize;
                      fwd->fd->bk_nextsize = victim->bk_nextsize->fd_nextsize
                          = victim;
                    }
                  else
                    {
                      assert((fwd->size & NON_MAIN_ARENA) == 0);
                      while ((unsigned long) size < fwd->size)
                        {
                          fwd = fwd->fd_nextsize;
                          assert((fwd->size & NON_MAIN_ARENA) == 0);
                        }

                      if ((unsigned long) size == (unsigned long) fwd->size)
                        /* Always insert in the second position.  */
                        fwd = fwd->fd;
                      else
                        {
                          victim->fd_nextsize = fwd;
                          victim->bk_nextsize = fwd->bk_nextsize;
                          fwd->bk_nextsize = victim;
                          victim->bk_nextsize->fd_nextsize = victim;
                        }
                      bck = fwd->bk;
                    }
                }
              else
                victim->fd_nextsize = victim->bk_nextsize = victim;
            }

          mark_bin(this, victim_index);
          victim->bk = bck;
          victim->fd = fwd;
          fwd->bk = victim;
          bck->fd = victim;

#define MAX_ITERS	10000
          if (++iters >= MAX_ITERS)
            break;
        }

      /*
       If a large request, scan through the chunks of current bin in
       sorted order to find smallest that fits.  Use the skip list for this.
       */

      if (!in_smallbin_range(nb))
        {
          bin = bin_at(this, idx);

          /* skip scan if empty or largest chunk is too small */
          if ((victim = first(bin)) != bin && (unsigned long) (victim->size)
              >= (unsigned long) (nb))
            {

              victim = victim->bk_nextsize;
              while (((unsigned long) (size = chunksize(victim))
                  < (unsigned long) (nb)))
                victim = victim->bk_nextsize;

              /* Avoid removing the first entry for a size so that the skip
               list does not have to be rerouted.  */
              if (victim != last(bin) && victim->size == victim->fd->size)
                victim = victim->fd;

              remainder_size = size - nb;
              unlink(victim, bck, fwd);

              /* Exhaust */
              if (remainder_size < MINSIZE)
                {
                  set_inuse_bit_at_offset(victim, size);
                  if (this != main_arena)
                    victim->size |= NON_MAIN_ARENA;
                }
              /* Split */
              else
                {
                  remainder = chunk_at_offset(victim, nb);
                  /* We cannot assume the unsorted list is empty and therefore
                   have to perform a complete insert here.  */
                  bck = unsorted_chunks(this);
                  fwd = bck->fd;
                  if (__builtin_expect(fwd->bk != bck, 0))
                    {
                      Allocator::report_error(
                          "malloc(): corrupted unsorted chunks",
                          chunk2mem(victim));
                      return NULL;
                    }
                  remainder->bk = bck;
                  remainder->fd = fwd;
                  bck->fd = remainder;
                  fwd->bk = remainder;
                  if (!in_smallbin_range(remainder_size))
                    {
                      remainder->fd_nextsize = NULL;
                      remainder->bk_nextsize = NULL;
                    }
                  set_head(victim, nb | PREV_INUSE |
                      (this != main_arena ? NON_MAIN_ARENA : 0));
                  set_head(remainder, remainder_size | PREV_INUSE);
                  set_foot(remainder, remainder_size);
                }
              this->check_malloced_chunk(victim, nb);
              void *p = chunk2mem(victim);
              if (__builtin_expect(perturb_byte, 0))
                alloc_perturb (p, bytes);
              return p;
            }
        }

      /*
       Search for a chunk by scanning bins, starting with next largest
       bin. This search is strictly by best-fit; i.e., the smallest
       (with ties going to approximately the least recently used) chunk
       that fits is selected.

       The bitmap avoids needing to check that most blocks are nonempty.
       The particular case of skipping all bins during warm-up phases
       when no chunks have been returned yet is faster than it might look.
       */

      ++idx;
      bin = bin_at(this,idx);
      block = idx2block(idx);
      map = this->binmap[block];
      bit = idx2bit(idx);

      for (;;)
        {

          /* Skip rest of block if there are no more set bits in this block.  */
          if (bit > map || bit == 0)
            {
              do
                {
                  if (++block >= BINMAPSIZE) /* out of bins */
                    goto use_top;
                }
              while ((map = this->binmap[block]) == 0);

              bin = bin_at(this, (block << BINMAPSHIFT));
              bit = 1;
            }

          /* Advance to bin with set bit. There must be one. */
          while ((bit & map) == 0)
            {
              bin = next_bin(bin);
              bit <<= 1;
              assert(bit != 0);
            }

          /* Inspect the bin. It is likely to be non-empty */
          victim = last(bin);

          /*  If a false alarm (empty bin), clear the bit. */
          if (victim == bin)
            {
              this->binmap[block] = map &= ~bit; /* Write through */
              bin = next_bin(bin);
              bit <<= 1;
            }

          else
            {
              size = chunksize(victim);

              /*  We know the first chunk in this bin is big enough to use. */
              assert((unsigned long)(size) >= (unsigned long)(nb));

              remainder_size = size - nb;

              /* unlink */
              unlink(victim, bck, fwd);

              /* Exhaust */
              if (remainder_size < MINSIZE)
                {
                  set_inuse_bit_at_offset(victim, size);
                  if (this != main_arena)
                    victim->size |= NON_MAIN_ARENA;
                }

              /* Split */
              else
                {
                  remainder = chunk_at_offset(victim, nb);

                  /* We cannot assume the unsorted list is empty and therefore
                   have to perform a complete insert here.  */
                  bck = unsorted_chunks(this);
                  fwd = bck->fd;
                  if (__builtin_expect(fwd->bk != bck, 0))
                    {
                      Allocator::report_error(
                          "malloc(): corrupted unsorted chunks 2",
                          chunk2mem(victim));
                      return NULL;
                    }
                  remainder->bk = bck;
                  remainder->fd = fwd;
                  bck->fd = remainder;
                  fwd->bk = remainder;

                  /* advertise as last remainder */
                  if (in_smallbin_range(nb))
                    this->last_remainder = remainder;
                  if (!in_smallbin_range(remainder_size))
                    {
                      remainder->fd_nextsize = NULL;
                      remainder->bk_nextsize = NULL;
                    }
                  set_head(victim, nb | PREV_INUSE |
                      (this != main_arena ? NON_MAIN_ARENA : 0));
                  set_head(remainder, remainder_size | PREV_INUSE);
                  set_foot(remainder, remainder_size);
                }
              this->check_malloced_chunk(victim, nb);
              void *p = chunk2mem(victim);
              if (__builtin_expect(perturb_byte, 0))
                alloc_perturb (p, bytes);
              return p;
            }
        }

      use_top:
      /*
       If large enough, split off the chunk bordering the end of memory
       (held in av->top). Note that this is in accord with the best-fit
       search rule.  In effect, av->top is treated as larger (and thus
       less well fitting) than any other available chunk since it can
       be extended to be as large as necessary (up to system
       limitations).

       We require that av->top always exists (i.e., has size >=
       MINSIZE) after initialization, so if it would otherwise be
       exhausted by current request, it is replenished. (The main
       reason for ensuring it exists is that we may need MINSIZE space
       to put in fenceposts in sysmalloc.)
       */

      victim = this->top;
      size = chunksize(victim);

      if ((unsigned long) (size) >= (unsigned long) (nb + MINSIZE))
        {
          remainder_size = size - nb;
          remainder = chunk_at_offset(victim, nb);
          this->top = remainder;
          set_head(victim, nb | PREV_INUSE |
              (this != main_arena ? NON_MAIN_ARENA : 0));
          set_head(remainder, remainder_size | PREV_INUSE);

          this->check_malloced_chunk(victim, nb);
          void *p = chunk2mem(victim);
          if (__builtin_expect(perturb_byte, 0))
            alloc_perturb (p, bytes);
          return p;
        }

#ifdef ATOMIC_FASTBINS
      /* When we are using atomic ops to free fast chunks we can get
       here for all block sizes.  */
      else if (have_fastchunks(av))
        {
          malloc_consolidate(av);
          /* restore original bin index */
          if (in_smallbin_range(nb))
          idx = smallbin_index(nb);
          else
          idx = largebin_index(nb);
        }
#else
      /*
       If there is space available in fastbins, consolidate and retry,
       to possibly avoid expanding memory. This can occur only if nb is
       in smallbin range so we didn't consolidate upon entry.
       */

      else if (have_fastchunks(this))
        {
          assert(in_smallbin_range(nb));
          this->consolidate();
          idx = smallbin_index(nb); /* restore original bin index */
        }
#endif

      /*
       Otherwise, relay to handle system-dependent cases
       */
      else
        {
          void *p = this->require_memory(nb);
          if (p != NULL && __builtin_expect(perturb_byte, 0))
            alloc_perturb (p, bytes);
          return p;
        }
    }
}

/*
 ------------------------------ free ------------------------------
 */

void
#ifdef ATOMIC_FASTBINS
_int_free(mstate av, mchunkptr p, int have_lock)
#else
Allocator::int_free(mchunkptr p)
#endif
{
  size_t size; /* its size */
  mfastbinptr* fb; /* associated fastbin */
  mchunkptr nextchunk; /* next contiguous chunk */
  size_t nextsize; /* its size */
  int nextinuse; /* true if nextchunk is used */
  size_t prevsize; /* size of previous contiguous chunk */
  mchunkptr bck; /* misc temp for linking */
  mchunkptr fwd; /* misc temp for linking */

#ifdef ATOMIC_FASTBINS
  int locked = 0;
#endif

  size = chunksize(p);

  /* Little security check which won't hurt performance: the
   allocator never wrapps around at the end of the address space.
   Therefore we can exclude some size values which might appear
   here by accident or by "design" from some intruder.  */
  if (__builtin_expect((uintptr_t) p > (uintptr_t) -size, 0)
      || __builtin_expect(misaligned_chunk (p), 0))
    {
#ifdef ATOMIC_FASTBINS
      if (! have_lock && locked)
      (void)mutex_unlock(&av->mutex);
#endif
      Allocator::report_error("free(): invalid pointer", chunk2mem(p));
      return;
    }
  /* We know that each chunk is at least MINSIZE bytes in size.  */
  if (__builtin_expect(size < MINSIZE, 0))
    {
      Allocator::report_error("free(): invalid size", chunk2mem(p));
      return;
    }

  this->check_inuse_chunk(p);

  /*
   If eligible, place chunk on a fastbin so it can be found
   and used quickly in malloc.
   */

  if ((unsigned long) (size) <= (unsigned long) (get_max_fast ())

#if TRIM_FASTBINS
      /*
       If TRIM_FASTBINS set, don't place chunks
       bordering top into fastbins
       */
      && (chunk_at_offset(p, size) != this->top)
#endif
  )
    {

      if (__builtin_expect(chunk_at_offset (p, size)->size <= 2 * SIZE_SZ, 0)
          || __builtin_expect(chunksize (chunk_at_offset (p, size))
              >= this->system_mem, 0))
        {
#ifdef ATOMIC_FASTBINS
          /* We might not have a lock at this point and concurrent modifications
           of system_mem might have let to a false positive.  Redo the test
           after getting the lock.  */
          if (have_lock
              || (
                    { assert (locked == 0);
                      mutex_lock(&av->mutex);
                      locked = 1;
                      chunk_at_offset (p, size)->size <= 2 * SIZE_SZ
                      || chunksize (chunk_at_offset (p, size)) >= av->system_mem;
                    }))
#endif
            {
              Allocator::report_error("free(): invalid next size (fast)",
                  chunk2mem(p));
            }
#ifdef ATOMIC_FASTBINS
          if (! have_lock)
            {
              (void)mutex_unlock(&av->mutex);
              locked = 0;
            }
#endif
        }

      if (__builtin_expect(perturb_byte, 0))
        free_perturb (chunk2mem(p), size - SIZE_SZ);

      set_fastchunks(this);
      fb = &fastbin (this, fastbin_index(size));

#ifdef ATOMIC_FASTBINS
      mchunkptr fd;
      mchunkptr old = *fb;
      do
        {
          /* Another simple check: make sure the top of the bin is not the
           record we are going to add (i.e., double free).  */
          if (__builtin_expect (old == p, 0))
            {
              errstr = "double free or corruption (fasttop)";
              goto errout;
            }
          p->fd = fd = old;
        }
      while ((old = catomic_compare_and_exchange_val_rel (fb, p, fd)) != fd);
#else
      /* Another simple check: make sure the top of the bin is not the
       record we are going to add (i.e., double free).  */
      if (__builtin_expect(*fb == p, 0))
        {
          Allocator::report_error("double free or corruption (fasttop)",
              chunk2mem(p));
          return;
        }

      p->fd = *fb;
      *fb = p;
#endif
    }

  /*
   Consolidate other non-mmapped chunks as they arrive.
   */

  else if (!chunk_is_mmapped(p))
    {
#ifdef ATOMIC_FASTBINS
      if (! have_lock)
        {
# if THREAD_STATS
          if(!mutex_trylock(&av->mutex))
          ++(av->stat_lock_direct);
          else
            {
              (void)mutex_lock(&av->mutex);
              ++(av->stat_lock_wait);
            }
# else
          (void)mutex_lock(&av->mutex);
# endif
          locked = 1;
        }
#endif

      nextchunk = chunk_at_offset(p, size);

      /* Lightweight tests: check whether the block is already the
       top block.  */
      if (__builtin_expect(p == this->top, 0))
        {
          Allocator::report_error("double free or corruption (top)",
              chunk2mem(p));
          return;
        }
      /* Or whether the next chunk is beyond the boundaries of the arena.  */
      if (__builtin_expect(contiguous (this) && (char *) nextchunk
          >= ((char *) this->top + chunksize(this->top)), 0))
        {
          Allocator::report_error("double free or corruption (out)",
              chunk2mem(p));
          return;
        }
      /* Or whether the block is actually not marked used.  */
      if (__builtin_expect(!prev_inuse(nextchunk), 0))
        {
          Allocator::report_error("double free or corruption (!prev)",
              chunk2mem(p));
          return;
        }

      nextsize = chunksize(nextchunk);
      if (__builtin_expect(nextchunk->size <= 2 * SIZE_SZ, 0)
          || __builtin_expect(nextsize >= this->system_mem, 0))
        {
          Allocator::report_error("free(): invalid next size (normal)",
              chunk2mem(p));
          return;
        }

      if (__builtin_expect(perturb_byte, 0))
        free_perturb (chunk2mem(p), size - SIZE_SZ);

      /* consolidate backward */
      if (!prev_inuse(p))
        {
          prevsize = p->prev_size;
          size += prevsize;
          p = chunk_at_offset(p, -((long) prevsize));
          unlink(p, bck, fwd);
        }

      if (nextchunk != this->top)
        {
          /* get and clear inuse bit */
          nextinuse = inuse_bit_at_offset(nextchunk, nextsize);

          /* consolidate forward */
          if (!nextinuse)
            {
              unlink(nextchunk, bck, fwd);
              size += nextsize;
            }
          else
            clear_inuse_bit_at_offset(nextchunk, 0);

          /*
           Place the chunk in unsorted chunk list. Chunks are
           not placed into regular bins until after they have
           been given one chance to be used in malloc.
           */

          bck = unsorted_chunks(this);
          fwd = bck->fd;
          if (__builtin_expect(fwd->bk != bck, 0))
            {
              Allocator::report_error("free(): corrupted unsorted chunks",
                  chunk2mem(p));
              return;
            }
          p->fd = fwd;
          p->bk = bck;
          if (!in_smallbin_range(size))
            {
              p->fd_nextsize = NULL;
              p->bk_nextsize = NULL;
            }
          bck->fd = p;
          fwd->bk = p;

          set_head(p, size | PREV_INUSE);
          set_foot(p, size);

          this->check_free_chunk(p);
        }

      /*
       If the chunk borders the current high end of memory,
       consolidate into top
       */

      else
        {
          size += nextsize;
          set_head(p, size | PREV_INUSE);
          this->top = p;
          this->check_chunk(p);
        }

      /*
       If freeing a large space, consolidate possibly-surrounding
       chunks. Then, if the total unused topmost memory exceeds trim
       threshold, ask malloc_trim to reduce top.

       Unless max_fast is 0, we don't know if there are fastbins
       bordering top, so we cannot tell for sure whether threshold
       has been reached unless fastbins are consolidated.  But we
       don't want to consolidate on each free.  As a compromise,
       consolidation is performed if FASTBIN_CONSOLIDATION_THRESHOLD
       is reached.
       */

      if ((unsigned long) (size) >= FASTBIN_CONSOLIDATION_THRESHOLD)
        {
          if (have_fastchunks(this))
            this->consolidate();

          if (this == main_arena)
            {
#ifndef MORECORE_CANNOT_TRIM
              if ((unsigned long) (chunksize(this->top))
                  >= (unsigned long) (mp_.trim_threshold))
                this->release_memory(mp_.top_pad);
#endif
            }
#if USE_ARENAS
          else

            {
              /* Always try heap_trim(), even if the top chunk is not
               large, because the corresponding heap might go away.  */
              heap_info *heap = heap_for_ptr(top(av));

              assert(heap->ar_ptr == av);
              heap_trim(heap, mp_.top_pad);
            }
#endif
        }

#ifdef ATOMIC_FASTBINS
      if (! have_lock)
        {
          assert (locked);
          (void)mutex_unlock(&av->mutex);
        }
#endif
    }
  /*
   If the chunk was allocated via mmap, release via munmap(). Note
   that if HAVE_MMAP is false but chunk_is_mmapped is true, then
   user must have overwritten memory. There's nothing we can do to
   catch this error unless MALLOC_DEBUG is set, in which case
   check_inuse_chunk (above) will have triggered error.
   */

  else
    {
#if HAVE_MMAP
      this->munmap_chunk(p);
#endif
    }
}

/*
 ------------------------- malloc_consolidate -------------------------

 malloc_consolidate is a specialized version of free() that tears
 down chunks held in fastbins.  Free itself cannot be used for this
 purpose since, among other things, it might place chunks back onto
 fastbins.  So, instead, we need to use a minor variant of the same
 code.

 Also, because this routine needs to be called the first time through
 malloc anyway, it turns out to be the perfect place to trigger
 initialization code.
 */

void
Allocator::consolidate()
{
  mfastbinptr* fb; /* current fastbin being consolidated */
  mfastbinptr* maxfb; /* last fastbin (for loop control) */
  mchunkptr p; /* current chunk being consolidated */
  mchunkptr nextp; /* next chunk to consolidate */
  mchunkptr unsorted_bin; /* bin header */
  mchunkptr first_unsorted; /* chunk to link to */

  /* These have same use as in free() */
  mchunkptr nextchunk;
  size_t size;
  size_t nextsize;
  size_t prevsize;
  int nextinuse;
  mchunkptr bck;
  mchunkptr fwd;

  /*
   If max_fast is 0, we know that av hasn't
   yet been initialized, in which case do so below
   */

  if (get_max_fast () != 0)
    {
      clear_fastchunks(this);

      unsorted_bin = unsorted_chunks(this);

      /*
       Remove each chunk from fast bin and consolidate it, placing it
       then in unsorted bin. Among other reasons for doing this,
       placing in unsorted bin avoids needing to calculate actual bins
       until malloc is sure that chunks aren't immediately going to be
       reused anyway.
       */

#if 0
      /* It is wrong to limit the fast bins to search using get_max_fast
       because, except for the main arena, all the others might have
       blocks in the high fast bins.  It's not worth it anyway, just
       search all bins all the time.  */
      maxfb = &fastbin (this, fastbin_index(get_max_fast ()));
#else
      maxfb = &fastbin (this, NFASTBINS - 1);
#endif
      fb = &fastbin (this, 0);
      do
        {
#ifdef ATOMIC_FASTBINS
          p = atomic_exchange_acq (fb, 0);
#else
          p = *fb;
#endif
          if (p != 0)
            {
#ifndef ATOMIC_FASTBINS
              *fb = 0;
#endif
              do
                {
                  this->check_inuse_chunk(p);
                  nextp = p->fd;

                  /* Slightly streamlined version of consolidation code in free() */
                  size = p->size & ~(PREV_INUSE | NON_MAIN_ARENA);
                  nextchunk = chunk_at_offset(p, size);
                  nextsize = chunksize(nextchunk);

                  if (!prev_inuse(p))
                    {
                      prevsize = p->prev_size;
                      size += prevsize;
                      p = chunk_at_offset(p, -((long) prevsize));
                      unlink(p, bck, fwd);
                    }

                  if (nextchunk != this->top)
                    {
                      nextinuse = inuse_bit_at_offset(nextchunk, nextsize);

                      if (!nextinuse)
                        {
                          size += nextsize;
                          unlink(nextchunk, bck, fwd);
                        }
                      else
                        clear_inuse_bit_at_offset(nextchunk, 0);

                      first_unsorted = unsorted_bin->fd;
                      unsorted_bin->fd = p;
                      first_unsorted->bk = p;

                      if (!in_smallbin_range (size))
                        {
                          p->fd_nextsize = NULL;
                          p->bk_nextsize = NULL;
                        }

                      set_head(p, size | PREV_INUSE);
                      p->bk = unsorted_bin;
                      p->fd = first_unsorted;
                      set_foot(p, size);
                    }

                  else
                    {
                      size += nextsize;
                      set_head(p, size | PREV_INUSE);
                      this->top = p;
                    }

                }
              while ((p = nextp) != 0);

            }
        }
      while (fb++ != maxfb);
    }
  else
    {
      this->check_malloc_state();
    }
}

/*
 ------------------------------ realloc ------------------------------
 */

void*
Allocator::int_realloc(mchunkptr oldp, size_t oldsize, size_t nb)
{
  mchunkptr newp; /* chunk to return */
  size_t newsize; /* its size */
  void* newmem; /* corresponding user mem */

  mchunkptr next; /* next contiguous chunk after oldp */

  mchunkptr remainder; /* extra space at end of newp */
  unsigned long remainder_size; /* its size */

  mchunkptr bck; /* misc temp for linking */
  mchunkptr fwd; /* misc temp for linking */

  unsigned long copysize; /* bytes to copy */
  unsigned int ncopies; /* INTERNAL_SIZE_T words to copy */
  size_t* s; /* copy source */
  size_t* d; /* copy destination */

  /* oldmem size */
  if (__builtin_expect(oldp->size <= 2 * SIZE_SZ, 0) || __builtin_expect(
      oldsize >= this->system_mem, 0))
    {
      Allocator::report_error("realloc(): invalid old size", chunk2mem(oldp));
      return NULL;
    }

  this->check_inuse_chunk(oldp);

  /* All callers already filter out mmap'ed chunks.  */
#if 0
  if (!chunk_is_mmapped(oldp))
#else
  assert (!chunk_is_mmapped(oldp));
#endif
    {

      next = chunk_at_offset(oldp, oldsize);
      size_t nextsize = chunksize(next);
      if (__builtin_expect(next->size <= 2 * SIZE_SZ, 0) || __builtin_expect(
          nextsize >= this->system_mem, 0))
        {
          Allocator::report_error("realloc(): invalid next size",
              chunk2mem(oldp));
          return NULL;
        }

      if ((unsigned long) (oldsize) >= (unsigned long) (nb))
        {
          /* already big enough; split below */
          newp = oldp;
          newsize = oldsize;
        }

      else
        {
          /* Try to expand forward into top */
          if (next == this->top && (unsigned long) (newsize = oldsize
              + nextsize) >= (unsigned long) (nb + MINSIZE))
            {
              set_head_size(oldp, nb | (this != main_arena ? NON_MAIN_ARENA : 0));
              this->top = chunk_at_offset(oldp, nb);
              set_head(this->top, (newsize - nb) | PREV_INUSE);
              this->check_inuse_chunk(oldp);
              return chunk2mem(oldp);
            }

          /* Try to expand forward into next chunk;  split off remainder below */
          else if (next != this->top && !inuse(next)
              && (unsigned long) (newsize = oldsize + nextsize)
                  >= (unsigned long) (nb))
            {
              newp = oldp;
              unlink(next, bck, fwd);
            }

          /* allocate, copy, free */
          else
            {
              newmem = this->int_malloc(nb - MALLOC_ALIGN_MASK);
              if (newmem == 0)
                return 0; /* propagate failure */

              newp = mem2chunk(newmem);
              newsize = chunksize(newp);

              /*
               Avoid copy if newp is next chunk after oldp.
               */
              if (newp == next)
                {
                  newsize += oldsize;
                  newp = oldp;
                }
              else
                {
                  /*
                   Unroll copy of <= 36 bytes (72 if 8byte sizes)
                   We know that contents have an odd number of
                   INTERNAL_SIZE_T-sized words; minimally 3.
                   */

                  copysize = oldsize - SIZE_SZ;
                  s = (size_t*) (chunk2mem(oldp));
                  d = (size_t*) (newmem);
                  ncopies = copysize / sizeof(size_t);
                  assert(ncopies >= 3);

                  if (ncopies > 9)
                    memcpy(d, s, copysize);

                  else
                    {
                      *(d + 0) = *(s + 0);
                      *(d + 1) = *(s + 1);
                      *(d + 2) = *(s + 2);
                      if (ncopies > 4)
                        {
                          *(d + 3) = *(s + 3);
                          *(d + 4) = *(s + 4);
                          if (ncopies > 6)
                            {
                              *(d + 5) = *(s + 5);
                              *(d + 6) = *(s + 6);
                              if (ncopies > 8)
                                {
                                  *(d + 7) = *(s + 7);
                                  *(d + 8) = *(s + 8);
                                }
                            }
                        }
                    }

#ifdef ATOMIC_FASTBINS
                  _int_free(av, oldp, 1);
#else
                  this->int_free(oldp);
#endif
                  this->check_inuse_chunk(newp);
                  return chunk2mem(newp);
                }
            }
        }

      /* If possible, free extra space in old or extended chunk */

      assert((unsigned long)(newsize) >= (unsigned long)(nb));

      remainder_size = newsize - nb;

      if (remainder_size < MINSIZE)
        { /* not enough extra to split off */
          set_head_size(newp, newsize | (this != main_arena ? NON_MAIN_ARENA : 0));
          set_inuse_bit_at_offset(newp, newsize);
        }
      else
        { /* split remainder */
          remainder = chunk_at_offset(newp, nb);
          set_head_size(newp, nb | (this != main_arena ? NON_MAIN_ARENA : 0));
          set_head(remainder, remainder_size | PREV_INUSE |
              (this != main_arena ? NON_MAIN_ARENA : 0));
          /* Mark remainder as inuse so free() won't complain */
          set_inuse_bit_at_offset(remainder, remainder_size);
#ifdef ATOMIC_FASTBINS
          _int_free(av, remainder, 1);
#else
          this->int_free(remainder);
#endif
        }

      this->check_inuse_chunk(newp);
      return chunk2mem(newp);
    }

}

/*
 ------------------------------ memalign ------------------------------
 */

void*
Allocator::int_malloc(size_t bytes, size_t alignment)
{
  size_t nb; /* padded  request size */
  char* m; /* memory returned by malloc call */
  mchunkptr p; /* corresponding chunk */
  char* brk; /* alignment point within p */
  mchunkptr newp; /* chunk to return */
  size_t newsize; /* its size */
  size_t leadsize; /* leading space before alignment point */
  mchunkptr remainder; /* spare room at end to split off */
  unsigned long remainder_size; /* its size */
  size_t size;

  /* If need less alignment than we give anyway, just relay to malloc */

  if (alignment <= MALLOC_ALIGNMENT)
    return this->int_malloc(bytes);

  /* Otherwise, ensure that it is at least a minimum chunk size */

  if (alignment < MINSIZE)
    alignment = MINSIZE;

  /* Make sure alignment is power of 2 (in case MINSIZE is not).  */
  if ((alignment & (alignment - 1)) != 0)
    {
      size_t a = MALLOC_ALIGNMENT * 2;
      while ((unsigned long) a < (unsigned long) alignment)
        a <<= 1;
      alignment = a;
    }

  checked_request2size(bytes, nb);

  /*
   Strategy: find a spot within that chunk that meets the alignment
   request, and then possibly free the leading and trailing space.
   */

  /* Call malloc with worst case padding to hit alignment. */

  m = (char*) (this->int_malloc(nb + alignment + MINSIZE));

  if (m == 0)
    return 0; /* propagate failure */

  p = mem2chunk(m);

  if ((((unsigned long) (m)) % alignment) != 0)
    { /* misaligned */

      /*
       Find an aligned spot inside chunk.  Since we need to give back
       leading space in a chunk of at least MINSIZE, if the first
       calculation places us at a spot with less than MINSIZE leader,
       we can move to the next aligned spot -- we've allocated enough
       total room so that this is always possible.
       */

      brk = (char*) mem2chunk(((unsigned long)(m + alignment - 1)) &
          -((signed long) alignment));
      if ((unsigned long) (brk - (char*) (p)) < MINSIZE)
        brk += alignment;

      newp = (mchunkptr) brk;
      leadsize = brk - (char*) (p);
      newsize = chunksize(p) - leadsize;

      /* For mmapped chunks, just adjust offset */
      if (chunk_is_mmapped(p))
        {
          newp->prev_size = p->prev_size + leadsize;
          set_head(newp, newsize|IS_MMAPPED);
          return chunk2mem(newp);
        }

      /* Otherwise, give back leader, use the rest */
      set_head(newp, newsize | PREV_INUSE |
          (this != main_arena ? NON_MAIN_ARENA : 0));
      set_inuse_bit_at_offset(newp, newsize);
      set_head_size(p, leadsize | (this != main_arena ? NON_MAIN_ARENA : 0));
#ifdef ATOMIC_FASTBINS
      _int_free(av, p, 1);
#else
      this->int_free(p);
#endif
      p = newp;

      assert (newsize >= nb &&
          (((unsigned long)(chunk2mem(p))) % alignment) == 0);
    }

  /* Also give back spare room at the end */
  if (!chunk_is_mmapped(p))
    {
      size = chunksize(p);
      if ((unsigned long) (size) > (unsigned long) (nb + MINSIZE))
        {
          remainder_size = size - nb;
          remainder = chunk_at_offset(p, nb);
          set_head(remainder, remainder_size | PREV_INUSE |
              (this != main_arena ? NON_MAIN_ARENA : 0));
          set_head_size(p, nb);
#ifdef ATOMIC_FASTBINS
          _int_free(av, remainder, 1);
#else
          this->int_free(remainder);
#endif
        }
    }

  this->check_inuse_chunk(p);
  return chunk2mem(p);
}

/*
 ------------------------- independent_calloc -------------------------
 */

void**
Allocator::int_icalloc(size_t n_elements, size_t elem_size, void** chunks)
{
  size_t sz = elem_size; /* serves as 1-element array */
  return this->ialloc<true, true> (n_elements, &sz, chunks);
}

/*
 ------------------------- independent_comalloc -------------------------
 */

void**
Allocator::int_icomalloc(size_t n_elements, size_t sizes[], void** chunks)
{
  return this->ialloc<false, false> (n_elements, sizes, chunks);
}

/*
 ------------------------------ ialloc ------------------------------
 ialloc provides common support for independent_X routines, handling all of
 the combinations that can result.

 */

template<bool one_size, bool also_clear>
  void**
  Allocator::ialloc(size_t n_elements, size_t* sizes, void** chunks)
  {
    size_t element_size; /* chunksize of each element, if all same */
    size_t contents_size; /* total size of elements */
    size_t array_size; /* request size of pointer array */
    void* mem; /* malloced aggregate space */
    mchunkptr p; /* corresponding chunk */
    size_t remainder_size; /* remaining bytes while splitting */
    void** marray; /* either "chunks" or malloced ptr array */
    mchunkptr array_chunk; /* chunk for malloced ptr array */
    int mmx; /* to disable mmap */
    size_t size;
    size_t size_flags;
    size_t i;

    /* Ensure initialization/consolidation */
    if (have_fastchunks(this))
      this->consolidate();

    /* compute array length, if needed */
    if (chunks != 0)
      {
        if (n_elements == 0)
          return chunks; /* nothing to do */
        marray = chunks;
        array_size = 0;
      }
    else
      {
        /* if empty req, must still return chunk representing empty array */
        if (n_elements == 0)
          return (void**) this->int_malloc(0);
        marray = 0;
        array_size = request2size(n_elements * (sizeof(void*)));
      }

    /* compute total element size */
    if (one_size)
      { /* all-same-size */
        element_size = request2size(*sizes);
        contents_size = n_elements * element_size;
      }
    else
      { /* add up all the sizes */
        element_size = 0;
        contents_size = 0;
        for (i = 0; i != n_elements; ++i)
          contents_size += request2size(sizes[i]);
      }

    /* subtract out alignment bytes from total to minimize overallocation */
    size = contents_size + array_size - MALLOC_ALIGN_MASK;

    /*
     Allocate the aggregate chunk.
     But first disable mmap so malloc won't use it, since
     we would not be able to later free/realloc space internal
     to a segregated mmap region.
     */
    mmx = mp_.n_mmaps_max; /* disable mmap */
    mp_.n_mmaps_max = 0;
    mem = this->int_malloc(size);
    mp_.n_mmaps_max = mmx; /* reset mmap */
    if (mem == 0)
      return 0;

    p = mem2chunk(mem);
    assert(!chunk_is_mmapped(p));
    remainder_size = chunksize(p);

    if (also_clear)
      { /* optionally clear the elements */
        memset(mem, 0, remainder_size - SIZE_SZ - array_size);
      }

    size_flags = PREV_INUSE | (this != main_arena ? NON_MAIN_ARENA : 0);

    /* If not provided, allocate the pointer array as final part of chunk */
    if (marray == 0)
      {
        array_chunk = chunk_at_offset(p, contents_size);
        marray = (void**) (chunk2mem(array_chunk));
        set_head(array_chunk, (remainder_size - contents_size) | size_flags);
        remainder_size = contents_size;
      }

    /* split out elements */
    for (i = 0;; ++i)
      {
        marray[i] = chunk2mem(p);
        if (i != n_elements - 1)
          {
            if (element_size != 0)
              size = element_size;
            else
              size = request2size(sizes[i]);
            remainder_size -= size;
            set_head(p, size | size_flags);
            p = chunk_at_offset(p, size);
          }
        else
          { /* the final element absorbs any overallocation slop */
            set_head(p, remainder_size | size_flags);
            break;
          }
      }

    if (marray != chunks)
      {
        /* final element must have exactly exhausted chunk */
        if (element_size != 0)
          assert(remainder_size == element_size);
        else
          assert(remainder_size == request2size(sizes[i]));
        this->check_inuse_chunk(mem2chunk(marray));
      }

    for (i = 0; i != n_elements; ++i)
      this->check_inuse_chunk(mem2chunk(marray[i]));

    return marray;
  }

/*
 ------------------------------ pvalloc ------------------------------
 */

void*
Allocator::int_pvalloc(size_t bytes)
{
  /* Ensure initialization/consolidation */
  if (have_fastchunks(this))
    this->consolidate();
  return this->int_malloc((bytes + page_size - 1) & ~(page_size - 1), page_size);
}

/*
 ------------------------------ malloc_trim ------------------------------
 */

int
Allocator::trim(size_t pad)
{
  /* Ensure initialization/consolidation */
  this->consolidate();

  const size_t ps = page_size;
  int psindex = bin_index (ps);
  const size_t psm1 = ps - 1;

  int result = 0;
  for (int i = 1; i < NBINS; ++i)
    if (i == 1 || i >= psindex)
      {
        mbinptr bin = bin_at (this, i);

        for (mchunkptr p = last (bin); p != bin; p = p->bk)
          {
            size_t size = chunksize (p);

            if (size > psm1 + sizeof(Allocator::malloc_chunk))
              {
                /* See whether the chunk contains at least one unused page.  */
                char *paligned_mem = (char *) (((uintptr_t) p
                    + sizeof(Allocator::malloc_chunk) + psm1) & ~psm1);

                assert ((char *) chunk2mem (p) + 4 * SIZE_SZ <= paligned_mem);
                assert ((char *) p + size > paligned_mem);

                /* This is the size we could potentially free.  */
                size -= paligned_mem - (char *) p;

                if (size > psm1)
                  {
                    /* When debugging we simulate destroying the memory
                     content.  */
                    memset(paligned_mem, 0x89, size & ~psm1);
                    //madvise (paligned_mem, size & ~psm1, MADV_DONTNEED);

                    result = 1;
                  }
              }
          }
      }

#ifndef MORECORE_CANNOT_TRIM
  return result | (this == main_arena ? this->release_memory(pad) : 0);
#else
  return result;
#endif
}

/*
 ------------------------------ mallopt ------------------------------
 */

int
Allocator::mallopt(int param_number, int value)
{
  int res = 1;

  this->acquire_lock();
  /* Ensure initialization/consolidation */
  this->consolidate();

  switch (param_number)
    {
  case M_MXFAST:
    if (value >= 0 && value <= signed(MAX_FAST_SIZE))
      {
        set_max_fast(value);
      }
    else
      res = 0;
    break;

  case M_TRIM_THRESHOLD:
    mp_.trim_threshold = value;
    mp_.no_dyn_threshold = 1;
    break;

  case M_TOP_PAD:
    mp_.top_pad = value;
    mp_.no_dyn_threshold = 1;
    break;

  case M_MMAP_THRESHOLD:
#if USE_ARENAS
    /* Forbid setting the threshold too high. */
    if((unsigned long)value > HEAP_MAX_SIZE/2)
    res = 0;
    else
#endif
    mp_.mmap_threshold = value;
    mp_.no_dyn_threshold = 1;
    break;

  case M_MMAP_MAX:
#if !HAVE_MMAP
    if (value != 0)
    res = 0;
    else
#endif
    mp_.n_mmaps_max = value;
    mp_.no_dyn_threshold = 1;
    break;

#ifdef PER_THREAD
    case M_ARENA_TEST:
    if (value > 0)
    mp_.arena_test = value;
    break;

    case M_ARENA_MAX:
    if (value > 0)
    mp_.arena_max = value;
    break;
#endif
    }
  this->release_lock();
  return res;
}

/* Helper code.  */

void
Allocator::report_error(const char *str, void *ptr)
{
  char buf[2 * sizeof(uintptr_t) + 1];
  buf[sizeof(buf) - 1] = '\0';
  char *cp = itoa((uintptr_t) ptr, &buf[sizeof(buf) - 1], 16);
  while (cp > buf)
    *--cp = '0';
  printf("*** kmalloc detected *** %s: 0x%s ***\n", str, cp);
}
void
Allocator::acquire_lock()
{
  mutex_lock(&this->mutex);
}
void
Allocator::release_lock()
{
  mutex_unlock(&this->mutex);
}

/* -------------------------- C++ memory ---------------------- */

void*
operator new(size_t sz)
{
  void*
  pt(KMemory::malloc( sz));
  if (pt)
    return pt;
  else
    panic("No more kernel memory available");
}

void
operator delete(void* pt)
{
  KMemory::free(pt);
}

void*
operator new(size_t sz, size_t align)
{
  void*
  pt(KMemory::malloc
  (sz, align));
  if (pt)
    return pt;
  else
    panic("No more kernel memory available");
}

void*
operator new[](size_t sz) {
  return ::operator new(sz);
}

/* ------------------------------------------------------------
 History:

 [see ftp://g.oswego.edu/pub/misc/malloc.c for the history of dlmalloc]

 */
/*
 * Local variables:
 * c-basic-offset: 2
 * End:
 */
