/* signal.cpp - signal management and dispatch (implementation) */
/* Copyright (C) 2010 Giovanni Campagna

 This file is part of the H+ kernel, a microkernel for the GNU system.

 H+ is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <processor.hpp>
#include <task.hpp>
#include <scheduler.hpp>
#include <global.hpp>
#include <trap.hpp>

namespace Trap
{
  void
  protectionError()
  {
    Task& t(Processor::getTask());
    GlobalStorage& gs(Processor::getGlobalStorage());
    handle<Scheduler> sched(gs.getScheduler());
    Process* p(sched->getProcess(t));
    assert(p != NULL);
    if (p->getPid() == 0)
      panic("General protection fault in kernel");
    else
      {
        siginfo_t sig =
          { siginfo_t::SIGSEGV, siginfo_t::ILL_PRVOPC, 0, 0,
              t.getProgramCounter(), 0,
                { 0 } };
        Thread* thr(p->getThread(t));
        thr->queueSignal(&sig);
        thr->mark(Thread::error);
      }
  }
  void
  instructionError()
  {
    Task& t(Processor::getTask());
    GlobalStorage& gs(Processor::getGlobalStorage());
    handle<Scheduler> sched(gs.getScheduler());
    Process* p(sched->getProcess(t));
    assert(p != NULL);
    if (p->getPid() == 0)
      panic("Invalid opcode in kernel");
    else
      {
        siginfo_t sig =
          { siginfo_t::SIGILL, siginfo_t::ILL_ILLOPC, 0, 0,
              t.getProgramCounter(), 0,
                { 0 } };
        Thread* thr(p->getThread(t));
        thr->queueSignal(&sig);
        thr->mark(Thread::error);
      }
  }
}
